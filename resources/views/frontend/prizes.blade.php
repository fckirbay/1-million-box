@extends('layouts.frontend.main')
@section('styles')

@endsection
@section('content')
	<section id="jackpots" class="jackpots back-dark section" style="padding-top: 160px">
		<div class="container">
			<div class="row justify-content-center text-center">
				<div class="col-lg-6">
					<div class="heading">
						<h2>Prizes</h2>
						<img src="{{ asset('assets/frontend/images/heading-border-effect.png') }}" class="img-fluid" alt="effect">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12">

					<section id="counter" class=" back-light">
						<div class="container">
							<div class="row conter-res yellow-bg text-center">
								<div class="col-md-5 counter-left text-center">
									<div>
										<h4>1 x Jackpot</h4>
									</div>
								</div>
								<div class="col-md-7 col-12 text-center counterinner">
									<ul>
										<li class="counter-text border-count">
											<h3 class="counter-after">$</h3>

										</li>
										<li class="counter-text border-count">
											<h3 class="counter counter-after">1</h3>
										</li>
										<li class="counter-text border-count">
											<h3 class="counter counter-after">0</h3>
										</li>
										<li class="counter-text border-count">
											<h3 class="counter counter-after">0</h3>
										</li>
										<li class="counter-text border-count">
											<h3 class="counter counter-after">0</h3>
										</li>
										<li class="counter-text border-count">
											<h3 class="counter counter-after">0</h3>
										</li>
										<li class="counter-text border-count">
											<h3 class="counter counter-after">0</h3>
										</li>
									</ul>

								</div>

							</div>
						</div>
					</section>

					<br />

					<div class="row justify-content-center text-center">
						<div class="col-lg-3 col-md-3">
							<div class="sub-heading">
								<div class="run-slick">
									<div class="running-img">
										<img src="{{ asset('assets/frontend/images/prizes.jpg') }}" class="img-fluid" alt="">
										<div class="run-txt">
											<h4>2 x</h4>
											<p>$10.000</p>
											<div class="casino-btn">
												<a href="#" class="btn-4 yellow-btn">Play</a></div>
										</div>
									</div>
									<div class="running-img">
										<img src="{{ asset('assets/frontend/images/prizes.jpg') }}" class="img-fluid" alt="">
										<div class="run-txt">
											<h4>50 x</h4>
											<p>$200</p>
											<div class="casino-btn">

												<a href="#" class="btn-4 yellow-btn">play</a></div>
										</div>
									</div>
									<div class="running-img">
										<img src="{{ asset('assets/frontend/images/prizes.jpg') }}" class="img-fluid" alt="">
										<div class="run-txt">
											<h4>7000 x</h4>
											<p>$10</p>
											<div class="casino-btn">

												<a href="#" class="btn-4 yellow-btn">play</a></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-3">
							<div class="sub-heading">
								<div class="run-slick">
									<div class="running-img">
										<img src="{{ asset('assets/frontend/images/prizes.jpg') }}" class="img-fluid" alt="">
										<div class="run-txt">
											<h4>3 x</h4>
											<p>$5.000</p>
											<div class="casino-btn">
												<a href="#" class="btn-4 yellow-btn">play</a></div>
										</div>
									</div>
									<div class="running-img">
										<img src="{{ asset('assets/frontend/images/prizes.jpg') }}" class="img-fluid" alt="">
										<div class="run-txt">
											<h4>100 x</h4>
											<p>$100</p>
											<div class="casino-btn">

												<a href="#" class="btn-4 yellow-btn">play</a></div>
										</div>
									</div>
									<div class="running-img">
										<img src="{{ asset('assets/frontend/images/prizes.jpg') }}" class="img-fluid" alt="">
										<div class="run-txt">
											<h4>10000 x</h4>
											<p>$5</p>
											<div class="casino-btn">

												<a href="#" class="btn-4 yellow-btn">play</a></div>
										</div>
									</div>

								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-3">
							<div class="sub-heading">
								<div class="run-slick">
									<div class="running-img">
										<img src="{{ asset('assets/frontend/images/prizes.jpg') }}" class="img-fluid" alt="">
										<div class="run-txt">
											<h4>5 x</h4>
											<p>$1.000</p>
											<div class="casino-btn">
												<a href="#" class="btn-4 yellow-btn">play</a></div>
										</div>
									</div>
									<div class="running-img">
										<img src="{{ asset('assets/frontend/images/prizes.jpg') }}" class="img-fluid" alt="">
										<div class="run-txt">
											<h4>500 x</h4>
											<p>$50</p>
											<div class="casino-btn">

												<a href="#" class="btn-4 yellow-btn">play</a></div>
										</div>
									</div>
									<div class="running-img">
										<img src="{{ asset('assets/frontend/images/prizes.jpg') }}" class="img-fluid" alt="">
										<div class="run-txt">
											<h4>50000 x</h4>
											<p>$2</p>
											<div class="casino-btn">

												<a href="#" class="btn-4 yellow-btn">play</a></div>
										</div>
									</div>

								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-3">
							<div class="sub-heading">
								<div class="run-slick">
									<div class="running-img">
										<img src="{{ asset('assets/frontend/images/prizes.jpg') }}" class="img-fluid" alt="">
										<div class="run-txt">
											<h4>20 x</h4>
											<p>$500</p>
											<div class="casino-btn">
												<a href="#" class="btn-4 yellow-btn">play</a></div>
										</div>
									</div>
									<div class="running-img">
										<img src="{{ asset('assets/frontend/images/prizes.jpg') }}" class="img-fluid" alt="">
										<div class="run-txt">
											<h4>3000 x</h4>
											<p>$20</p>
											<div class="casino-btn">

												<a href="#" class="btn-4 yellow-btn">play</a></div>
										</div>
									</div>
									<div class="running-img">
										<img src="{{ asset('assets/frontend/images/prizes.jpg') }}" class="img-fluid" alt="">
										<div class="run-txt">
											<h4>125000 x</h4>
											<p>$1</p>
											<div class="casino-btn">

												<a href="#" class="btn-4 yellow-btn">play</a></div>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection
@section('scripts')
  
@endsection