		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>1 Million Box</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!---Font Icon-->
		<link href="{{ asset('assets/frontend/css/font-awesome.min.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/frontend/font/flaticon.css') }}" rel="stylesheet">
		<!-- / -->

		<!-- Plugin CSS -->
		<link href="{{ asset('assets/frontend/css/bootstrap.min.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/frontend/css/slick.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/frontend/css/animate.min.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/frontend/css/magnific-popup.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/frontend/css/YouTubePopUp.css') }}" rel="stylesheet">
		<link rel="stylesheet" href="{{ asset('assets/frontend/css/menu.css') }}">
		<!-- / -->
		<!-- Favicon -->
		<link rel="icon" href="{{ asset('assets/frontend/images/header-logo.png') }}" />
		<!-- / -->
		<!-- Theme Style -->
		<link href="{{ asset('assets/frontend/css/style.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/frontend/css/responsive.css') }}" rel="stylesheet">