	<!-- Casino Contact start -->
	<section id="contact-us" class="contact-us back-dark contact section">
		<div class="container">
			<div class="row">

				<!--<div class="col-lg-8 col-md-8">
					<div class="contact-about">
						<div class="heading">
							<h2>ABOUT 1 Million Box</h2>
							<img src="{{ asset('assets/frontend/images/heading-border-effect.png') }}" class="img-fluid" alt="effect">
						</div>
						<p class="mb30">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p><br>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ua.</p>
					</div>
				</div>-->
				<div class="col-lg-12 col-md-12">
					<div class="row">
						<div class="col-lg-8">
							<div class="payments">
								<div class="heading">
									<h2>Payment Methods</h2>
									<img src="{{ asset('assets/frontend/images/heading-border-effect.png') }}" class="img-fluid" alt="effect">
								</div>
								<ul>
									<li><a href="#"><img src="{{ asset('assets/frontend/images/payment/bitcoin.jpg') }}" class="img-fluid" alt="effect"></a></li>
									<li><a href="#"><img src="{{ asset('assets/frontend/images/payment/westernunion.jpg') }}" class="img-fluid" alt="effect"></a></li>
									<li><a href="#"><img src="{{ asset('assets/frontend/images/payment/payment-image-2.jpg') }}" class="img-fluid" alt="effect"></a></li>
									<li><a href="#"><img src="{{ asset('assets/frontend/images/payment/payment-image-5.jpg') }}" class="img-fluid" alt="effect"></a></li>
								</ul>
							</div>
						</div>
							<div class="col-lg-4">
								<div class="payments">
									<div class="heading">
										<h3 style="padding-top: 5px;">Subscribe for offers</h3>
										<img src="{{ asset('assets/frontend/images/heading-border-effect.png') }}" class="img-fluid" alt="effect">
									</div>
									<div class="sub-form">
										<form>
											<div class="form-group col-sm-12">
												<input type="text" class="form-control" name="email" placeholder="Enter Your Email">
											</div>
											<div class="casino-btn newsletter">
												<a href="#" class="btn-4 yellow-btn" style="line-height: 12px;">send</a></div>
											</form>
										</div></div>
									</div></div>
								</div>
							</div>
							<div class="row control-pad">
								<div class="border-effect1">
									<img src="{{ asset('assets/frontend/images/border-effect.png') }}" class="img-fluid" alt="effect">
								</div>
								<div class="border-effect2">
									<img src="{{ asset('assets/frontend/images/border-effect.png') }}" class="img-fluid" alt="effect">
								</div>
							</div>
							<div class="row justify-content-center text-center">
								<div class="col-md-12 bot-menu">
									<div class="foot-menu">
										<ul>
											<li><a href="{{ url('/') }}">Home</a></li>
											<li><a href="{{ url('/play') }}">Play</a></li>
											<li><a href="{{ url('/prizes') }}">Prizes</a></li>
											<li><a href="{{ url('/how-to-play') }}">How to Play?</a></li>
											<li><a href="{{ url('/how-to-start') }}">How to Start?</a></li>
											<li><a href="{{ url('/frequently-ask-questions') }}">Frequently Ask Questions</a></li>
										</ul>
									</div>
								</div>
				<!--<div class="col-md-12">
					<div class="copy-right">
						<h6>Copyright 2020. All rights reserved by 1MillionBox.com</h6>
					</div>
				</div>-->
			</div>
		</div>

	</section>
	<!-- Casino Contact End -->