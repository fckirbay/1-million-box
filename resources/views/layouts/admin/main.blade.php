<!DOCTYPE html>
<html lang="tr">
    <head>
    @include('partials.admin.head')
    @yield('styles')
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            @include('partials.admin.header')
            @include('partials.admin.side')
            <div class="content-wrapper">
                @yield('content')
            </div>
            @include('partials.admin.footer')
        <div class="control-sidebar-bg"></div>
        </div>
        @include('partials.admin.scripts')
        @yield('scripts')
        
        @if(session('permission_denied'))
        <script>
        	swal("Yetkiniz yok!", "", "error");
        </script>
        @endif
        
    </body>
</html>