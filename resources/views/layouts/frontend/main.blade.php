<!DOCTYPE HTML>
<html lang="en">
	<head>
    @include('partials.frontend.head')
    @yield('styles')
    </head>
    <body oncontextmenu ="return false;">
        <!-- back to top start -->
        <div id="back-top-btn">
            <i class="fa fa-chevron-up"></i>
        </div>
        <!-- back to top end -->
        <!-- Preloader Start -->
        <div class="preloader">
            <div class="loader">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        @include('partials.frontend.header')
        <!-- Preloader end -->
        @yield('content')
        @include('partials.frontend.footer')
        @include('partials.frontend.scripts')
        @yield('scripts')
    </body>
</html>