@extends('layouts.admin.main')
@section('styles')

@endsection
@section('content')
	<!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Ödüller</h3>
              {!! Form::open(['url'=>'admin/rewardeds', 'method' => 'get', 'style' => 'float:right', 'id'=>'search-form'])  !!}
              <div class="box-tools" style="width:250px">

                <div class="row">
                  <div class="col-xs-8">
                    <select class="form-control" name="days">
                      <option value="1095">Tüm Zamanlar</option>
                      <option value="3">Son 3 Gün</option>
                      <option value="7" selected>Son 1 Hafta</option>
                      <option value="14">Son 2 Hafta</option>
                      <option value="30">Son 1 Ay</option>
                      <option value="60">Son 2 Ay</option>
                      <option value="90">Son 3 Ay</option>
                      <option value="180">Son 6 Ay</option>
                      <option value="365">Son 1 Yıl</option>
                    </select>
                  </div>
                  <div class="col-xs-2">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
              {!! Form::close() !!}
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th style="width:25%">Kullanıcı ID</th>
                  <th class="orta" style="width:10%">Toplam İzleme</th>
                </tr>
                @forelse($rewardeds as $key => $val)
                <tr>
                  <td>{{ $val->username }}</td>
                  <td class="orta">{{ $val->total }}</td>
                </tr>
                @empty
                <tr>
                    <td colspan="2" style="text-align:center">Hiç kayıt bulunamadı.</td>
                </tr>
                @endforelse
              </table>
            </div>
            {!! $rewardeds->appends(Request::capture()->except('page'))->render() !!}
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
@endsection
@section('scripts')
  <script>
    $('#search-form').submit(function() {
        $(this).find(":input").filter(function(){return !this.value;}).attr("disabled", "disabled");
    });
    // Un-disable form fields when page loads, in case they click back after submission or client side validation
    $('#search-form').find(":input").prop("disabled", false);
  </script>
  @include('sweet::alert')
@endsection