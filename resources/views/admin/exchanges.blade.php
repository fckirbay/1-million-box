@extends('layouts.admin.main')
@section('styles')

@endsection
@section('content')
	<!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Dönüştürmeler</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th style="width:10%">Kullanıcı ID</th>
                  <th class="orta" style="width:20%">Kullanıcı Adı</th>
                  <th class="orta" style="width:10%">Adet</th>
                  <th class="orta" style="width:60%">Tarih</th>
                </tr>
                @forelse($exchanges as $key => $val)
                <tr>
                  <td><a href="{{ url('admin/user-management/user', $val->user_id) }}">{{ $val->user_id }}</a></td>
                  <td class="orta">{{ $val->first_name }}</td>
                  <td class="orta">{{ $val->amount }}</td>
                  <td class="orta">{{ Carbon\Carbon::parse($val->date)->format('d/m/Y H:i:s') }}</td>
                </tr>
                @empty
                <tr>
                    <td colspan="5" style="text-align:center">Hiç kayıt bulunamadı.</td>
                </tr>
                @endforelse
              </table>
            </div>
            {{ $exchanges->links() }}
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
@endsection
@section('scripts')
  @include('sweet::alert')
@endsection