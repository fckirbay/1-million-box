@extends('layouts.admin.main')
@section('styles')

@endsection
@section('content')
	<!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Sayfalar <a href="{{ url('admin/site-management/new-page') }}" class="btn btn-primary btn-xs">Yeni Ekle</a></h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Ara">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th style="width:40%">Başlık</th>
                  <th style="width:15%">Kategori</th>
                  <th style="width:15%">Yayınlanma Tarihi</th>
                  <th style="width:20%">İşlem</th>
                </tr>
                @forelse($pages as $key => $val)
                <tr>
                  <td>{{ $val->title }}</td>
                  <td>{{ $val->category }}</td>
                  <td>{{ Carbon\Carbon::parse($val->created_at)->format('H:i:s d.m.Y') }}</td>
                  <td><a href="{{ url('admin/site-management/edit-page', $val->id) }}" class="btn btn-primary btn-xs">Düzenle</a> <a href="{{ url('admin/site-management/delete-page', $val->id) }}" class="btn btn-danger btn-xs">Sil</a></td>
                </tr>
                @empty
                <tr>
                    <td colspan="4" style="text-align:center">Hiç kayıt bulunamadı.</td>
                </tr>
                @endforelse
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
@endsection
@section('scripts')
  @include('sweet::alert')
@endsection