<!-- ================================== TOP NAVIGATION ================================== -->
            <div class="side-menu animate-dropdown outer-bottom-xs">
                <div class="head"><i class="icon fa fa-align-justify fa-fw"></i> Kategoriler</div>        
                <nav class="yamm megamenu-horizontal" role="navigation">
                    <ul class="nav">
                        <!--
                        <!-- /.menu-item -->
            
                        
            
            <?php $__empty_1 = true; $__currentLoopData = $menu[0]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                <?php if(isset($menu[1][$key])): ?>
        		<li class="dropdown menu-item">
                 <a href="<?php echo e(url('kategori', $val->slug)); ?>" class="dropdown-toggle" data-hover="dropdown"><img src="<?php echo e(asset($val->icon)); ?>" style="width:16px; margin-right:8px"/> <?php echo e($val->name); ?></a>
                    <ul class="dropdown-menu mega-menu">
                        <li class="yamm-content">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-lg-4">
                                    <ul>
                                        <?php $__empty_2 = true; $__currentLoopData = $menu[1][$key]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key2 => $val2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_2 = false; ?>
                                        <li><a href="<?php echo e(url('kategori', $val2['slug'])); ?>"><?php echo e($val2['name']); ?></a></li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_2): ?>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                                <?php if($val->photo != null): ?>
                                <div class="dropdown-banner-holder">
                                    <a href="#"><img alt="" src="<?php echo e(asset($val->photo)); ?>" /></a>
                                </div>
                                <?php endif; ?>
                            </div><!-- /.row -->
                        </li><!-- /.yamm-content -->                    
                    </ul><!-- /.dropdown-menu -->
                    </li>
                <?php else: ?>
                <li class="menu-item">
                    <a href="<?php echo e(url('kategori', $val->slug)); ?>"><img src="<?php echo e(asset($val->icon)); ?>" style="width:16px; margin-right:8px"/> <?php echo e($val->name); ?></a>
                </li>
                <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
            <?php endif; ?>
                    </ul><!-- /.nav -->
                </nav><!-- /.megamenu-horizontal -->
            </div><!-- /.side-menu -->
<!-- ================================== TOP NAVIGATION : END ================================== -->