<!DOCTYPE html>
<html lang="tr">
    <head>
    <?php echo $__env->make('partials.mobile.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->yieldContent('styles'); ?>
    </head>
    <body>
        <?php echo $__env->make('partials.mobile.preloader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div id="page-transitions" class="page-build highlight-red">
    	    <?php echo $__env->make('partials.mobile.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    	    <?php echo $__env->make('partials.mobile.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    	    <div class="page-content header-clear">
                <?php echo $__env->yieldContent('content'); ?>
            </div>
            <?php echo $__env->make('partials.mobile.menu-cart', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php echo $__env->make('partials.mobile.menu-share', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php echo $__env->make('partials.mobile.menu-2', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <a href="#" class="back-to-top-badge back-to-top-small bg-highlight"><i class="fa fa-angle-up"></i>Back to Top</a>
        </div>
        <?php echo $__env->make('partials.mobile.scripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->yieldContent('scripts'); ?>
    </body>
</html>