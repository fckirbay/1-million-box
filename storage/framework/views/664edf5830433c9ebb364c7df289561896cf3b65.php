<?php $__env->startSection('styles'); ?>
    <style>
        .wheelContainer{
          position:absolute;
          width:100%;
          height:100%;
          max-width:1000px;
          margin-left:auto;
          margin-right:auto;
          left:50%;
          top:50%;
          transform: translate(-50%, -50%) ; 
        }
        
        
        .wheelSVG{
          position:absolute;
          width:100%;
          height:100%;
          visibility:hidden;
          overflow:visible;
          
        }
        .peg{
          visibility:hidden;
        }
        .wheelOutline, .valueContainer, .toast, .centerCircle{
          pointer-events:none;
        }
        
        .wheelText{
          pointer-events:none;
          /* font-size:28px; */
          /* fill:red; */
          text-anchor:middle;
          font-family:'Fjalla One', Arial, sans-serif;
          -webkit-user-select: none;
          user-select:none;
        }
        
        .toast {
          position:absolute;
          background-color:#E81D62;
          border-radius:12px;
          opacity:0;
          text-align:center;
        }
        .toast p{
          clear: both;
          font-family:'Fjalla One', Arial, sand-serif;
          margin:23px;
          font-size:30px;
          color:#ededed;
          letter-spacing:-0px;
          -webkit-touch-callout: none; 
          -webkit-user-select: none; 
          -khtml-user-select: none;  
          -moz-user-select: none;    
          -ms-user-select: none;     
          user-select: none;  
          line-height:32px;
          -webkit-transition: font-size .2s ease;
          transition: font-size .2s ease; 
          -webkit-transition: line-height .2s ease;
          transition: line-height .2s ease; 
        }
        
        /* Smartphones (portrait and landscape) ----------- */
        @media  only screen and (max-width : 480px) {
          .toast p, .toast span{
            font-size:18px;
            line-height:18px;
          }
        }
        @media  only screen and (min-width : 481px) and (max-width : 800px) {
          .toast p, .toast span{
            font-size:38px;
            line-height:38px;
          }
        }
        
        @media  only screen and (min-width : 801px)  {
          .toast p, .toast span{
            font-size:58px;
            line-height:60px;
          }
        }
        
        
        @media  only screen and (max-height : 480px) {
          .toast p, .toast span{
            font-size:33px;
            line-height:35px;
          }
        }
        </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>


<h3 class="uppercase bolder center-text" style="margin-top:50px"><?php echo app('translator')->getFromJson('general.coming'); ?> <span class="color-highlight"><?php echo app('translator')->getFromJson('general.soon'); ?></span></h3>

<p class="center-text boxed-text-large bottom-30" style="margin-top:30px">
<?php echo app('translator')->getFromJson('general.you_can_win_extra_tickets'); ?>
</p>


<h3 class="uppercase bolder center-text" style="margin-top:50px"><?php echo app('translator')->getFromJson('general.do_you_want'); ?> <span class="color-highlight"><?php echo app('translator')->getFromJson('general.more_tickets'); ?></span>?</h3>

<p class="center-text boxed-text-large bottom-30" style="margin-top:30px">
<?php echo app('translator')->getFromJson('general.do_you_want_more_tickets'); ?>
</p>

        <!--

          <button class="col button button-fill spinBtn" style="width:80%; height:40px; margin-left:10%; font-size:20px; font-weight:bold; margin-top:20px">Çevir Kazan</button>

          

          <div class="wheelContainer" style="width:100%; height:50%; pointer-events: none">
            <svg class="wheelSVG" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid meet" text-rendering="optimizeSpeed">
          <defs>
          <filter id="shadow" x="-100%" y="-100%" width="550%" height="550%">
            <feOffset in="SourceAlpha" dx="0" dy="0" result="offsetOut"></feOffset>        
            <feGaussianBlur stdDeviation="9" in="offsetOut" result="drop" />
          <feColorMatrix in="drop" result="color-out" type="matrix"
                values="0 0 0 0   0
                        0 0 0 0   0 
                        0 0 0 0   0 
                        0 0 0 .3 0"/>  
           <feBlend in="SourceGraphic" in2="color-out" mode="normal" />
          </filter> 
            
          </defs>
            <g class="mainContainer">   
              <g class="wheel"/>    
            </g> 
            <g class="centerCircle" />
              
            <g class="wheelOutline" />
              <g class="pegContainer" >
                <path  class="peg" fill="#EEEEEE" d="M22.139,0C5.623,0-1.523,15.572,0.269,27.037c3.392,21.707,21.87,42.232,21.87,42.232	s18.478-20.525,21.87-42.232C45.801,15.572,38.623,0,22.139,0z" /> 
              </g>
          <g class="valueContainer" />

          </svg>
            <div class="toast">
            	<p>

              <p/>
            </div>
		    </div>

        -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.4/TweenMax.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.4/utils/Draggable.min.js'></script>
    <script src="<?php echo e(asset('assets/mobile/scripts/spin2win/ThrowPropsPlugin.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/mobile/scripts/spin2win/Spin2WinWheel.min.js?v='.rand(1111111111,9999999999))); ?>"></script>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/gsap/latest/plugins/TextPlugin.min.js'></script>
    <script src="<?php echo e(asset('assets/mobile/scripts/spin2win/index.js?v='.rand(1111111111,9999999999))); ?>"></script>
    
    <script>
      $(".spinBtn").click(function(){
        $.ajax({
                    url: 'spin-2-win-date',
                    method: 'POST',
                    dataType: 'json',
                    success: function(data) {
                        JSON.stringify(data); //to string
                    }
                });
      });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.mobile.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>