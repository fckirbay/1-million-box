<?php $__env->startSection('styles'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
	<section id="contact-page" class="contact-page section" style="padding-top: 160px">
        <div class="container">


            <div class="row">
                <div class="col-md-6">
                    <div class="heading">
                        <h2>Contact us</h2>
                        <img src="<?php echo e(asset('assets/frontend/images/heading-border-effect.png')); ?>" class="img-fluid" alt="effect">
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-lg-2 col-md-3 col-3">
                                    <div class="location-icon">
                                        <i class="fa fa-at"></i>
                                    </div>
                                </div>
                                <div class="col-lg-10 col-md-9 col-9">
                                    <div class="location-txt">
                                        <span>E-Mail: <br />info@1millionbox.com</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-lg-2 col-md-3 col-3">
                                    <div class="location-icon">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                </div>
                                <div class="col-lg-10 col-md-9 col-9">
                                    <div class="location-txt">
                                        <span>Phone:<br />(+356) 7955 4233</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="heading">
                        <h2>Register</h2>
                        <img src="<?php echo e(asset('assets/frontend/images/heading-border-effect.png')); ?>" class="img-fluid" alt="effect">
                    </div>
                    <div class="faq-form">

                        <form>
                        	<div class="form-group col-sm-12">
                                <input type="text" class="form-control" name="name" placeholder="Enter Your Name">
                            </div>
                            <div class="form-group col-sm-12">
                                <input type="text" class="form-control" name="email" placeholder="Enter Your Email">
                            </div>
                            <div class="form-group col-sm-12">
                                <input type="password" class="form-control" name="password" placeholder="Enter Your Password">
                            </div>
                            <div class="form-group col-sm-12">
                                <input type="password" class="form-control" name="confirm_password" placeholder="Confirm Your Password">
                            </div>
                            <div class="casino-btn col-sm-12">
                                <a href="#" class="btn-4 yellow-btn faq-btn">Register</a></div>
                        </form>

                    </div>

                </div>
            </div>

        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
  
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontend.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>