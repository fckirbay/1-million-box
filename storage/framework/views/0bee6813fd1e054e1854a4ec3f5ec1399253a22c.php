<?php $__env->startSection('styles'); ?>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
		<div class="terms-conditions-page">
			<div class="row">
				<div class="col-md-12 terms-conditions">
                	<h2 class="heading-title"><?php echo e($page->title); ?></h2>
                	<div class="">
                		<?php echo e($page->content); ?>

                	</div>
                </div>			
            </div><!-- /.row -->
		</div><!-- /.sigin-in-->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.theme.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>