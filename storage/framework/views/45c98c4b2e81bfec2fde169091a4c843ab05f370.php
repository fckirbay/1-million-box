<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b><?php echo e(config('app.version')); ?></b>
    </div>
    <strong>Copyright &copy; 2018 <a href="<?php echo e(url('/')); ?>"><?php echo e(config('app.name')); ?></a></strong>
  </footer>