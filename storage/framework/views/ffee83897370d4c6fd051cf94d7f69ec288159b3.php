<?php $__env->startSection('styles'); ?>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <table class="table-borders-dark">
        <tr>
            <th><?php echo app('translator')->getFromJson('general.city'); ?></th>
            <th><?php echo app('translator')->getFromJson('general.number'); ?></th>
            <th><?php echo app('translator')->getFromJson('general.earnings'); ?></th>
            <th><?php echo app('translator')->getFromJson('general.date'); ?></th>
        </tr>
        <?php $__empty_1 = true; $__currentLoopData = $openBoxes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
        <tr>
            <td><?php if(session('lang') == "tr"): ?><?php echo e($val->name_en); ?><?php else: ?><?php echo e($val->name_en); ?><?php endif; ?></td>
            <td><?php echo e($val->number); ?></td>
            <td><?php if($val->prize > 0): ?><strong><?php if(Sentinel::getUser()->currency == "eur"): ?>€<?php endif; ?><?php echo e(number_format($val->prize, 2, ',', '.')); ?> <?php if(Sentinel::getUser()->currency == "try"): ?>₺<?php endif; ?></strong><?php else: ?> - <?php endif; ?></td>
            <td><?php echo e(Carbon\Carbon::parse($val->created_at)->format('d M H:i')); ?></td>
        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
        <tr>
            <td colspan ="4"><?php echo app('translator')->getFromJson('general.you_havent_opened_any_boxes'); ?></td>
        </tr>
        <?php endif; ?>
    </table>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.mobile.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>