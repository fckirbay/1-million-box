<?php $__env->startSection('styles'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="row ">
   <div class="shopping-cart">
      
      <div class="shopping-cart-table ">
         <div class="table-responsive">
            <table class="table">
               <thead>
                  <tr>
                     <th class="cart-romove item">İşlem</th>
                     <th class="cart-description item">Ürün Resmi</th>
                     <th class="cart-product-name item">Ürün Adı</th>
                     <th class="cart-qty item">Ürün Adedi</th>
                     <th class="cart-sub-total item">Ara Toplam</th>
                     <th class="cart-total last-item">Genel Toplam</th>
                  </tr>
               </thead>
               <!-- /thead -->
               <?php echo Form::open(['url'=>'sepeti-guncelle', 'method'=>'post', 'class'=>'register-form outer-top-xs']); ?>

               <tfoot>
                  <tr>
                     <td colspan="7">
                        <div class="shopping-cart-btn">
                           <span class="">
                           <a href="<?php echo e(url('/')); ?>" class="btn btn-upper btn-primary outer-left-xs">Alışverişe Devam Et</a>
                           <?php if(Cart::count() > 0): ?>
                           <button type="submit" class="btn btn-upper btn-primary pull-right outer-right-xs">Sepeti Güncelle</button>
                           <?php endif; ?>
                           </span>
                        </div>
                        <!-- /.shopping-cart-btn -->
                     </td>
                  </tr>
               </tfoot>
               <tbody>
                  <?php $__empty_1 = true; $__currentLoopData = Cart::content(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                  <?php $product = App\Products::where('id', $row->id)->select('slug')->first();
                     $photo = App\ProductPhotos::where('product_id', $row->id)->where('is_featured', 1)->select('photo')->first(); ?>
                  <tr>
                     <td class="romove-item"><a href="<?php echo e(url('sepetten-cikar', $row->rowId)); ?>" title="cancel" class="icon"><i class="fa fa-trash-o"></i></a></td>
                     <td class="cart-image">
                        <a class="entry-thumbnail" href="<?php echo e(url($product->slug)); ?>">
                        <img src="<?php echo e(asset($photo->photo)); ?>" alt="">
                        </a>
                     </td>
                     <td class="cart-product-name-info">
                        <h4 class='cart-product-description'><a href="<?php echo e(url($product->slug)); ?>"><?php echo e($row->name); ?></a></h4>
                        <!--<div class="row">
                           <div class="col-sm-4">
                           	<div class="rating rateit-small"></div>
                           </div>
                           <div class="col-sm-8">
                           	<div class="reviews">
                           		(06 Yorum)
                           	</div>
                           </div>
                           </div>
                           <div class="cart-product-info">
                           				<span class="product-color">COLOR:<span>Blue</span></span>
                           </div>
                           -->
                     </td>
                     <td class="cart-product-quantity">
                        <div class="quant-input">
                           <div class="arrows">
                              <div class="arrow plus gradient"><span class="ir"><i class="icon fa fa-sort-asc"></i></span></div>
                              <div class="arrow minus gradient"><span class="ir"><i class="icon fa fa-sort-desc"></i></span></div>
                           </div>
                           <input type="text" value="<?php echo e($row->qty); ?>" min="1" max="3" name="row-<?php echo e($row->rowId); ?>">
                        </div>
                     </td>
                     <td class="cart-product-sub-total"><span class="cart-sub-total-price"><?php echo e(number_format($row->price * $row->qty / 1.18, 2, ',', '.')); ?>₺</span></td>
                     <td class="cart-product-grand-total"><span class="cart-grand-total-price"><?php echo e(number_format($row->price * $row->qty, 2, ',', '.')); ?>₺</span></td>
                  </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                  <tr>
                     <td colspan="6" style="text-align:center">Sepetiniz boş!</td>
                  </tr>
                  <?php endif; ?>
               </tbody>
               <?php echo Form::close(); ?>

               <!-- /tbody -->
            </table>
            <!-- /table -->
         </div>
      </div>
      <!-- /.shopping-cart-table -->
      <!--
         <div class="col-md-6 col-sm-12 estimate-ship-tax">
         	<table class="table">
         		<thead>
         			<tr>
         				<th>
         					<span class="estimate-title">İndirim Kodu</span>
         					<p>Eğer bir kuponunuz varsa kodunu aşağıya girin..</p>
         				</th>
         			</tr>
         		</thead>
         		<tbody>
         				<tr>
         					<td>
         						<div class="form-group">
         							<input type="text" class="form-control unicase-form-control text-input" placeholder="Kupon kodu..">
         						</div>
         						<div class="clearfix pull-right">
         							<button type="submit" class="btn-upper btn btn-primary">KUPONU ONAYLA</button>
         						</div>
         					</td>
         				</tr>
         		</tbody>
         	</table>
         </div>
         -->
      <?php if(Cart::count() > 0): ?>
      <div class="col-md-12 col-sm-12 cart-shopping-total" style="margin-top:-40px">
         <table class="table">
            <thead>
               <tr>
                  <th>
                     <div class="cart-sub-total">
                        Ara Toplam<span class="inner-left-md"><?php echo e(number_format(Cart::total() / 1.18, 2, ',', '.')); ?>₺</span>
                     </div>
                     <div class="cart-grand-total">
                        Genel Toplam<span class="inner-left-md"><?php echo e(number_format(Cart::total(), 2, ',', '.')); ?>₺</span>
                     </div>
                  </th>
               </tr>
            </thead>
            <!-- /thead -->
            <tbody>
               <tr>
                  <td>
                     <div class="cart-checkout-btn pull-right">
                        <a href="<?php echo e(url('odeme')); ?>" class="btn btn-primary checkout-btn">ÖDEME YAP</a>
                        <!--<span class="">Checkout with multiples address!</span>-->
                     </div>
                  </td>
               </tr>
            </tbody>
            <!-- /tbody -->
         </table>
         <!-- /table -->
      </div>
      <!-- /.cart-shopping-total -->
      <?php endif; ?>
   </div>
   <!-- /.shopping-cart -->
</div>
<!-- /.row -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<script>
   /*===================================================================================*/
   /*  QUANTITY
   /*===================================================================================*/
   
   $('.quant-input .plus').click(function() {
       var val = $(this).parent().next().val();
       val = parseInt(val) + 1;
       $(this).parent().next().val(val);
   });
   $('.quant-input .minus').click(function() {
       var val = $(this).parent().next().val();
       if (val > 1) {
           val = parseInt(val) - 1;
           $(this).parent().next().val(val);
       }
   });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.theme.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>