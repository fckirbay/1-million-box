				<!-- jQuery -->
				<script src="<?php echo e(asset('assets/frontend/js/jquery-3.2.1.min.js')); ?>"></script>
				<script src="<?php echo e(asset('assets/frontend/js/jquery-migrate-3.0.0.min.js')); ?>"></script>

				<!-- Plugins -->
				<script src="<?php echo e(asset('assets/frontend/js/popper.min.js')); ?>"></script>
				<script src="<?php echo e(asset('assets/frontend/js/bootstrap.min.js')); ?>"></script>
				<script src="<?php echo e(asset('assets/frontend/js/slick.min.js')); ?>"></script>
				<script src="<?php echo e(asset('assets/frontend/js/counter.js')); ?>"></script>
				<script src="<?php echo e(asset('assets/frontend/js/jquery.countdown.min.js')); ?>"></script>
				<script src="<?php echo e(asset('assets/frontend/js/menu-opener.js')); ?>"></script>
				<script src="<?php echo e(asset('assets/frontend/js/waypoints.js')); ?>"></script>
				<script src="<?php echo e(asset('assets/frontend/js/YouTubePopUp.jquery.js')); ?>"></script>
				<script src="<?php echo e(asset('assets/frontend/js/jquery.event.move.js')); ?>"></script>
				<script src="<?php echo e(asset('assets/frontend/js/SmoothScroll.js')); ?>"></script>
				<!-- custom -->
				<script src="<?php echo e(asset('assets/frontend/js/custom.js')); ?>"></script>
				<script src="<?php echo e(asset('assets/frontend/js/menu.js')); ?>"></script>
				<?php echo $__env->make('sweet::alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>