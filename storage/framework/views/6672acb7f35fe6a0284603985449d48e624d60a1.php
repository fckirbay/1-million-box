<?php $__env->startSection('styles'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
			<div class="page-content header-clear-medium">
					<div class="content bottom-5">
						<div class="link-list link-list-2 link-list-long-border">
							<a href="<?php echo e(url('buy-bonus-extra', 1)); ?>">
								<i class="fa fa-heart color-red2-dark"></i>
								<span><?php echo app('translator')->getFromJson('general.double_earnings'); ?></span>
								<em class="bg-blue2-dark">4 <?php echo app('translator')->getFromJson('general.coin'); ?><br /></em>
								<strong>Tur sonundaki kazançlarınızı ikiye katlar.</strong>
								<i class="fa fa-angle-right"></i>
							</a>
						</div>
						<div class="link-list link-list-2 link-list-long-border">
							<a href="<?php echo e(url('buy-bonus-extra', 2)); ?>">
								<i class="fa fa-heart color-red2-dark"></i>
								<span><?php echo app('translator')->getFromJson('general.pass'); ?></span>
								<em class="bg-blue2-dark">2 <?php echo app('translator')->getFromJson('general.coin'); ?><br /></em>
								<strong>Oyun sırasında bir kez pas hakkı tanır.</strong>
								<i class="fa fa-angle-right"></i>
							</a>
						</div>
						<div class="link-list link-list-2 link-list-long-border">
							<a href="<?php echo e(url('buy-bonus-extra', 3)); ?>">
								<i class="fa fa-heart color-red2-dark"></i>
								<span><?php echo app('translator')->getFromJson('general.show_result'); ?></span>
								<em class="bg-blue2-dark">3 <?php echo app('translator')->getFromJson('general.coin'); ?><br /></em>
								<strong>Bir turdaki kapalı kartı görmenizi sağlar.</strong>
								<i class="fa fa-angle-right"></i>
							</a>
						</div>
						<div class="link-list link-list-2 link-list-long-border">
							<a href="<?php echo e(url('buy-bonus-extra', 4)); ?>" class="no-border">
								<i class="fa fa-heart color-red2-dark"></i>
								<span><?php echo app('translator')->getFromJson('general.continue_left'); ?></span>
								<em class="bg-blue2-dark">4 <?php echo app('translator')->getFromJson('general.coin'); ?></em>
								<strong>Bir önceki turda kaldığınız yerden başlatır.</strong>
								<i class="fa fa-angle-right"></i>
							</a>
						</div>
					</div>
			</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
  
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.cardgame.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>