<?php $__env->startSection('styles'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
			<div class="page-content-black"></div>
			<div class="page-content">
				<div class="cover-wrapper cover-no-buttons">
					<div data-height="cover" class="caption bottom-0">
						<div class="caption-center">
							<div class="left-50 right-50">
								<h1 class="color-white center-text uppercase ultrabold fa-4x"><?php echo app('translator')->getFromJson('general.login'); ?></h1>
								<p class="color-highlight center-text font-12 under-heading bottom-30 top-5">
									<?php echo app('translator')->getFromJson('general.if_you_are_a_member_login'); ?>
								</p>
								<?php echo Form::open(['url'=>'login', 'method'=>'post']); ?>

								<div class="input-style input-light has-icon input-style-1 input-required">
									<i class="input-icon fa fa-user font-11"></i>
									<span style="background-color: transparent;"><?php echo app('translator')->getFromJson('general.username'); ?></span>
									<input type="name" name="username" placeholder="<?php echo app('translator')->getFromJson('general.username'); ?>" autocomplete="off">
								</div>
								<div class="input-style input-light has-icon input-style-1 input-required bottom-30">
									<i class="input-icon fa fa-lock font-11"></i>
									<span style="background-color: transparent;"><?php echo app('translator')->getFromJson('general.password'); ?></span>
									<input type="password" name="password" placeholder="<?php echo app('translator')->getFromJson('general.password'); ?>" autocomplete="off">
								</div>
								<div class="one-half">
									<a href="<?php echo e(url('signup')); ?>" class="font-11 color-white opacity-50"><?php echo app('translator')->getFromJson('general.create_an_account'); ?></a>
								</div>
								<div class="one-half last-column">
									<a href="<?php echo e(url('forgot-password')); ?>" class="text-right font-11 color-white opacity-50"><?php echo app('translator')->getFromJson('general.forgot_password'); ?></a>
								</div>
								<div class="clear"></div>
								<button type="submit" class="back-button button button-full button-m shadow-large button-round-small bg-highlight top-30 bottom-0" style="width:100%"><?php echo app('translator')->getFromJson('general.sign_in'); ?></button>
								<?php echo Form::close(); ?>

								<div class="divider top-30"></div>
								<a href="#" class="back-button button button-icon button-full button-xs shadow-large button-round-small font-11 bg-facebook top-30 bottom-0"><i class="fab fa-facebook-f"></i><?php echo app('translator')->getFromJson('general.sign_in_with_facebook'); ?></a>
								<a href="#" class="back-button button button-icon button-full button-xs shadow-large button-round-small font-11 bg-twitter top-10 bottom-40"><i class="fab fa-twitter"></i><?php echo app('translator')->getFromJson('general.sign_in_with_twitter'); ?></a>
							</div>
						</div>
					</div>
					<div class="caption-overlay bg-black opacity-90"></div>
					<div class="caption-bg" style="background-image:url(assets/cardgame/images/pictures/29t.jpg)"></div>
				</div>
			</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
  
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.cardgame.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>