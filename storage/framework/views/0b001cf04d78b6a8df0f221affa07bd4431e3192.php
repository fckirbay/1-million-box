        <div id="menu-3" class="menu-sidebar menu-dark menu-sidebar-right menu-sidebar-reveal">
            <div class="menu-scroll">
               <div class="sidebar-icons">
                  <a href="https://www.facebook.com/enabled.labs"><i class="font-10 fa-fw fab fa-facebook-f"></i></a>
                  <a href="https://twitter.com/iEnabled"><i class="font-10 fa-fw fab fa-twitter"></i></a>
                  <a href="tel:+1 234 567 890"><i class="font-10 fa-fw fa fa-phone"></i></a>
                  <a href="mailto:yourname@domain.com"><i class="font-11 fa-fw far fa-envelope"></i></a>
                  <a href="#" class="close-menu"><i class="font-10 fa-fw fa fa-times"></i></a>
               </div>
               <div class="sidebar-logo">
                  <a href="#"></a>
                  <em>Powerful Mobile Solutions</em>
               </div>
               <div class="sidebar-divider"></div>
               <div class="sidebar-separator">Highlights</div>
               <div class="menu-items menu-icons">
                  <a class="demo-color demo-red menu-item-active" href="#"><i class="color-red-light fa fa-dot-circle"></i>Red</a>
                  <a class="demo-color demo-green" href="#"><i class="color-green-light fa fa-dot-circle"></i>Green</a>
                  <a class="demo-color demo-blue" href="#"><i class="color-blue-light fa fa-dot-circle"></i>Blue</a>
                  <a class="demo-color demo-pink" href="#"><i class="color-pink-light fa fa-dot-circle"></i>Pink</a>
                  <a class="demo-color demo-orange" href="#"><i class="color-orange-light fa fa-dot-circle"></i>Orange</a>
                  <a class="demo-color demo-magenta" href="#"><i class="color-magenta-light fa fa-dot-circle"></i>Magenta</a>
                  <a class="demo-color demo-simple" href="#"><i class="color-gray-dark fa fa-dot-circle"></i>Minimalist</a>
               </div>
               <div class="sidebar-divider"></div>
               <div class="sidebar-separator">Menu Settings</div>
               <div class="menu-items menu-icons">
                  <a class="demo-icons demo-icons-on menu-item-active" href="#"><i class="fa fa-check"></i>Menu Icons On</a>
                  <a class="demo-icons demo-icons-off" href="#"><i class="fa fa-times"></i>Menu Icons Off</a>
               </div>
               <div class="sidebar-divider"></div>
               <div class="sidebar-separator">Menu Color</div>
               <div class="menu-items menu-icons">
                  <a class="demo-dark menu-item-active" href="#"><i class="fa fa-moon"></i>Sidebar Dark</a>
                  <a class="demo-light" href="#"><i class="fa fa-sun"></i>Sidebar Light</a>
               </div>
               <div class="sidebar-divider"></div>
               <div class="sidebar-separator">Header Settings</div>
               <div class="menu-items menu-icons">
                  <a class="demo-icons demo-header-light menu-item-active" href="#"><i class="fa fa-sun"></i>Header Light</a>
                  <a class="demo-icons demo-header-dark" href="#"><i class="fa fa-moon"></i>Header Dark</a>
               </div>
               <div class="sidebar-divider"></div>
               <div class="sidebar-separator">Footer Settings</div>
               <div class="menu-items menu-icons">
                  <a class="demo-icons demo-footer-dark menu-item-active" href="#"><i class="fa fa-sun"></i>Footer Dark</a>
                  <a class="demo-icons demo-footer-light" href="#"><i class="fa fa-moon"></i>Footer Light</a>
               </div>
               <div class="sidebar-divider"></div>
               <div class="sidebar-separator">Sidebar Effect</div>
               <div class="menu-items menu-icons">
                  <a href="navigations.html"><i class="fa fa-link font-10"></i>Sidebar 3D</a>
                  <a href="navigations.html"><i class="fa fa-link font-10"></i>Sidebar Over</a>
                  <a href="navigations.html"><i class="fa fa-link font-10"></i>Sidebar Push</a>
                  <a href="navigations.html"><i class="fa fa-link font-10"></i>Sidebar Reveal</a>
                  <a href="navigations.html"><i class="fa fa-link font-10"></i>Sidebar Parallax</a>
               </div>
               <div class="sidebar-divider"></div>
               <p class="sidebar-copyright">Copyright <span class="copyright-year"></span> Enabled. All Rights Reserved</p>
            </div>
         </div>