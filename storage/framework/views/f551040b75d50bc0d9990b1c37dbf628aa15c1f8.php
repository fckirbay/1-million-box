<?php $__env->startSection('styles'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
			<div class="page-content header-clear-medium" style="padding-top: 50px">
				<div class="content" style="margin: 0px 0px 0px">
					<div class="pricing-4 round-medium shadow-small" style="max-width: 1920px; border-radius: 0px !important; box-shadow: 0 0 0 0 rgba(0,0,0,0) !important;">
						<h3 class="pricing-value center-text bg-blue2-light bottom-10 color-white"><?php echo e(explode('.',Sentinel::getUser()->balance)[0]); ?><sup>.<?php echo e(explode('.',Sentinel::getUser()->balance)[1]); ?></sup></h3>
						<h2 class="pricing-subtitle center-text bg-blue2-light" style="margin-bottom: 0px; margin-top: -11px"><?php echo app('translator')->getFromJson('general.your_total_points'); ?></h2>
						<h1 class="pricing-title center-text bg-blue2-dark uppercase"><?php echo app('translator')->getFromJson('general.your_jokers'); ?></h1>
						<ul class="pricing-list bottom-30">
							<li style="font-weight: 500; float: left; width: 96%"><?php echo app('translator')->getFromJson('general.double_earnings'); ?> <span style="float: right; font-weight: bold"><em class="<?php if($jokers && $jokers->double_chance): ?> bg-green2-dark <?php else: ?> bg-red2-dark <?php endif; ?>" style="font-size: 15px;
	    font-weight: 700;
	    border-radius: 3px;
	    top: 47px;
	    line-height: 23px;
	    text-align: center;
	    font-style: normal;
	    color: #fff;
	    padding: 0 8px;"><?php if($jokers): ?><?php echo e($jokers->double_earnings); ?><?php else: ?> 0 <?php endif; ?></em></span></li>

							<li style="font-weight: 500; float: left; width: 96%"><?php echo app('translator')->getFromJson('general.pass'); ?> <span style="float: right; font-weight: bold"><em class="<?php if($jokers && $jokers->pass): ?> bg-green2-dark <?php else: ?> bg-red2-dark <?php endif; ?>" style="font-size: 15px;
	    font-weight: 700;
	    border-radius: 3px;
	    top: 47px;
	    line-height: 23px;
	    text-align: center;
	    font-style: normal;
	    color: #fff;
	    padding: 0 8px;"><?php if($jokers): ?><?php echo e($jokers->pass); ?><?php else: ?> 0 <?php endif; ?></em></span></li>

							<li style="font-weight: 500; float: left; width: 96%"><?php echo app('translator')->getFromJson('general.show_result'); ?> <span style="float: right; font-weight: bold"><em class="<?php if($jokers && $jokers->show_result): ?> bg-green2-dark <?php else: ?> bg-red2-dark <?php endif; ?>" style="font-size: 15px;
	    font-weight: 700;
	    border-radius: 3px;
	    top: 47px;
	    line-height: 23px;
	    text-align: center;
	    font-style: normal;
	    color: #fff;
	    padding: 0 8px;"><?php if($jokers): ?><?php echo e($jokers->show_result); ?><?php else: ?> 0 <?php endif; ?></em></span></li>

							<li style="font-weight: 500; float: left; width: 96%"><?php echo app('translator')->getFromJson('general.continue_left'); ?> <span style="float: right; font-weight: bold"><em class="<?php if($jokers && $jokers->continue_left): ?> bg-green2-dark <?php else: ?> bg-red2-dark <?php endif; ?>" style="font-size: 15px;
	    font-weight: 700;
	    border-radius: 3px;
	    top: 47px;
	    line-height: 23px;
	    text-align: center;
	    font-style: normal;
	    color: #fff;
	    padding: 0 8px;"><?php if($jokers): ?><?php echo e($jokers->continue_left); ?><?php else: ?> 0 <?php endif; ?></em></span></li>
						</ul>
						<a href="<?php echo e(url('buy-bonus')); ?>" class="button button-xs bg-blue2-dark button-center-large button-round-large uppercase"><?php echo app('translator')->getFromJson('general.buy_joker'); ?></a>
						<span class="center-text color-gray-dark small-text font-10 uppercase top-10 bottom-0"><?php echo app('translator')->getFromJson('general.25_off_all_jokers_now'); ?></span>
					</div>
					<div class="clear"></div>
				</div>
			</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
  
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.cardgame.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>