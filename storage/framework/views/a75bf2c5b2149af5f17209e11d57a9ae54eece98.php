<?php $__env->startSection('styles'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="page-content header-clear-medium" style="padding-top: 50px;">
	<br />
		<div class="content">
			<h3 class="bolder" style="text-align: center;"><?php echo e($method->name); ?></h3>
			<p style="text-align: center">
				<?php echo e($method->coin); ?> <?php echo app('translator')->getFromJson('general.coin'); ?>

				<br /><br />
				Ödülünüzü almak için aşağıdaki formu doldurun.
			</p>
			<?php echo Form::open(['url'=>'prize', 'method'=>'post', 'class'=>'register-form outer-top-xs']); ?>

				<input type="hidden" name="method" value="<?php echo e($method->url); ?>"/>
				<div class="input-style input-style-2 input-required">
					<em><i class="fa fa-angle-down"></i></em>
					<select name="lang">
						<option value="2000">2.000 Coin</option>
	                    <option value="5000">5.000 Coin</option>
	                    <option value="10000">10.000 Coin</option>
	                    <option value="20000">20.000 Coin</option>
	                    <option value="25000">25.000 Coin</option>
	                    <option value="50000">50.000 Coin</option>
	                    <option value="100000">100.000 Coin</option>
	                    <option value="250000">250.000 Coin</option>
	                    <option value="500000">500.000 Coin</option>
	                    <option value="1000000">1.000.000 Coin</option>
					</select>
				</div>
				<?php if($method->url == "papara"): ?>
				<div class="input-style input-style-2 has-icon input-required">
					<i class="input-icon far fa-edit"></i>
					<input type="text" name="papara_no" placeholder="Papara No" required>
				</div>
				<?php elseif($method->url == "ininal"): ?>
				<div class="input-style input-style-2 has-icon input-required">
					<i class="input-icon far fa-edit"></i>
					<input type="text" name="ininal_no" placeholder="Ininal No" required>
				</div>
				<?php endif; ?>
				<div class="input-style input-style-2 has-icon input-required">
					<i class="input-icon far fa-edit"></i>
					<input type="text" name="tc_no" placeholder="TC Kimlik No" required>
				</div>
				<button type="submit" class="back-button button button-full button-m shadow-large button-round-small bg-highlight top-30 bottom-0" style="width:100%"><?php echo app('translator')->getFromJson('general.send'); ?></button>
			<?php echo e(Form::close()); ?>

		</div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.cardgame.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>