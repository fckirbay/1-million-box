<!--<div class="header header-fixed header-logo-center">
	<?php if(URL::previous() !== URL::current()): ?>
		<a href="<?php echo e(URL::previous()); ?>" class="back-button header-icon header-icon-1"><i class="fas fa-arrow-left"></i></a>
	<?php endif; ?>
	<a href="<?php echo e(url('/')); ?>" class="header-title">Do <span style="opacity:0.3">or</span> Die</a>
	<?php if(Sentinel::check()): ?>
	<a href="#" class="header-icon header-icon-4" data-menu="menu-list">
		<i class="fa fa-user"></i>
	</a>
	<?php else: ?>
	<a href="<?php echo e(url('login')); ?>" class="header-icon header-icon-4"><i class="fas fa-sign-in-alt"></i></a>
	<?php endif; ?>
</div>
<div id="menu-list" class="menu menu-box-modal round-medium" data-menu-width="310" data-menu-height="257" data-menu-effect="menu-over" style="border-radius: 20px !important">
	<div class="link-list link-list-1 content bottom-0">
		<a href="<?php echo e(url('my-account')); ?>">
			<i class="fas fa-user color-facebook"></i>
			<span class="font-13"><?php echo app('translator')->getFromJson('general.my_account'); ?></span>
			<i class="fa fa-angle-right"></i>
		</a>
		<a href="<?php echo e(url('history')); ?>">
			<i class="fas fa-history color-twitter"></i>
			<span class="font-13"><?php echo app('translator')->getFromJson('general.game_history'); ?></span>
			<i class="fa fa-angle-right"></i>
		</a>
		<a href="<?php echo e(url('my-references')); ?>">
			<i class="fas fa-user-plus color-whatsapp"></i>
			<span class="font-13"><?php echo app('translator')->getFromJson('general.references'); ?></span>
			<i class="fa fa-angle-right"></i>
		</a>
		<a href="<?php echo e(url('support')); ?>">
			<i class="fas fa-comment-dots color-mail"></i>
			<span class="font-13"><?php echo app('translator')->getFromJson('general.support'); ?></span>
			<i class="fa fa-angle-right"></i>
		</a>
		<a href="<?php echo e(url('logout')); ?>" class="no-border">
			<i class="fas fa-sign-out-alt color-google"></i>
			<span class="font-13"><?php echo app('translator')->getFromJson('general.logout'); ?></span>
			<i class="fa fa-angle-right"></i>
		</a>
	</div>
</div>-->
<!-- Header -->
<section id="header">

	<!-- NAV AREA CSS -->
	<nav id="nav-part" class="navbar header-nav other-nav custom_nav full_nav sticky-top navbar-expand-md hidden-main">
		<div class="container">


			<a class="navbar-brand" href="index.html"><img src="<?php echo e(asset('assets/frontend/images/header-logo.png')); ?>" class="img-fluid logo-color" alt="logo"></a>

			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<div class="nav-res">
					<ul class="nav navbar-nav m-auto menu-inner">
						<li><a href="<?php echo e(url('/')); ?>" class="<?php echo e(Request::segment(1) === null ? 'active' : null); ?>">Home</a></li>
						<li><a href="<?php echo e(url('/play')); ?>" class="<?php echo e(Request::segment(1) === 'play' ? 'active' : null); ?>">Play</a></li>
						<li><a href="<?php echo e(url('/prizes')); ?>" class="<?php echo e(Request::segment(1) === 'prizes' ? 'active' : null); ?>">Prizes</a></li>
					</ul>
				</div>
				<?php if(!Sentinel::check()): ?>
				<ul class="login_menu navbar-right nav-sign">
					<li class="login"><a href="<?php echo e(url('/register')); ?>" class="btn-4 yellow-btn">Signup</a></li>
					<li class="login"><a href="<?php echo e(url('/login')); ?>" class="btn-4 pink-bg">Login</a></li>
				</ul>
				<?php else: ?>
				<ul class="login_menu navbar-right nav-sign">
					<li class="<?php echo e(url('/my-account')); ?>"><a href="#" class="btn-4 yellow-btn">My Account</a></li>
					<li class="<?php echo e(url('/logout')); ?>"><a href="#" class="btn-4 pink-bg">Logout</a></li>
				</ul>
				<?php endif; ?>
			</div>
		</div>
	</nav>
	<!-- mobile menu -->

	<nav id='cssmenu' class="hidden mobile">
		<div class="logo">
			<a href="<?php echo e(url('/')); ?>" class="logo">
				<img src="<?php echo e(asset('assets/frontend/images/header-logo.png')); ?>" class="img-responsive" alt="">
			</a>
		</div>
		<div id="head-mobile"></div>
		<div class="button"><i class="more-less fa fa-align-right"></i></div>
		<ul>
			<li><a href="<?php echo e(url('/')); ?>" class="active">Home</a></li>
			<li><a href="<?php echo e(url('/play')); ?>">Play</a></li>
			<li><a href="<?php echo e(url('/prizes')); ?>">Prizes</a></li>
			<?php if(!Sentinel::check()): ?>
				<li class="login"><a href="<?php echo e(url('/register')); ?>" class="btn-4 yellow-bg yellow-btn">Register</a></li>
				<li class="login"><a href="<?php echo e(url('/login')); ?>" class="btn-4 yellow-bg">Login</a></li>
			<?php else: ?>
				<li class="login"><a href="<?php echo e(url('/my-account')); ?>" class="btn-4 yellow-bg yellow-btn">My Account</a></li>
				<li class="login"><a href="<?php echo e(url('/logout')); ?>" class="btn-4 yellow-bg">Logout</a></li>
			<?php endif; ?>
		</li>
	</ul>

</nav>

<!-- End mobile menu -->
</section>
	<!-- Header End -->