        <div id="menu-share" class="menu-flyin menu-share">
            <div class="menu-flyin-content">
               <h1 class="uppercase bolder center-text bottom-10">Share the love</h1>
               <p class="center-text">
                  Share our page with the world, increase <br>your page exposure with the world.
               </p>
               <div class="menu-share-socials">
                  <a href="#" class="shareToFacebook"><i class="facebook-color fab fa-facebook-square"></i><em>Facebook</em></a>
                  <a href="#" class="shareToGooglePlus"><i class="google-color fab fa-google-plus-square"></i><em>Google +</em></a>
                  <a href="#" class="shareToTwitter"><i class="twitter-color fab fa-twitter-square"></i><em>Twitter</em></a>
                  <a href="#" class="shareToPinterest"><i class="pinterest-color fab fa-pinterest-square"></i><em>Pinterest</em></a>
                  <a href="#" class="shareToWhatsApp" data-action="share/whatsapp/share"><i class="whatsapp-color fab fa-whatsapp-square"></i><em>WhatsApp</em></a>
                  <a href="#" class="shareToMail"><i class="mail-color fa fa-envelope-square"></i><em>Email</em></a>
               </div>
            </div>
        </div>