<?php $__env->startSection('styles'); ?>
<style>
.btn-primary:hover,
.btn-primary:focus {
    background-color: #108d6f;
    border-color: #108d6f;
    box-shadow: none;
    outline: none;
}

.btn-primary {
    color: #fff;
    background-color: #007b5e;
    border-color: #007b5e;
}

section {
    padding: 30px 0;
}

section .section-title {
    text-align: center;
    color: #007b5e;
    margin-bottom: 50px;
    text-transform: uppercase;
}

#team .card {
    border: none;
    background: #ffffff;
}

.image-flip:hover .backside,
.image-flip.hover .backside {
    -webkit-transform: rotateY(0deg);
    -moz-transform: rotateY(0deg);
    -o-transform: rotateY(0deg);
    -ms-transform: rotateY(0deg);
    transform: rotateY(0deg);
    border-radius: .25rem;
}

.image-flip:hover .frontside,
.image-flip.hover .frontside {
    -webkit-transform: rotateY(180deg);
    -moz-transform: rotateY(180deg);
    -o-transform: rotateY(180deg);
    transform: rotateY(180deg);
}

.mainflip {
    -webkit-transition: 1s;
    -webkit-transform-style: preserve-3d;
    -ms-transition: 1s;
    -moz-transition: 1s;
    -moz-transform: perspective(1000px);
    -moz-transform-style: preserve-3d;
    -ms-transform-style: preserve-3d;
    transition: 1s;
    transform-style: preserve-3d;
    position: relative;
}

.frontside {
    position: relative;
    -webkit-transform: rotateY(0deg);
    -ms-transform: rotateY(0deg);
    z-index: 2;
    margin-bottom: 30px;
}

.backside {
    position: absolute;
    top: 0;
    left: 0;
    background: white;
    -webkit-transform: rotateY(-180deg);
    -moz-transform: rotateY(-180deg);
    -o-transform: rotateY(-180deg);
    -ms-transform: rotateY(-180deg);
    transform: rotateY(-180deg);
    -webkit-box-shadow: 5px 7px 9px -4px rgb(158, 158, 158);
    -moz-box-shadow: 5px 7px 9px -4px rgb(158, 158, 158);
    box-shadow: 5px 7px 9px -4px rgb(158, 158, 158);
}

.frontside,
.backside {
    -webkit-backface-visibility: hidden;
    -moz-backface-visibility: hidden;
    -ms-backface-visibility: hidden;
    backface-visibility: hidden;
    -webkit-transition: 1s;
    -webkit-transform-style: preserve-3d;
    -moz-transition: 1s;
    -moz-transform-style: preserve-3d;
    -o-transition: 1s;
    -o-transform-style: preserve-3d;
    -ms-transition: 1s;
    -ms-transform-style: preserve-3d;
    transition: 1s;
    transform-style: preserve-3d;
}

.frontside .card,
.backside .card {
    min-height: 120px;
}

.backside .card button {
    font-size: 18px;
    color: #007b5e !important;
    background-color: Transparent;
    background-repeat:no-repeat;
    border: none;
    cursor:pointer;
    overflow: hidden;
    outline:none;
}

.frontside .card .card-title,
.backside .card .card-title {
    color: #007b5e !important;
}

.frontside .card .card-body img {
    width: 120px;
    height: 120px;
    border-radius: 50%;
}
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="row">
   <!-- Team -->
   <section id="team" class="pb-5">
       <div class="container">
           <div class="row">
               <!-- Team member -->
               <?php $__empty_1 = true; $__currentLoopData = $addresses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
               <div class="col-xs-12 col-sm-6 col-md-4">
                   <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                       <div class="mainflip">
                           <div class="frontside">
                               <div class="card">
                                   <div class="card-body text-center">
                                       <h4 class="card-title"><br /><?php echo e($val->title); ?></h4>
                                       <p class="card-text"><?php echo e($val->town . ' / ' . $val->city); ?></p>
                                       <p class="card-text">(<?php echo e($val->name_surname); ?>)</p>
                                   </div>
                               </div>
                           </div>
                           <div class="backside">
                               <div class="card">
                                   <div class="card-body text-center mt-4">
                                       <h4 class="card-title"><?php echo e($val->title); ?></h4>
                                       <p class="card-text"><?php echo e($val->address . ' ' . $val->district . ' ' . $val->town . ' / ' . $val->city . ' ' . $val->postal_code); ?></p>
                                       <ul class="list-inline">
                                           <li class="list-inline-item">
                                               <a class="social-icon text-xs-center" target="_blank" href="#">
                                                   <i class="fa fa-edit"></i>
                                               </a>
                                           </li>
                                           <li class="list-inline-item">
                                               <?php echo Form::open(['url'=>'adres-sil', 'method'=>'post', 'class'=>'register-form outer-top-xs', 'id'=>'delete-address-'.$val->id]); ?>

                                                  <input type="hidden" name="address_id" value="<?php echo e($val->id); ?>"/>
                                                  <button class="social-icon text-xs-center delete-address" type="button" data-id="<?php echo e($val->id); ?>">
                                                      <i class="fa fa-times"></i>
                                                  </button>
                                               <?php echo Form::close(); ?>

                                           </li>
                                       </ul>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
               <div class="col-xs-12 col-sm-12 col-md-12" style="background-color:#fff; line-height:50px; font-weight:bold; font-size:14px; text-align:center">
                  Kayıtlı bir adresiniz yok!
                  <br />
                  Yeni bir adres eklemek için tıklayın!
               </div>
               <?php endif; ?>
               <!-- ./Team member -->
           </div>
       </div>
   </section>
<!-- Team -->
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<script>
   $( ".delete-address" ).click(function() {      
      
      var form =  $(this).closest("form");
      swal({
          title: "Emin misiniz?",
          text: "Bu adresi kalıcı olarak silmek istediğinize emin misiniz?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Evet, eminim!",
          cancelButtonText: "Hayır, vazgeçtim!",
          closeOnConfirm: false,
          closeOnCancel: false },
          function(isConfirm){
              if (isConfirm) {
                  form.submit();
              } else {
                  swal("Cancelled", "Your imaginary file is safe :)", "error");
              }
      });
      
   });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.theme.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>