<?php $__env->startSection('styles'); ?>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
            <?php $__empty_1 = true; $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
            <div href="#" class="invoice-box">
               <a data-toggle-box="invoice-<?php echo e($key); ?>" href="#" class="invoice-box-header">
               <em>#<?php echo e($val->id); ?></em>
               <strong><?php echo e(Carbon\Carbon::parse($val->created_at)->format('d M Y')); ?></strong>
               <span><?php echo e($val->name_surname); ?></span>
               <u class="color-black"><?php echo e(number_format($val->total_amount, 2, ',', '.')); ?>₺ <del class="color-gray-dark"><?php if($val->order_status == 0): ?>Onay Bekliyor
                    					  <?php elseif($val->order_status == 1): ?>Hazırlanıyor
                    					  <?php elseif($val->order_status == 2): ?>Kargolandı
                    					  <?php elseif($val->order_status == 3): ?>Tamamlandı
                    					  <?php elseif($val->order_status == 4): ?>İptal Edildi
                    					  <?php elseif($val->order_status == 5): ?>İade Edildi
                    					  <?php elseif($val->order_status == 6): ?>Reddedildi
                    					  <?php endif; ?></del></u>
               </a>
               <div id="invoice-<?php echo e($key); ?>" class="invoice-box-content">
                  <div class="decoration"></div>
                  <div class="invoice-box-item">
                     <em>İsim Soyisim:</em>
                     <strong><?php echo e($val->name_surname); ?></strong>
                  </div>
                  <div class="invoice-box-item">
                     <em>Toplam Tutar:</em>
                     <strong><?php echo e(number_format($val->total_amount, 2, ',', '.')); ?>₺</strong>
                  </div>
                  <div class="invoice-box-item">
                     <em>Kargo Ücreti:</em>
                     <strong><?php if($val->shipping_price > 0): ?><?php echo e(number_format($val->shipping_price, 2, ',', '.')); ?>₺<?php else: ?> Ücretsiz <?php endif; ?></strong>
                  </div>
                  <div class="invoice-box-item">
                     <em>Ödeme Durumu</em>
                     <strong><?php if($val->order_status == 0): ?>Onay Bekliyor
                    			  <?php else: ?> Onaylandı
                    			  <?php endif; ?></strong>
                  </div>
                  <div class="invoice-box-item">
                     <em>Ödeme Türü </em>
                     <strong><?php if($val->payment_method == 1): ?> Kredi Kartı <?php elseif($val->payment_method == 2): ?> Debit Kart <?php elseif($val->payment_method == 3): ?> Havale/EFT <?php endif; ?></strong>
                  </div>
                  <div class="invoice-box-item">
                     <em>Kargo Durumu</em>
                     <strong><?php if($val->shipping == null): ?> Kargo Bekleniyor
                     <?php else: ?> Takip Kodu: <?php echo e($val->shipping); ?> <?php endif; ?></strong>
                  </div>
                  <div class="invoice-box-item bottom-20">
                     <em>Ürünler</em>
                     <?php $__empty_2 = true; $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_2 = false; ?>
                     <a href="<?php echo e(url($products[$key]['slug'])); ?>"><?php echo e($products[$key]['title']); ?> <?php if($products[$key]['qty'] > 1): ?>x<?php echo e($products[$key]['qty']); ?><?php endif; ?><u><?php echo e(number_format($products[$key]['total_price'], 2, ',', '.')); ?>₺</u></a>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_2): ?>
                     
                     <?php endif; ?>
                  </div>
                  <div class="decoration"></div>
                  <a href="#" class="button bg-highlight button-rounded button-full button-s uppercase ultrabold bottom-30">Faturayı İndir</a>
               </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
            <div style="text-align:center; margin-top:20px">Hiç siparişiniz yok.</div>
            <?php endif; ?>
            
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
   
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.mobile.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>