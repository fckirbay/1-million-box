<?php $__env->startSection('styles'); ?>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
            <a href="#" class="cover-back-button back-button"><i class="fa fa-chevron-left font-12 color-white"></i></a>
            <a href="index.html" class="cover-home-button"><i class="fa fa-home font-14 color-white"></i></a>
            <div class="cover-item cover-item-full">
               <div class="cover-content cover-content-center">
                  <div class="page-login content-boxed content-boxed-padding top-0 bottom-0">
                     <h3 class="uppercase ultrabold top-10 bottom-0 center-text"><?php echo app('translator')->getFromJson('general.login'); ?></h3>
                     <p class="smaller-text bottom-30 center-text"><?php echo app('translator')->getFromJson('general.hello_there'); ?></p>
                     <?php echo Form::open(['url'=>'login', 'method'=>'post', 'class'=>'register-form outer-top-xs']); ?>

                     <div class="page-login-field top-15">
                        <i class="far fa-id-card"></i>
                        <input type="text" name="username" id="username" placeholder="<?php echo app('translator')->getFromJson('general.username'); ?>" style="text-transform: lowercase;" required>
                        <em>(<?php echo app('translator')->getFromJson('general.required'); ?>)</em>
                     </div>
                     <div class="page-login-field bottom-15">
                        <i class="fa fa-lock"></i>
                        <input type="password" name="password" placeholder="<?php echo app('translator')->getFromJson('general.password'); ?>" required>
                        <em>(<?php echo app('translator')->getFromJson('general.required'); ?>)</em>
                     </div>
                     <div class="page-login-links bottom-10">
                        <a class="forgot float-right" href="<?php echo e(url('signup')); ?>"><i class="fa fa-user float-right"></i><?php echo app('translator')->getFromJson('general.signup'); ?></a>
                        <!--<a class="create float-left" href="<?php echo e(url('forgot-password')); ?>"><i class="fa fa-eye"></i><?php echo app('translator')->getFromJson('general.forgot_password'); ?></a>-->
                        <div class="clear"></div>
                     </div>
                     <button type="submit" class="button button-red button-full button-rounded button-s uppercase ultrabold bottom-10"><?php echo app('translator')->getFromJson('general.login'); ?></button>
                     <?php echo Form::close(); ?>

                  </div>
               </div>
               <div class="cover-infinite-background" style="background-image:url(asset('assets/mobile/images/euro-bg.jpg'));"></div>
            <div class="cover-overlay overlay bg-black opacity-80"></div>
            </div>
            
            
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
   <script>
       $(function() {
            $('#username').on('keypress', function(e) {
                if (e.which == 32)
                    return false;
            });
            
            $(document).on('paste', '#username', function(e) {
              e.preventDefault();
              // prevent copying action
              var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
              withoutSpaces = withoutSpaces.replace(/\s+/g, '');
              $(this).val(withoutSpaces);
              // you need to use val() not text()
            });
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.mobile.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>