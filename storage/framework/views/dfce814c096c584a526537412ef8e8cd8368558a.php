<?php $__env->startSection('styles'); ?>
    <style>
       @-webkit-keyframes ticker {
  0% {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
    visibility: visible;
  }
  100% {
    -webkit-transform: translate3d(-100%, 0, 0);
    transform: translate3d(-100%, 0, 0);
  }
}
@keyframes  ticker {
  0% {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
    visibility: visible;
  }
  100% {
    -webkit-transform: translate3d(-100%, 0, 0);
    transform: translate3d(-100%, 0, 0);
  }
}
.ticker-wrap {
  width: 100%;
  overflow: scroll;
  height: 30px;
  background-color: rgba(0, 0, 0, 0.9);
  padding-left: 100%;
}
.ticker-wrap .ticker {
  display: inline-block;
  height: 30px;
  line-height: 30px;
  white-space: nowrap;
  padding-right: 100%;
  box-sizing: content-box;
  -webkit-animation-iteration-count: infinite;
  animation-iteration-count: infinite;
  -webkit-animation-timing-function: linear;
  animation-timing-function: linear;
  -webkit-animation-name: ticker;
  animation-name: ticker;
  -webkit-animation-duration: 30s;
  animation-duration: 100s;
}
.ticker-wrap .ticker__item {
  display: inline-block;
  padding: 0 2rem;
  font-size: 16px;
  color: white;
}
    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
            <!--<a data-menu="menu-3" href="#" class="demo-settings demo-settings-fixed bg-highlight">Settings</a>-->
            <div class="single-slider owl-carousel owl-no-dots bottom-30" style="margin-bottom:0px !important">
               <?php if(session('lang') == "tr"): ?>
               <div class="store-slide-1">
                  <a href="#" class="store-slide-image" style="margin: 0px auto 30px !important; max-width:100% !important"><img src="<?php echo e(asset('assets/mobile/images/slider/31.jpg')); ?>" alt="" style="width:100%; height:150px"></a>
               </div>
               <div class="store-slide-1">
                  <a href="#" class="store-slide-image" style="margin: 0px auto 30px !important; max-width:100% !important"><img src="<?php echo e(asset('assets/mobile/images/slider/21.jpg')); ?>" alt="" style="width:100%; height:150px"></a>
               </div>
               <div class="store-slide-1">
                  <a href="#" class="store-slide-image" style="margin: 0px auto 30px !important; max-width:100% !important"><img src="<?php echo e(asset('assets/mobile/images/slider/11.jpg')); ?>" alt="" style="width:100%; height:150px"></a>
               </div>
               <?php else: ?>
               <div class="store-slide-1">
                  <a href="#" class="store-slide-image" style="margin: 0px auto 30px !important; max-width:100% !important"><img src="<?php echo e(asset('assets/mobile/images/slider/32.jpg')); ?>" alt="" style="width:100%; height:150px"></a>
               </div>
               <div class="store-slide-1">
                  <a href="#" class="store-slide-image" style="margin: 0px auto 30px !important; max-width:100% !important"><img src="<?php echo e(asset('assets/mobile/images/slider/22.jpg')); ?>" alt="" style="width:100%; height:150px"></a>
               </div>
               <div class="store-slide-1">
                  <a href="#" class="store-slide-image" style="margin: 0px auto 30px !important; max-width:100% !important"><img src="<?php echo e(asset('assets/mobile/images/slider/12.jpg')); ?>" alt="" style="width:100%; height:150px"></a>
               </div>
               <?php endif; ?>
            </div>
            <div class="content-full">
               <!--<p class="center-text boxed-text-large bottom-30">
                  Plenty of categories to choose from. You can also use icon lists like the main menu for this section.
               </p>-->
               
               
               
               <div class="ticker-wrap" style="margin-top:-10px">
                  <div class="ticker">
                     <?php $__currentLoopData = $prizes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                     <?php 
                        if($val->currency == "try") {
                            $curr = "₺";
                        } else {
                            $curr = "€";
                        }
                     ?>
                        <div class="ticker__item"><?php echo app('translator')->getFromJson('general.win_x_prize', [ 'user' => $val->first_name, 'prize' => $curr . number_format($val->prize, 2, ',', '.') ]); ?></div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </div>
               </div>
               

               <div class="decoration decoration-margins"></div>
               
               <div class="decoration decoration-margins" style="margin-bottom:10px; margin-top:-10px"></div>
               
                <p class="center-text boxed-text-large">
                    <?php echo app('translator')->getFromJson('general.you_can_earn_5_extra_tickets'); ?>
                </p>
                
                <div class="decoration decoration-margins"></div>
                
               <div class="content">
                  <h3 class="uppercase bolder center-text"><?php echo app('translator')->getFromJson('general.active'); ?> <span class="color-highlight"><?php echo app('translator')->getFromJson('general.rooms'); ?></span></h3>
               </div>
               
               <div class="blog-categories blog-categories-3 bottom-20">
                  <?php $__currentLoopData = $rooms; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                     <a href="<?php echo e(url('room', $val->slug)); ?>"><strong></strong><em><?php if(Lang::locale() == "tr"): ?><?php echo e($val->name); ?><?php else: ?><?php echo e($val->name_en); ?><?php endif; ?></em><span class="bg-red-dark opacity-50"></span><img src="<?php echo e(asset('assets/mobile/images/empty.png')); ?>" data-src="<?php echo e(asset($val->photo)); ?>" class="preload-image responsive-image" alt="img"></a>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  <div class="clear"></div>
               </div>
               
            </div>
            <div class="decoration decoration-margins"></div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.mobile.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>