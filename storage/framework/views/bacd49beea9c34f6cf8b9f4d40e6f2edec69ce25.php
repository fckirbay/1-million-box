<?php $__env->startSection('styles'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
	<div class="page-content header-clear-medium">
		<br />
		<?php $__empty_1 = true; $__currentLoopData = $paymentMethods; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
		<a href="<?php echo e(url('prize', $val->url)); ?>">
			<div class="content-boxed content-boxed-full bottom-20">
				<img data-src="<?php echo e(asset($val->photo)); ?>" src="<?php echo e(asset($val->photo)); ?>" class="preload-image responsive-image shadow-small bottom-15" alt="img">
				<div class="content" style="text-align: center">
					<p style="font-weight: bold; font-size: 20px; margin-bottom: 10px"><?php echo e($val->name); ?></p>
					<p style="margin-bottom: 0px; font-size: 16px"><?php echo e($val->coin); ?> <?php echo app('translator')->getFromJson('general.coin'); ?></p>
				</div>
			</div>
		</a>
		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
		<?php endif; ?>
	</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.cardgame.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>