<!-- ============================================== NEWSLETTER ============================================== -->
<div class="sidebar-widget newsletter wow fadeInUp outer-bottom-small">
	<h3 class="section-title">E-Bülten</h3>
	<div class="sidebar-widget-body outer-top-xs">
		<p>E-Bültenimize kaydolun!</p>
        <form role="form">
        	 <div class="form-group">
			    <label class="sr-only" for="exampleInputEmail1">E-Posta Adresi</label>
			    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="E-Posta Adresiniz">
			  </div>
			<button class="btn btn-primary">Abone Ol</button>
		</form>
	</div><!-- /.sidebar-widget-body -->
</div><!-- /.sidebar-widget -->
<!-- ============================================== NEWSLETTER: END ============================================== -->