<?php $__env->startSection('styles'); ?>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="contact-page">
		<div class="row">
			
				<div class="col-md-12 contact-map outer-bottom-vs">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3124.4491216360548!2d27.174580715183588!3d38.454191480551664!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14b962a90d76ecff%3A0xb16364e47894b32f!2sFolkart+Towers!5e0!3m2!1str!2str!4v1522697634675" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>				</div>
				<div class="col-md-9 contact-form">
	<div class="col-md-12 contact-title">
		<h4>İletişim Formu</h4>
	</div>
	
	
	
	<?php if(Session::has('result')): ?>
        <div class="alert alert-success" style="text-align:center"><?php echo e(Session::get('result')); ?></div>
        <br />
    <?php endif; ?>
    
		<?php echo Form::open(['url'=>'bize-ulasin', 'method'=>'post', 'class'=>'form-horizontal register-form outer-top-xs']); ?>

		
	<?php if($errors->any()): ?>
        <div class="alert alert-danger" style="text-align:center">
            <ul>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
    <?php endif; ?>
    
	<div class="col-md-5">
			<div class="form-group">
		    <label class="info-title" for="exampleInputName">İsim Soyisim <span>*</span></label>
		    <input type="text" name="name_surname" class="form-control unicase-form-control text-input" id="exampleInputName" placeholder="">
		  </div>
	</div>
	<div class="col-md-5 col-md-offset-1">
			<div class="form-group">
		    <label class="info-title" for="exampleInputEmail1">E-Posta Adresi <span>*</span></label>
		    <input type="email" name="email" class="form-control unicase-form-control text-input" id="exampleInputEmail1" placeholder="">
		  </div>
	</div>
	<div class="col-md-5">
			<div class="form-group">
		    <label class="info-title" for="exampleInputTitle">Telefon</label>
		    <input type="text" name="phone" class="form-control unicase-form-control text-input" id="exampleInputTitle" placeholder="">
		  </div>
	</div>
	<div class="col-md-5 col-md-offset-1">
			<div class="form-group">
		    <label class="info-title" for="exampleInputTitle">Konu Başlığı <span>*</span></label>
		    <input type="text" name="subject" class="form-control unicase-form-control text-input" id="exampleInputTitle" placeholder="">
		  </div>
	</div>
	<div class="col-md-11">
			<div class="form-group">
		    <label class="info-title" for="exampleInputComments">Mesajınız <span>*</span></label>
		    <textarea name="message" class="form-control unicase-form-control" id="exampleInputComments"></textarea>
		  </div>
	</div>
	<div class="col-md-12 outer-bottom-small m-t-20">
		<button type="submit" class="btn-upper btn btn-primary checkout-page-button">Gönder</button>
	</div>
	<?php echo Form::close(); ?>

</div>
<div class="col-md-3 contact-info">
	<div class="contact-title">
		<h4>Information</h4>
	</div>
	<div class="clearfix address">
		<span class="contact-i"><i class="fa fa-map-marker"></i></span>
		<span class="contact-span">Adalet Mahallesi, Folkart Towers, No:201, Bayraklı / İZMİR</span>
	</div>
	<div class="clearfix phone-no">
		<span class="contact-i"><i class="fa fa-mobile"></i></span>
		<span class="contact-span">(555) 555 55 55<br>(555) 555 55 55</span>
	</div>
	<div class="clearfix email">
		<span class="contact-i"><i class="fa fa-envelope"></i></span>
		<span class="contact-span"><a href="#">info@bydukkan.com</a></span>
	</div>
</div>			</div><!-- /.contact-page -->
		</div><!-- /.row -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.theme.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>