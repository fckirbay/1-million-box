<!-- ============================================== BRANDS CAROUSEL ============================================== -->
<div id="brands-carousel" class="logo-slider wow fadeInUp">

		<div class="logo-slider-inner">	
			<div id="brand-slider" class="owl-carousel brand-slider custom-carousel owl-theme">
				<div class="item">
					<a href="#" class="image">
						<img data-echo="<?php echo e(asset('assets/theme/images/brands/pullandbear.png')); ?>" src="<?php echo e(asset('assets/theme/images/blank.gif')); ?>" alt="">
					</a>	
				</div><!--/.item-->

				<div class="item">
					<a href="#" class="image">
						<img data-echo="<?php echo e(asset('assets/theme/images/brands/pullandbear.png')); ?>" src="<?php echo e(asset('assets/theme/images/blank.gif')); ?>" alt="">
					</a>	
				</div><!--/.item-->

				<div class="item">
					<a href="#" class="image">
						<img data-echo="<?php echo e(asset('assets/theme/images/brands/pullandbear.png')); ?>" src="<?php echo e(asset('assets/theme/images/blank.gif')); ?>" alt="">
					</a>	
				</div><!--/.item-->

				<div class="item">
					<a href="#" class="image">
						<img data-echo="<?php echo e(asset('assets/theme/images/brands/pullandbear.png')); ?>" src="<?php echo e(asset('assets/theme/images/blank.gif')); ?>" alt="">
					</a>	
				</div><!--/.item-->

				<div class="item">
					<a href="#" class="image">
						<img data-echo="<?php echo e(asset('assets/theme/images/brands/pullandbear.png')); ?>" src="<?php echo e(asset('assets/theme/images/blank.gif')); ?>" alt="">
					</a>	
				</div><!--/.item-->

				<div class="item">
					<a href="#" class="image">
						<img data-echo="<?php echo e(asset('assets/theme/images/brands/pullandbear.png')); ?>" src="<?php echo e(asset('assets/theme/images/blank.gif')); ?>" alt="">
					</a>	
				</div><!--/.item-->

				<div class="item">
					<a href="#" class="image">
						<img data-echo="<?php echo e(asset('assets/theme/images/brands/pullandbear.png')); ?>" src="<?php echo e(asset('assets/theme/images/blank.gif')); ?>" alt="">
					</a>	
				</div><!--/.item-->

				<div class="item">
					<a href="#" class="image">
						<img data-echo="<?php echo e(asset('assets/theme/images/brands/pullandbear.png')); ?>" src="<?php echo e(asset('assets/theme/images/blank.gif')); ?>" alt="">
					</a>	
				</div><!--/.item-->

				<div class="item">
					<a href="#" class="image">
						<img data-echo="<?php echo e(asset('assets/theme/images/brands/pullandbear.png')); ?>" src="<?php echo e(asset('assets/theme/images/blank.gif')); ?>" alt="">
					</a>	
				</div><!--/.item-->

				<div class="item">
					<a href="#" class="image">
						<img data-echo="<?php echo e(asset('assets/theme/images/brands/pullandbear.png')); ?>" src="<?php echo e(asset('assets/theme/images/blank.gif')); ?>" alt="">
					</a>	
				</div><!--/.item-->
		    </div><!-- /.owl-carousel #logo-slider -->
		</div><!-- /.logo-slider-inner -->
	
</div><!-- /.logo-slider -->
<!-- ============================================== BRANDS CAROUSEL : END ============================================== -->