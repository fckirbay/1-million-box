<?php $__env->startSection('styles'); ?>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="container">
		<div class="x-page inner-bottom-sm">
			<div class="row">
				<div class="col-md-12 x-text text-center">
					<h1>404</h1>
					<p>Üzgünüz, aradığınız sayfa bulunamadı ya da aktif değil. </p>
					<form role="form" class="outer-top-vs outer-bottom-xs">
                        <input placeholder="Arama yapın..." autocomplete="off">
                        <button class="  btn-default le-button">Ara</button>
                    </form>
					<a href="<?php echo e(url('/')); ?>"><i class="fa fa-home"></i> Anasayfaya Dön</a>
				</div>
			</div><!-- /.row -->
		</div><!-- /.sigin-in-->
	</div><!-- /.container -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.theme.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>