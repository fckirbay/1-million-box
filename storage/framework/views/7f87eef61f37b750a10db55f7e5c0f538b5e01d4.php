    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
    <title><?php echo app('translator')->getFromJson('general.lucky_box'); ?> || <?php echo app('translator')->getFromJson('general.new_game'); ?> </title>
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/mobile/styles/style.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/mobile/fonts/css/fontawesome-all.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/mobile/styles/framework.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/theme/plugins/sweetalert/dist/sweetalert.css')); ?>">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <script>android.fontSizeNormal();</script>