<?php $__env->startSection('styles'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
			<div class="page-content header-clear-medium" style="padding-top: 50px; padding-bottom: 0px">
				<div class="content-boxed" style="margin: 0px 0px 0px; border-radius: 0px !important">
					<div class="content bottom-5">
						<div class="link-list link-list-2 link-list-long-border">
							<?php $__empty_1 = true; $__currentLoopData = $references; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
							<a href="#" class="no-border">
								<?php if($val->verification == 0): ?>
									<i class="fa fa-times-circle color-red2-dark"></i>
								<?php else: ?>
									<i class="fa fa-check-circle color-green2-dark"></i>
								<?php endif; ?>
								<span><?php echo e($val->first_name); ?></span>
								<strong><?php echo e(Carbon\Carbon::parse($val->created_at)->format('d M H:i')); ?></strong>
								<i class="fa fa-angle-right"></i>
							</a>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
							Kayıt yok.
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
  
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.cardgame.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>