<?php $__env->startSection('styles'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="page-content header-clear-medium content-boxed" style="margin: 0px 0px 0px; border-radius: 0px">
	<div class="content bottom-0" style="height:100%; overflow:hidden">
		<?php $__empty_1 = true; $__currentLoopData = $messages->reverse(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
            <?php if($val->owner == 1): ?>
	            <div class="speech-bubble speech-right color-black">
					<?php echo e($val->message); ?>

					<em class="speech-read bottom-20" style="margin-bottom: 0px !important;"><?php echo e(Carbon\Carbon::parse($val->date)->format('d M H:i')); ?></em>
				</div>
				<div class="clear"></div>
	        <?php else: ?>
		        <div class="speech-bubble speech-left bg-highlight">
					<?php echo e($val->message); ?>

					<em class="speech-read bottom-20" style="margin-bottom: 0px !important;"><?php echo e(Carbon\Carbon::parse($val->date)->format('d M H:i')); ?></em>
				</div>
				<div class="clear"></div>
	        <?php endif; ?>
	    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
	    <?php endif; ?>
	</div>
	<div class="content" style="bottom:0;position:fixed; width:92%">
		<div class="input-style input-style-2 input-required">
			<span>Enter your Message</span>
			<em>(required)</em>
			<textarea placeholder=""></textarea>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.cardgame.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>