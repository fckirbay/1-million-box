<?php $__env->startSection('styles'); ?>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
            <div class="single-store-slider owl-carousel owl-has-dots gallery store-product-slider">
                <?php $__empty_1 = true; $__currentLoopData = $photos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                <a class="show-gallery" href="<?php echo e(asset($val->photo)); ?>"><img src="<?php echo e(asset($val->photo)); ?>" alt="<?php echo e($val->title); ?> <?php echo e($key); ?>"></a>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <?php endif; ?>
            </div>
            <div class="content">
               <div class="store-product">
                  <h2 class="store-product-title"><?php echo e($product->title); ?></h2>
                  <span class="store-product-price">
                      <?php if($product->old_price > 0): ?>
                        <em><del><?php echo e(number_format($product->old_price, 2, ',', '.')); ?>₺</del></em>
                      <?php endif; ?>
                      <strong><?php echo e(number_format($product->price, 2, ',', '.')); ?>₺</strong></span>
                  <span class="store-product-reviews">
                  <strong><?php echo e($rate); ?><i class="fa fa-star"></i></strong>
                  <a href="#"><?php echo e(count($comments)); ?> Yorum</a>
                  </span>
                  <?php if($val->old_price > 0): ?>
                      <span class="store-product-discount bg-highlight">%<?php echo e(intval(($val->old_price - $val->price) / $val->old_price * 100)); ?></span>
                  <?php endif; ?>
                  
               </div>
               <!--
               <div class="store-product-select-box select-box select-box-2 bottom-10 top-25">
                  <select>
                     <option value="option-1">Select Storage Capacity</option>
                     <option value="option-2">$399 - 64 GB</option>
                     <option value="option-3">$299 - 32 GB - Best Deal</option>
                     <option value="option-4">$299 - 16 GB</option>
                  </select>
               </div>
               <div class="store-product-select-box select-box select-box-2 bottom-30">
                  <select>
                     <option value="option-a">Select Shipping / Pick Up</option>
                     <option value="option-b">Personal Pickup - Free</option>
                     <option value="option-c">FedEx - Free (2-4 Days)</option>
                     <option value="option-d">Drone - Instant Delivery</option>
                  </select>
               </div>
               -->
               <div class="decoration top-30 bottom-30"></div>
               <a href="#" class="button button-blue button-icon button-full button-sm top-15 button-rounded uppercase ultrabold"><i class="fa fa-shopping-cart"></i>Sepete Ekle</a>
               <a href="#" class="button bg-highlight button-icon button-full button-sm top-15 bottom-30 button-rounded uppercase ultrabold"><i class="fa fa-heart"></i>Favorilerime Ekle</a>
               <div class="decoration top-30"></div>
               <!--
               <h4 class="bold bottom-30 color-highlight">Product Highlights</h4>
               <ul class="font-icon-list">
                  <li><i class="fa fa-truck color-blue-dark"></i>Free Shipping</li>
                  <li><i class="fa fa-heart color-red-dark"></i>Extended Warranty</li>
                  <li><i class="fa fa-money-bill-alt color-green2-dark"></i>Money Back Guarantee</li>
                  <li><i class="fa fa-coffee color-brown-dark"></i>Coffee on the House from Us</li>
               </ul>
               <div class="decoration"></div>
               -->
               <h4 class="bold bottom-10 color-highlight">Ürün Açıklaması</h4>
               <div class="container">
                  <p class="bottom-10">
                     <?php echo $product->description; ?>

                     <!--
                     <br>
                     <br>
                     <a href="#" class="read-more-show color-highlight">Devamını Oku <i class="fa fa-caret-down"></i></a>
                     -->
                  </p>
                  <!--
                  <p class="read-more-box">
                     Devamını oku...
                  </p>
                  -->
               </div>
               
               <div class="decoration"></div>
               <h4 class="bold bottom-10 color-highlight">Kullanıcı Yorumları</h4>
               <em class="small-text color-gray-dark bottom-30">
               Yalnızca ürünü satın alan byDükkan üyelerinin yorumları listelenir.
               </em>
               <?php $__empty_1 = true; $__currentLoopData = $comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
               <div class="review-6 container">
                  <h1 class="font-16 capitalize regularbold">John Doe</h1>
                  <em class="color-highlight font-11 small-text">Onaylı Üye</em>
                  <p>
                     <?php echo e($val->comment); ?>

                  </p>
                  <h3>Product Quality</h3>
                  <div class="review-stars"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></div>
                  <div class="clear"></div>
                  <h3>Delivery Times</h3>
                  <div class="review-stars"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></div>
                  <div class="clear"></div>
                  <h3>Ease of User</h3>
                  <div class="review-stars"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></div>
                  <div class="clear"></div>
               </div>
               <div class="decoration"></div>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
               <div style="text-align:center">Hiç yorum yok.</div>
               <?php endif; ?>
            </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
   
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.mobile.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>