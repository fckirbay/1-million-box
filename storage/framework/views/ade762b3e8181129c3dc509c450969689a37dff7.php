<?php $__env->startSection('styles'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
	<!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Hatalar <a class="btn btn-danger btn-xs" href="<?php echo e(url('admin/delete-errors')); ?>">Tümünü Sil</a></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th style="width:50%">Hata Mesajı</th>
                  <th class="orta" style="width:10%">Hata Kodu</th>
                  <th class="orta" style="width:20%">Dosya</th>
                  <th class="orta" style="width:10%">Satır</th>
                  <th class="orta" style="width:10%">Tarih</th>
                </tr>
                <?php $__empty_1 = true; $__currentLoopData = $errors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                <tr>
                  <td><?php echo e($val->message); ?></td>
                  <td class="orta"><?php echo e($val->code); ?></td>
                  <td class="orta"><?php echo e($val->file); ?></td>
                  <td class="orta"><?php echo e($val->row); ?></td>
                  <td class="orta"><?php echo e(Carbon\Carbon::parse($val->date)->format('d/m/Y H:i:s')); ?></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <tr>
                    <td colspan="5" style="text-align:center">Hiç kayıt bulunamadı.</td>
                </tr>
                <?php endif; ?>
              </table>
            </div>
            <?php echo e($errors->links()); ?>

            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
  <?php echo $__env->make('sweet::alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>