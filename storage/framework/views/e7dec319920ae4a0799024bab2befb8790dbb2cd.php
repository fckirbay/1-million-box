<?php $__env->startSection('styles'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
	<!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Ödüller</h3>
              <?php echo Form::open(['url'=>'admin/rewardeds', 'method' => 'get', 'style' => 'float:right', 'id'=>'search-form']); ?>

              <div class="box-tools" style="width:250px">

                <div class="row">
                  <div class="col-xs-8">
                    <select class="form-control" name="days">
                      <option value="1095">Tüm Zamanlar</option>
                      <option value="3">Son 3 Gün</option>
                      <option value="7" selected>Son 1 Hafta</option>
                      <option value="14">Son 2 Hafta</option>
                      <option value="30">Son 1 Ay</option>
                      <option value="60">Son 2 Ay</option>
                      <option value="90">Son 3 Ay</option>
                      <option value="180">Son 6 Ay</option>
                      <option value="365">Son 1 Yıl</option>
                    </select>
                  </div>
                  <div class="col-xs-2">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
              <?php echo Form::close(); ?>

            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th style="width:25%">Kullanıcı ID</th>
                  <th class="orta" style="width:10%">Toplam İzleme</th>
                </tr>
                <?php $__empty_1 = true; $__currentLoopData = $rewardeds; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                <tr>
                  <td><?php echo e($val->username); ?></td>
                  <td class="orta"><?php echo e($val->total); ?></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <tr>
                    <td colspan="2" style="text-align:center">Hiç kayıt bulunamadı.</td>
                </tr>
                <?php endif; ?>
              </table>
            </div>
            <?php echo $rewardeds->appends(Request::capture()->except('page'))->render(); ?>

            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
  <script>
    $('#search-form').submit(function() {
        $(this).find(":input").filter(function(){return !this.value;}).attr("disabled", "disabled");
    });
    // Un-disable form fields when page loads, in case they click back after submission or client side validation
    $('#search-form').find(":input").prop("disabled", false);
  </script>
  <?php echo $__env->make('sweet::alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>