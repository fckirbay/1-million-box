<?php $__env->startSection('styles'); ?>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
            <div class="content" style="margin-top:10px">
               <?php echo Form::open(['url'=>'my-account', 'method'=>'post', 'class'=>'register-form outer-top-xs']); ?>

                   <div class="select-box select-box-1">
                        <em><?php echo app('translator')->getFromJson('general.language'); ?></em>
                        <select name="lang">
                            <option value="en" <?php if(Sentinel::getUser()->lang === "en"): ?> selected <?php endif; ?>><?php echo app('translator')->getFromJson('general.english'); ?></option>
                            <option value="de" <?php if(Sentinel::getUser()->lang === "de"): ?> selected <?php endif; ?>><?php echo app('translator')->getFromJson('general.deutsch'); ?></option>
                            <option value="in" <?php if(Sentinel::getUser()->lang === "in"): ?> selected <?php endif; ?>><?php echo app('translator')->getFromJson('general.hindi'); ?></option>
                            <!--
                            <option value="in" <?php if(Sentinel::getUser()->lang === "in"): ?> selected <?php endif; ?>><?php echo app('translator')->getFromJson('general.french'); ?></option>
                            <option value="fr" <?php if(Sentinel::getUser()->lang === "fr"): ?> selected <?php endif; ?>><?php echo app('translator')->getFromJson('general.french'); ?></option>
                            <option value="it" <?php if(Sentinel::getUser()->lang === "it"): ?> selected <?php endif; ?>><?php echo app('translator')->getFromJson('general.italian'); ?></option>
                            <option value="es" <?php if(Sentinel::getUser()->lang === "es"): ?> selected <?php endif; ?>><?php echo app('translator')->getFromJson('general.spanish'); ?></option>
                            <option value="ru" <?php if(Sentinel::getUser()->lang === "ru"): ?> selected <?php endif; ?>><?php echo app('translator')->getFromJson('general.russian'); ?></option>
                            <option value="zh" <?php if(Sentinel::getUser()->lang === "zh"): ?> selected <?php endif; ?>><?php echo app('translator')->getFromJson('general.chinese'); ?></option>
                            -->
                            <option value="tr" <?php if(Sentinel::getUser()->lang === "tr"): ?> selected <?php endif; ?>><?php echo app('translator')->getFromJson('general.turkish'); ?></option>
                        </select>
                   </div>
                   <div class="select-box select-box-1">
                        <em><?php echo app('translator')->getFromJson('general.rewarded_ad_notifications'); ?></em>
                        <select name="is_notify">
                            <option value="1" <?php if(Sentinel::getUser()->is_notify == 1): ?> selected <?php endif; ?>><?php echo app('translator')->getFromJson('general.yes'); ?></option>
                            <option value="0" <?php if(Sentinel::getUser()->is_notify == 0): ?> selected <?php endif; ?>><?php echo app('translator')->getFromJson('general.no'); ?></option>
                        </select>
                   </div>
                   <div class="input-simple-1 has-icon input-red bottom-30"><em><?php echo app('translator')->getFromJson('general.email_address'); ?></em><i class="fa fa-at"></i><input type="email" name="email_2" placeholder="<?php echo app('translator')->getFromJson('general.email_address'); ?>" value="<?php echo e(Sentinel::getUser()->email_2); ?>"></div>
                   <div class="input-simple-1 has-icon input-green bottom-30"><em><?php echo app('translator')->getFromJson('general.username'); ?></em><i class="fa fa-user"></i><input type="text" name="name_surname" placeholder="Jonh Doe" value="<?php echo e(Sentinel::getUser()->first_name); ?>" disabled></div>
                   <div class="input-simple-1 has-icon input-red bottom-30"><em><?php echo app('translator')->getFromJson('general.mobile_phone'); ?></em><i class="fa fa-mobile"></i><input type="text" name="phone" class="phone" placeholder="" minlength="13" value="<?php echo e(Sentinel::getUser()->phone); ?>" disabled></div>
                   
                   <div class="input-simple-1 has-icon input-red bottom-30"><em><?php echo app('translator')->getFromJson('general.password'); ?></em><i class="fas fa-asterisk"></i><input type="password" name="password" class="phone" placeholder="<?php echo app('translator')->getFromJson('general.password'); ?>" minlength="6" maxlength="15"></div>
                   <div class="input-simple-1 has-icon input-red bottom-30"><em><?php echo app('translator')->getFromJson('general.password_again'); ?></em><i class="fas fa-asterisk"></i><input type="password" name="password_confirm" class="phone" placeholder="<?php echo app('translator')->getFromJson('general.password_again'); ?>" minlength="6" maxlength="15"></div>
                   <button type="submit" class="button button-green" style="width:100%"><?php echo app('translator')->getFromJson('general.save'); ?></button>
               <?php echo Form::close(); ?>

               <div class="clear"></div>
            </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.mobile.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>