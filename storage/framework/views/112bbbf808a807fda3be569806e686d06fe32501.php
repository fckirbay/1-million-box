<?php $__env->startSection('styles'); ?>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
            <?php $__empty_1 = true; $__currentLoopData = $wishlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
            <div class="store-slide-2">
               <a href="#" class="store-slide-image">
               <img class="preload-image" src="<?php echo e(asset('assets/mobile/images/empty.png')); ?>" data-src="<?php echo e(asset($val->photo)); ?>" alt="<?php echo e($val->title); ?>">
               </a>
               <div class="store-slide-title">
                  <strong><?php echo e($val->title); ?></strong>
                  <em class="color-gray-dark"><?php echo e($val->subtitle); ?></em>
               </div>
               <div class="store-slide-button">
                  <strong><?php echo e(number_format($val->price, 2, ',', '.')); ?>₺</strong>
                  <a href="#"><i class="fa fa-times color-black"></i></a>
               </div>
            </div>
            <div class="decoration bottom-0"></div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
            <p style="text-align:center">Favori listeniz boş!</p>
            <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
   
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.mobile.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>