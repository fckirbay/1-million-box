<?php $__env->startSection('styles'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="row profile">
   <div class="col-md-12" style="padding-right:0px">
      <div class="my-wishlist-page">
         <div class="profile-content">
            <div class="table-responsive">
               <table class="table">
                  <thead>
                     <tr>
                        <th colspan="4" class="heading-title">Favorilerim</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php $__empty_1 = true; $__currentLoopData = $wishlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                     <tr id="wish-<?php echo e($val->wishId); ?>">
                        <td class="col-md-2"><img src="<?php echo e(asset($val->photo)); ?>" alt="<?php echo e($val->title); ?>"></td>
                        <td class="col-md-7">
                           <div class="product-name" style="font-size:16px"><a href="#"><?php echo e($val->title); ?></a></div>
                           <!--<div class="rating">
                              <i class="fa fa-star rate"></i>
                              <i class="fa fa-star rate"></i>
                              <i class="fa fa-star rate"></i>
                              <i class="fa fa-star rate"></i>
                              <i class="fa fa-star non-rate"></i>
                              <span class="review">( 06 Reviews )</span>
                           </div>-->
                           <div class="price"  style="margin-top:10px; font-size:16px">
                              <?php echo e(number_format($val->price, 2, ',', '.')); ?>₺
                              <?php if($val->old_price > 0): ?>
                              <span class="price-before-discount" style="text-decoration: line-through; color:#d3d3d3"><?php echo e(number_format($val->old_price, 2, ',', '.')); ?>₺</span>
                              <?php endif; ?>
                           </div>
                        </td>
                        <td class="col-md-2">
                           <a href="<?php echo e(url($val->slug)); ?>" class="btn-upper btn btn-primary"><i class="fa fa-search"></i> Ürünü İncele</a>
                        </td>
                        <td class="col-md-1 close-btn">
                           <a href="#" class="remove-wishlist-button" data-id="<?php echo e($val->wishId); ?>"><i class="fa fa-times"></i></a>
                        </td>
                     </tr>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                     <tr style="text-align:center">
                        <td colspan="4">Favori listeniz boş.</td>
                     </tr>
                     <?php endif; ?>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<script>
   $( ".remove-wishlist-button" ).click(function() {
     var id = $(this).data('id');
     $.ajax({
       url: 'favorilerimden-sil',
       method: 'POST',
       data:{
         id: id,
         _token: "<?php echo e(csrf_token()); ?>"
       },
       dataType: 'html',
       success: function(data) {
           var obj = jQuery.parseJSON( data);
           if(obj.status == "success") {
             $("#wish-"+id).remove();
           	swal({
              title: "Tebrikler!",
              text: obj.msg,
              timer: 1000,
              showConfirmButton: false,
              type: "success"
            });
           } else {
           	swal({
              title: "Bilgi!",
              text: obj.msg,
              timer: 1000,
              showConfirmButton: false,
              type: "info"
           	});
           }
           
       }
     });
   });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.theme.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>