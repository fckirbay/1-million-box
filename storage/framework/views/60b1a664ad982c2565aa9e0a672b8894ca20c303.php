<?php $__env->startSection('styles'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="page-content header-clear-small" style="padding-top: 0px;">
	<div class="single-slider owl-carousel owl-no-dots bottom-0">
		<div class="caption caption-margins round-medium shadow-large bottom-20" style="margin: 0 0 0; border-radius: 0px !important;">
			<div class="caption-bottom">
				<h1 class="color-white center-text bolder">Seçimini Yap</h1>
				<p class="under-heading color-white center-text bottom-20 opacity-80">Risk almak mı, azla yetinmek mi?</p>
			</div>
			<div class="caption-overlay bg-gradient"></div>
			<img class="caption-image owl-lazy" data-src="<?php echo e(asset('assets/cardgame/images/slider/doordie.png')); ?>">
		</div>
		<div class="caption caption-margins round-medium shadow-large bottom-20" style="margin: 0 0 0; border-radius: 0px !important;">
			<div class="caption-bottom">
				<h1 class="color-white center-text bolder">Sezgilerinize Güvenin</h1>
				<p class="under-heading color-white center-text bottom-20 opacity-80">Tek yapmanız gereken doğru kararı vermek.</p>
			</div>
			<div class="caption-overlay bg-gradient"></div>
			<img class="caption-image owl-lazy" data-src="<?php echo e(asset('assets/cardgame/images/slider/intuation.png')); ?>">
		</div>
		<div class="caption caption-margins round-medium shadow-large bottom-20" style="margin: 0 0 0; border-radius: 0px !important;">
			<div class="caption-bottom">
				<h1 class="color-white center-text bolder">Katlayarak Kazanın</h1>
				<p class="under-heading color-white center-text bottom-20 opacity-80">Risk alarak kazançlarınızı katlayın.</p>
			</div>
			<div class="caption-overlay bg-gradient"></div>
			<img class="caption-image owl-lazy" data-src="<?php echo e(asset('assets/cardgame/images/slider/winbyfold.jpg')); ?>">
		</div>
	</div>
	<div class="single-slider owl-carousel owl-no-dots bottom-10 top-0">
		<div class="content-boxed bottom-0" style="margin: 0 0 0; border-radius: 0px !important">
			<div class="content center-text">
				<h1 class="bolder"><?php echo app('translator')->getFromJson('general.sign_up_for_free'); ?></h1>
				<p class="under-heading font-12 color-highlight bottom-10"><?php echo app('translator')->getFromJson('general.play_completely_free'); ?></p>
				<p>
					<?php echo app('translator')->getFromJson('general.get_free_access_to_all_content'); ?>
				</p>
			</div>
		</div>
		<div class="content-boxed bottom-0" style="margin: 0 0 0; border-radius: 0px !important">
			<div class="content center-text">
				<h1 class="bolder bottom-0"><?php echo app('translator')->getFromJson('general.try_your_chance'); ?></h1>
				<p class="under-heading font-12 color-highlight bottom-10"><?php echo app('translator')->getFromJson('general.earn_extra_rights_by_performing_daily_tasks'); ?></p>
				<p>
					<?php echo app('translator')->getFromJson('general.you_can_win_up_to_5000_coins_in_a_single_game'); ?>
				</p>
			</div>
		</div>
		<div class="content-boxed bottom-0" style="margin: 0 0 0; border-radius: 0px !important">
			<div class="content center-text">
				<h1 class="bolder bottom-0"><?php echo app('translator')->getFromJson('general.higher_or_lower'); ?></h1>
				<p class="under-heading font-12 color-highlight bottom-10"><?php echo app('translator')->getFromJson('general.all_you_have_to_do_is_guess'); ?></p>
				<p>
					<?php echo app('translator')->getFromJson('general.collect_coins_and_play_your_coins_as_we_know_from_our_childhood'); ?>
				</p>
			</div>
		</div>
		<div class="content-boxed bottom-0" style="margin: 0 0 0; border-radius: 0px !important">
			<div class="content center-text">
				<h1 class="bolder bottom-0"><?php echo app('translator')->getFromJson('general.win_a_prize'); ?></h1>
				<p class="under-heading font-12 color-highlight bottom-10"><?php echo app('translator')->getFromJson('general.get_your_prize_instantly'); ?></p>
				<p>
					<?php echo app('translator')->getFromJson('general.with_the_coins_you_have_collected_in_the_game'); ?>
				</p>
			</div>
		</div>
	</div>
	<div class="content-boxed" style="margin: 0px 0px 0px; border-radius: 0px !important; padding-top: 0px">
		<div class="content">
			<div class="divider"></div>
			<div class="grid-columns">
				<div>
					<i class="fa fa-trophy color-yellow1-dark fa-3x"></i>
					<h3 class="bolder">Future Proof</h3>
					<p>
						Built to last, with the latest quality code.
					</p>
				</div>
				<div>
					<i class="fab fa-cloudscale color-red1-dark fa-3x"></i>
					<h3 class="bolder">Powerful</h3>
					<p>
						Speed, Features and Flexibility all in One!
					</p>
				</div>
				<div>
					<i class="fa fa-check color-green1-light fa-2x"></i>
					<h3 class="bolder">Easy to Use</h3>
					<p>
						Customers love our products for their ease.
					</p>
				</div>
				<div>
					<i class="far fa-life-ring color-blue2-dark fa-3x"></i>
					<h3 class="bolder">Customer Care</h3>
					<p>
						We treat others like we want to be treated.
					</p>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.cardgame.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>