<!-- ============================================== HEADER ============================================== -->
<header class="header-style-1">
   <!-- ============================================== TOP MENU ============================================== -->
   <div class="top-bar animate-dropdown">
      <div class="container">
         <div class="header-top-inner">
            <div class="cnt-account">
               <ul class="list-unstyled">
                  <?php if(Sentinel::check()): ?>
                  <li><a>Hoşgeldin <?php echo e(Sentinel::getUser()->first_name); ?></a></li>
                  <li><a href="<?php echo e(url('hesabim')); ?>"><i class="icon fa fa-user"></i>Hesabım</a></li>
                  <li><a href="<?php echo e(url('favorilerim')); ?>"><i class="icon fa fa-heart"></i>Favorilerim</a></li>
                  <li><a href="<?php echo e(url('siparislerim')); ?>"><i class="icon fa fa-tags"></i>Siparişlerim</a></li>
                  <li><a href="<?php echo e(url('sepetim')); ?>"><i class="icon fa fa-shopping-cart"></i>Sepetim</a></li>
                  <li><a href="<?php echo e(url('cikis-yap')); ?>"><i class="icon fa fa-sign-out"></i>Çıkış Yap</a></li>
                  <?php else: ?>
                  <li><a href="<?php echo e(url('sepetim')); ?>"><i class="icon fa fa-shopping-cart"></i>Sepetim</a></li>
                  <li><a href="<?php echo e(url('kayit-ol')); ?>"><i class="icon fa fa-sign-in"></i>Kayıt Ol</a></li>
                  <li><a href="<?php echo e(url('giris-yap')); ?>"><i class="icon fa fa-lock"></i>Giriş</a></li>
                  <?php endif; ?>
               </ul>
            </div>
            <!-- /.cnt-account -->
            <div class="clearfix"></div>
         </div>
         <!-- /.header-top-inner -->
      </div>
      <!-- /.container -->
   </div>
   <!-- /.header-top -->
   <!-- ============================================== TOP MENU : END ============================================== -->
   <div class="main-header">
      <div class="container">
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-3 logo-holder" style="margin-top:0px">
               <!-- ============================================================= LOGO ============================================================= -->
               <div class="logo">
                  <a href="<?php echo e(url('/')); ?>">
                  <img src="<?php echo e(asset('assets/theme/images/logoo.png')); ?>" alt="" style="width:150px">
                  </a>
               </div>
               <!-- /.logo -->
               <!-- ============================================================= LOGO : END ============================================================= -->				
            </div>
            <!-- /.logo-holder -->
            <div class="col-xs-12 col-sm-12 col-md-7 top-search-holder">
               <!-- /.contact-row -->
               <!-- ============================================================= SEARCH AREA ============================================================= -->
               <div class="search-area">
                  <?php echo Form::open(['url'=>'arama', 'method'=>'get']); ?>

                  <div class="control-group">
                     <ul class="categories-filter animate-dropdown">
                        <li class="dropdown">
                           <a class="dropdown-toggle"  data-toggle="dropdown" href="#">Kategoriler <b class="caret"></b></a>
                           <ul class="dropdown-menu" role="menu" >
                              <li class="menu-header">Computer</li>
                              <li role="presentation"><a role="menuitem" tabindex="-1" href="#">- byPeluş</a></li>
                              <li role="presentation"><a role="menuitem" tabindex="-1" href="#">- Hediyelik Eşya</a></li>
                              <li role="presentation"><a role="menuitem" tabindex="-1" href="#">- El Yapımı</a></li>
                              <li role="presentation"><a role="menuitem" tabindex="-1" href="#">- Saat</a></li>
                           </ul>
                        </li>
                     </ul>
                     <input class="search-field" placeholder="Arama yapın..." />
                     <button class="search-button" type="submit"></button>    
                  </div>
                  <?php echo Form::close(); ?>

               </div>
               <!-- /.search-area -->
               <!-- ============================================================= SEARCH AREA : END ============================================================= -->				
            </div>
            <!-- /.top-search-holder -->
            <div class="col-xs-12 col-sm-12 col-md-2 animate-dropdown top-cart-row">
               <!-- ============================================================= SHOPPING CART DROPDOWN ============================================================= -->
               <div class="dropdown dropdown-cart">
                  <a href="#" class="dropdown-toggle lnk-cart" data-toggle="dropdown">
                     <div class="items-cart-inner">
                        <div class="basket">
                           <i class="glyphicon glyphicon-shopping-cart"></i>
                        </div>
                        <div class="basket-item-count"><span class="count"><?php echo e(Cart::count()); ?></span></div>
                        <div class="total-price-basket">
                           <span class="lbl">Sepet -</span>
                           <span class="total-price">
                           <span class="value"><?php echo e(Cart::total(2, ',', '.')); ?></span><span class="sign">₺</span>
                           </span>
                        </div>
                     </div>
                  </a>
                  <ul class="dropdown-menu">
                     <li id="cart_content">
                        <?php $__empty_1 = true; $__currentLoopData = Cart::content(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                        <?php $photo = App\ProductPhotos::where('product_id', $row->id)->where('is_featured', 1)->first(); ?>
                        <div class="cart-item product-summary">
                           <div class="row">
                              <div class="col-xs-4">
                                 <div class="image">
                                    <a href="#"><img src="<?php echo e(asset($photo->photo)); ?>" alt="" style="width:auto; height:40px; display:block; margin:auto;"></a>
                                 </div>
                              </div>
                              <div class="col-xs-7">
                                 <h3 class="name"><a href="#"><?php echo e($row->name); ?> <?php if($row->qty > 1): ?> x <?php echo e($row->qty); ?><?php endif; ?></a></h3>
                                 <div class="price"><?php echo e(number_format($row->price * $row->qty, 2, ',', '.')); ?>₺</div>
                              </div>
                              <div class="col-xs-1 action">
                                 <a href="<?php echo e(url('sepetten-cikar', $row->rowId)); ?>"><i class="fa fa-trash"></i></a>
                              </div>
                           </div>
                        </div>
                        <!-- /.cart-item -->
                        <br />
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                        <div class="cart-item product-summary">
                           <div class="row" style="text-align:center">
                              Sepetiniz boş!
                           </div>
                        </div>
                        <?php endif; ?>
                        <div class="clearfix"></div>
                        <hr>
                        <div class="clearfix cart-total">
                           <div class="pull-right">
                              <span class="text">Toplam :</span><span class='price'><?php echo e(Cart::total(2, ',', '.')); ?>₺</span>
                           </div>
                           <div class="clearfix"></div>
                           <a href="<?php echo e(url('odeme')); ?>" class="btn btn-upper btn-primary btn-block m-t-20">Ödeme Yap</a>	
                        </div>
                        <!-- /.cart-total-->
                     </li>
                  </ul>
                  <!-- /.dropdown-menu-->
               </div>
               <!-- /.dropdown-cart -->
               <!-- ============================================================= SHOPPING CART DROPDOWN : END============================================================= -->				
            </div>
            <!-- /.top-cart-row -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container -->
   </div>
   <!-- /.main-header -->
   <!-- ============================================== NAVBAR ============================================== -->
   <div class="header-nav animate-dropdown">
      <div class="container">
         <div class="yamm navbar navbar-default" role="navigation">
            <div class="navbar-header">
               <button data-target="#mc-horizontal-menu-collapse" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
            </div>
            <div class="nav-bg-class">
               <div class="navbar-collapse collapse" id="mc-horizontal-menu-collapse">
                  <div class="nav-outer">
                     <ul class="nav navbar-nav">
                        <li class="active dropdown yamm-fw">
                           <a href="<?php echo e(url('/')); ?>" data-hover="dropdown" class="dropdown-toggle" data-toggle="dropdown">Anasayfa</a>
                        </li>
                        <?php $__empty_1 = true; $__currentLoopData = $menu[0]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                        <?php if($val->is_top == 1): ?>
                        <?php if(isset($menu[1][$key])): ?>
                        <li class="dropdown yamm mega-menu">
                           <a href="<?php echo e(url('kategori', $val->slug)); ?>" data-hover="dropdown" class="dropdown-toggle"><?php echo e($val->name); ?></a>
                           <ul class="dropdown-menu container">
                              <li>
                                 <div class="yamm-content ">
                                    <div class="row">
                                       <div class="col-xs-12 col-sm-6 col-md-8 col-menu">
                                          <h2 class="title">Alt Kategoriler</h2>
                                          <ul class="links">
                                             <?php $__empty_2 = true; $__currentLoopData = $menu[1][$key]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key2 => $val2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_2 = false; ?>
                                             <li><a href="<?php echo e(url('kategori', $val2['slug'])); ?>"><?php echo e($val2['name']); ?></a></li>
                                             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_2): ?>
                                             <?php endif; ?>
                                          </ul>
                                       </div>
                                       <!-- /.col -->
                                       <div class="col-xs-12 col-sm-6 col-md-4 col-menu banner-image">
                                          <img class="img-responsive" src="<?php echo e(asset('assets/theme/images/banners/top-menu-banner.jpg')); ?>" alt="">
                                       </div>
                                       <!-- /.yamm-content -->					
                                    </div>
                                 </div>
                              </li>
                           </ul>
                        </li>
                        <?php else: ?>
                        <li class="hidden-sm">
                           <a href="<?php echo e(url('kategori', $val->slug)); ?>"><?php echo e($val->name); ?>

                           <?php if($val->is_new == 1): ?><span class="menu-label new-menu hidden-xs">Yeni</span><?php endif; ?>
                           </a>
                        </li>
                        <?php endif; ?>
                        <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                        <?php endif; ?>
                        <li class="dropdown  navbar-right special-menu">
                           <a href="#">Günün Fırsatları</a>
                        </li>
                     </ul>
                     <!-- /.navbar-nav -->
                     <div class="clearfix"></div>
                  </div>
                  <!-- /.nav-outer -->
               </div>
               <!-- /.navbar-collapse -->
            </div>
            <!-- /.nav-bg-class -->
         </div>
         <!-- /.navbar-default -->
      </div>
      <!-- /.container-class -->
   </div>
   <!-- /.header-nav -->
   <!-- ============================================== NAVBAR : END ============================================== -->
</header>
<!-- ============================================== HEADER : END ============================================== -->