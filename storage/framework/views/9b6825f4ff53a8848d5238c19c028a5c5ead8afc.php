<!-- jQuery 2.2.3 -->
<script src="<?php echo e(asset('assets/admin/plugins/jquery/dist/jquery.min.js')); ?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo e(asset('assets/admin/plugins/bootstrap/dist/js/bootstrap.min.js')); ?>"></script>
<!-- FastClick -->
<script src="<?php echo e(asset('assets/admin/plugins/fastclick/lib/fastclick.js')); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo e(asset('assets/admin/js/adminlte.min.js')); ?>"></script>
<!-- Sweetalert -->
<script src="<?php echo e(asset('assets/admin/plugins/sweetalert/dist/sweetalert.min.js')); ?>"></script>
<?php echo $__env->make('sweet::alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>