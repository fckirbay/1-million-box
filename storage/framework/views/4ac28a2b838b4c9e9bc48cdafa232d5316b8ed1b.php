<!-- ============================================== HOT DEALS ============================================== -->
<div class="sidebar-widget hot-deals wow fadeInUp outer-bottom-xs">
	<h3 class="section-title">Ayın Fırsatları</h3>
	<div class="owl-carousel sidebar-carousel custom-carousel owl-theme outer-top-ss">
		
		<?php
			$remaining_time = Carbon\Carbon::now()->endOfMonth()->diffInMinutes(Carbon\Carbon::now()); 
			$days = $remaining_time / 1440; 
			$hours = $remaining_time % 1440 / 60;
			$minutes = $remaining_time % 1440 % 60; 
		?>
		
		<?php $__empty_1 = true; $__currentLoopData = $month_deals; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
														<div class="item">
					<div class="products">
						<div class="hot-deal-wrapper">
							<div class="image">
								<img src="<?php echo e(asset($val->photo)); ?>" style="width:auto; height:200px; display:block; margin:auto;" alt="">
							</div>
							<?php if($val->old_price != null): ?>
								<div class="sale-offer-tag"><span style="font-size:12px">%<?php echo e(intval(($val->old_price - $val->price) / $val->old_price * 100)); ?> <br /> İnd.</span></div>
							<?php endif; ?>
							<div class="timing-wrapper">
								<div class="box-wrapper">
									<div class="date box">
										<span class="key"><?php echo e(intval($days)); ?></span>
										<span class="value">GÜN</span>
									</div>
								</div>
				                
				                <div class="box-wrapper">
									<div class="hour box">
										<span class="key"><?php echo e(intval($hours)); ?></span>
										<span class="value">SAAT</span>
									</div>
								</div>

				                <div class="box-wrapper">
									<div class="minutes box">
										<span class="key"><?php echo e(intval($minutes)); ?></span>
										<span class="value">DAKİKA</span>
									</div>
								</div>
							</div>
						</div><!-- /.hot-deal-wrapper -->

						<div class="product-info text-left m-t-20">
							<h3 class="name"><a href="<?php echo e(url($val->slug)); ?>"><?php echo e($val->title); ?></a></h3>
							<div class="rating rateit-small"></div>

							<div class="product-price">	
								<span class="price">
									<?php echo e(number_format($val->price, 2, ',', '.')); ?>₺
								</span>
									<?php if($val->old_price != null): ?>
							    <span class="price-before-discount"><?php echo e(number_format($val->old_price, 2, ',', '.')); ?>₺</span>
							    <?php endif; ?>
							
							</div><!-- /.product-price -->
							
						</div><!-- /.product-info -->

						<div class="cart clearfix animate-effect">
							<div class="action">
								
								<div class="btn-group">
									<a href="<?php echo e(url($val->slug)); ?>" class="btn btn-primary cart-btn" type="button"> <i class="fa fa-shopping-cart"></i>	Ürünü İncele</a>
								</div>
								
							</div><!-- /.action -->
						</div><!-- /.cart -->
					</div>	
					</div>
					
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
					<?php endif; ?>
						
	    
    </div><!-- /.sidebar-widget -->
</div>
<!-- ============================================== HOT DEALS: END ============================================== -->