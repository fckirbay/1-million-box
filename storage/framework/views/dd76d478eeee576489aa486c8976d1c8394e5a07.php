<?php $__env->startSection('styles'); ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
            <div class="content" style="margin-top:20px">
               <h4 class="uppercase bolder" style="text-align:center"><?php echo app('translator')->getFromJson('general.safe'); ?></h4>
               <p style="text-align:center">
                  <?php echo app('translator')->getFromJson('general.you_can_only_withdraw_50'); ?>
               </p>
               <div class="checkout-total">
                  <strong class="font-14 regularbold"><?php echo app('translator')->getFromJson('general.remaining'); ?></strong>
                  <span class="font-14"><?php echo e(Sentinel::getUser()->ticket); ?></span>
                  <div class="clear"></div>
                  <strong class="font-14 regularbold"><?php echo app('translator')->getFromJson('general.spent'); ?></strong>
                  <span class="font-14"><?php echo e(Sentinel::getUser()->won + Sentinel::getUser()->lost); ?></span>
                  <div class="clear"></div>
                  <strong class="font-14 regularbold"><?php echo app('translator')->getFromJson('general.won'); ?></strong>
                  <span class="font-14"><?php echo e(Sentinel::getUser()->won); ?></span>
                  <div class="clear"></div>
                  <strong class="font-14 regularbold color-highlight"><?php echo app('translator')->getFromJson('general.lost'); ?></strong>
                  <span class="font-14 color-highlight"><?php echo e(Sentinel::getUser()->lost); ?></span>
                  <div class="clear"></div>
                  <strong class="font-14 regularbold"><?php echo app('translator')->getFromJson('general.total_earnings'); ?></strong>
                  <span class="font-14"><?php if(Sentinel::getUser()->currency == "eur"): ?>€<?php endif; ?><?php echo e(number_format(Sentinel::getUser()->earnings, 2, ',', '.')); ?> <?php if(Sentinel::getUser()->currency == "try"): ?>₺<?php endif; ?></span>
                  <div class="clear"></div>
                  <strong class="font-16 half-top"><?php echo app('translator')->getFromJson('general.balance'); ?> </strong>
                  <span class="font-16 color-highlight ultrabold half-top"><?php if(Sentinel::getUser()->currency == "eur"): ?>€<?php endif; ?><?php echo e(number_format(Sentinel::getUser()->balance, 2, ',', '.')); ?> <?php if(Sentinel::getUser()->currency == "try"): ?>₺<?php endif; ?></span>
                  <div class="clear"></div>
               </div>
               
               <a href="#" class="button button-green button-full button-rounded button-sm uppercase ultrabold" id="withdraw-money"><?php echo app('translator')->getFromJson('general.withdraw_money'); ?></a>
               
               <p style="text-align:center">
                  <?php echo app('translator')->getFromJson('general.your_withdrawal_requests_will_be_processed'); ?>
               </p>
            </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
   <script>
       $( "#withdraw-money" ).click(function() {
            swal("<?php echo app('translator')->getFromJson('general.sorry'); ?>! :(", "<?php echo app('translator')->getFromJson('general.your_balance_are_inadequate'); ?>", "error");
        });
   </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.mobile.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>