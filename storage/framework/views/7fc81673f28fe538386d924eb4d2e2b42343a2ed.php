<!DOCTYPE HTML>
<html lang="en">
	<head>
    <?php echo $__env->make('partials.frontend.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->yieldContent('styles'); ?>
    </head>
    <body oncontextmenu ="return false;">
        <!-- back to top start -->
        <div id="back-top-btn">
            <i class="fa fa-chevron-up"></i>
        </div>
        <!-- back to top end -->
        <!-- Preloader Start -->
        <div class="preloader">
            <div class="loader">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <?php echo $__env->make('partials.frontend.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- Preloader end -->
        <?php echo $__env->yieldContent('content'); ?>
        <?php echo $__env->make('partials.frontend.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->make('partials.frontend.scripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->yieldContent('scripts'); ?>
    </body>
</html>