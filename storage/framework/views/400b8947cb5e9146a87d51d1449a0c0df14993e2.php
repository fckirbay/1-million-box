<!-- ============================================== SPECIAL OFFER ============================================== -->
<div class="sidebar-widget outer-bottom-small wow fadeInUp">
   <h3 class="section-title">Özel Fırsatlar</h3>
   <div class="sidebar-widget-body outer-top-xs">
      <div class="owl-carousel sidebar-carousel special-offer custom-carousel owl-theme outer-top-xs">
         <div class="item">
            <div class="products special-product">
               <div class="product">
                  <div class="product-micro">
                     <div class="row product-micro-row">
                        <div class="col col-xs-5">
                           <div class="product-image">
                              <div class="image">
                                 <a href="<?php echo e(url($specialDeals[0]->slug)); ?>">
                                 <img src="<?php echo e(asset($specialDeals[0]->photo)); ?>" alt="">
                                 </a>					
                              </div>
                              <!-- /.image -->
                           </div>
                           <!-- /.product-image -->
                        </div>
                        <!-- /.col -->
                        <div class="col col-xs-7">
                           <div class="product-info">
                              <h3 class="name"><a href="<?php echo e(url($specialDeals[0]->slug)); ?>"><?php echo e($specialDeals[0]->title); ?></a></h3>
                              <div class="rating rateit-small"></div>
                              <div class="product-price">	
                                 <span class="price">
                                 <?php echo e(number_format($specialDeals[0]->price, 2, ',', '.')); ?>				</span>
                                 <?php if($specialDeals[0]->old_price != null): ?>
        										     <span class="price-before-discount"><?php echo e(number_format($specialDeals[0]->old_price, 2, ',', '.')); ?>₺</span>
        										     <?php endif; ?>
                              </div>
                              <!-- /.product-price -->
                           </div>
                        </div>
                        <!-- /.col -->
                     </div>
                     <!-- /.product-micro-row -->
                  </div>
                  <!-- /.product-micro -->
               </div>
               <div class="product">
                  <div class="product-micro">
                     <div class="row product-micro-row">
                        <div class="col col-xs-5">
                           <div class="product-image">
                              <div class="image">
                                 <a href="<?php echo e(url($specialDeals[1]->slug)); ?>">
                                 <img src="<?php echo e(asset($specialDeals[1]->photo)); ?>" alt="">
                                 </a>					
                              </div>
                              <!-- /.image -->
                           </div>
                           <!-- /.product-image -->
                        </div>
                        <!-- /.col -->
                        <div class="col col-xs-7">
                           <div class="product-info">
                              <h3 class="name"><a href="<?php echo e(url($specialDeals[1]->slug)); ?>"><?php echo e($specialDeals[1]->title); ?></a></h3>
                              <div class="rating rateit-small"></div>
                              <div class="product-price">	
                                 <span class="price">
                                 <?php echo e(number_format($specialDeals[1]->price, 2, ',', '.')); ?>				</span>
                                 <?php if($specialDeals[1]->old_price != null): ?>
        										     <span class="price-before-discount"><?php echo e(number_format($specialDeals[1]->old_price, 2, ',', '.')); ?>₺</span>
        										     <?php endif; ?>
                              </div>
                              <!-- /.product-price -->
                           </div>
                        </div>
                        <!-- /.col -->
                     </div>
                     <!-- /.product-micro-row -->
                  </div>
                  <!-- /.product-micro -->
               </div>
               <div class="product">
                  <div class="product-micro">
                     <div class="row product-micro-row">
                        <div class="col col-xs-5">
                           <div class="product-image">
                              <div class="image">
                                 <a href="<?php echo e(url($specialDeals[2]->slug)); ?>">
                                 <img src="<?php echo e(asset($specialDeals[2]->photo)); ?>" alt="">
                                 </a>					
                              </div>
                              <!-- /.image -->
                           </div>
                           <!-- /.product-image -->
                        </div>
                        <!-- /.col -->
                        <div class="col col-xs-7">
                           <div class="product-info">
                              <h3 class="name"><a href="<?php echo e(url($specialDeals[2]->slug)); ?>"><?php echo e($specialDeals[2]->title); ?></a></h3>
                              <div class="rating rateit-small"></div>
                              <div class="product-price">	
                                 <span class="price">
                                 <?php echo e(number_format($specialDeals[2]->price, 2, ',', '.')); ?>				</span>
                                 <?php if($specialDeals[2]->old_price != null): ?>
        										     <span class="price-before-discount"><?php echo e(number_format($specialDeals[2]->old_price, 2, ',', '.')); ?>₺</span>
        										     <?php endif; ?>
                              </div>
                              <!-- /.product-price -->
                           </div>
                        </div>
                        <!-- /.col -->
                     </div>
                     <!-- /.product-micro-row -->
                  </div>
                  <!-- /.product-micro -->
               </div>
            </div>
         </div>
         <div class="item">
            <div class="products special-product">
               <div class="product">
                  <div class="product-micro">
                     <div class="row product-micro-row">
                        <div class="col col-xs-5">
                           <div class="product-image">
                              <div class="image">
                                 <a href="<?php echo e(url($specialDeals[3]->slug)); ?>">
                                 <img src="<?php echo e(asset($specialDeals[3]->photo)); ?>" alt="">
                                 </a>					
                              </div>
                              <!-- /.image -->
                           </div>
                           <!-- /.product-image -->
                        </div>
                        <!-- /.col -->
                        <div class="col col-xs-7">
                           <div class="product-info">
                              <h3 class="name"><a href="<?php echo e(url($specialDeals[3]->slug)); ?>"><?php echo e($specialDeals[3]->title); ?></a></h3>
                              <div class="rating rateit-small"></div>
                              <div class="product-price">	
                                 <span class="price">
                                 <?php echo e(number_format($specialDeals[3]->price, 2, ',', '.')); ?>				</span>
                                 <?php if($specialDeals[3]->old_price != null): ?>
        										     <span class="price-before-discount"><?php echo e(number_format($specialDeals[3]->old_price, 2, ',', '.')); ?>₺</span>
        										     <?php endif; ?>
                              </div>
                              <!-- /.product-price -->
                           </div>
                        </div>
                        <!-- /.col -->
                     </div>
                     <!-- /.product-micro-row -->
                  </div>
                  <!-- /.product-micro -->
               </div>
               <div class="product">
                  <div class="product-micro">
                     <div class="row product-micro-row">
                        <div class="col col-xs-5">
                           <div class="product-image">
                              <div class="image">
                                 <a href="<?php echo e(url($specialDeals[4]->slug)); ?>">
                                 <img src="<?php echo e(asset($specialDeals[4]->photo)); ?>" alt="">
                                 </a>					
                              </div>
                              <!-- /.image -->
                           </div>
                           <!-- /.product-image -->
                        </div>
                        <!-- /.col -->
                        <div class="col col-xs-7">
                           <div class="product-info">
                              <h3 class="name"><a href="<?php echo e(url($specialDeals[4]->slug)); ?>"><?php echo e($specialDeals[4]->title); ?></a></h3>
                              <div class="rating rateit-small"></div>
                              <div class="product-price">	
                                 <span class="price">
                                 <?php echo e(number_format($specialDeals[4]->price, 2, ',', '.')); ?>				</span>
                                 <?php if($specialDeals[4]->old_price != null): ?>
        										     <span class="price-before-discount"><?php echo e(number_format($specialDeals[4]->old_price, 2, ',', '.')); ?>₺</span>
        										     <?php endif; ?>
                              </div>
                              <!-- /.product-price -->
                           </div>
                        </div>
                        <!-- /.col -->
                     </div>
                     <!-- /.product-micro-row -->
                  </div>
                  <!-- /.product-micro -->
               </div>
               <div class="product">
                  <div class="product-micro">
                     <div class="row product-micro-row">
                        <div class="col col-xs-5">
                           <div class="product-image">
                              <div class="image">
                                 <a href="<?php echo e(url($specialDeals[5]->slug)); ?>">
                                 <img src="<?php echo e(asset($specialDeals[5]->photo)); ?>" alt="">
                                 </a>					
                              </div>
                              <!-- /.image -->
                           </div>
                           <!-- /.product-image -->
                        </div>
                        <!-- /.col -->
                        <div class="col col-xs-7">
                           <div class="product-info">
                              <h3 class="name"><a href="<?php echo e(url($specialDeals[5]->slug)); ?>"><?php echo e($specialDeals[5]->title); ?></a></h3>
                              <div class="rating rateit-small"></div>
                              <div class="product-price">	
                                 <span class="price">
                                 <?php echo e(number_format($specialDeals[5]->price, 2, ',', '.')); ?>				</span>
                                 <?php if($specialDeals[5]->old_price != null): ?>
        										     <span class="price-before-discount"><?php echo e(number_format($specialDeals[5]->old_price, 2, ',', '.')); ?>₺</span>
        										     <?php endif; ?>
                              </div>
                              <!-- /.product-price -->
                           </div>
                        </div>
                        <!-- /.col -->
                     </div>
                     <!-- /.product-micro-row -->
                  </div>
                  <!-- /.product-micro -->
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- /.sidebar-widget-body -->
</div>
<!-- /.sidebar-widget -->
<!-- ============================================== SPECIAL OFFER : END ============================================== -->