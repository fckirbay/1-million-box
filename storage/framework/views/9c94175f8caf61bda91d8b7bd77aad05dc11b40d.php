<?php $__env->startSection('styles'); ?>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
            <a href="#" class="cover-back-button back-button"><i class="fa fa-chevron-left font-12 color-white"></i></a>
            <a href="index.html" class="cover-home-button"><i class="fa fa-home font-14 color-white"></i></a>
            <div class="cover-item cover-item-full">
               <div class="cover-content cover-content-center">
                  <div class="page-login content-boxed content-boxed-padding top-0 bottom-0">
                     <h3 class="uppercase ultrabold top-10 bottom-0">Parolanızı Sıfırlayın</h3>
                     <?php echo Form::open(['url'=>'sifremi-unuttum', 'method'=>'post', 'class'=>'register-form outer-top-xs']); ?>

                     <br />
                     <div class="page-login-field bottom-15">
                        <i class="fa fa-at"></i>
                        <input type="text" name="email" placeholder="TC No veya E-Posta Adresi" required>
                        <em>(zorunlu)</em>
                     </div>
                     <br />
                     <button type="submit" class="button button-red button-full button-rounded button-s uppercase ultrabold bottom-10">PAROLAMI SIFIRLA</button>
                     <?php echo Form::close(); ?>

                  </div>
               </div>
               <div class="cover-item cover-item-full" style="background-image:url(images/pictures_vertical/bg1.jpg);"></div>
               <div class="cover-overlay overlay bg-black opacity-80"></div>
            </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
   
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.mobile.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>