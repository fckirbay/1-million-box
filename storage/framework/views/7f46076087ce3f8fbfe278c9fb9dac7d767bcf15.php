<?php $__env->startSection('styles'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
	<!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Markalar <a href="#addnew" data-toggle="modal" class="btn btn-primary btn-xs">Yeni Ekle</a></h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Ara">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th style="width:40%">Marka</th>
                  <th style="width:20%">Durum</th>
                  <th style="width:20%">İşlem</th>
                </tr>
                <?php $__empty_1 = true; $__currentLoopData = $brands; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                <tr>
                  <td><?php echo e($val->brand); ?></td>
                  <td><?php if($val->is_active == 0): ?> Pasif <?php else: ?> Aktif <?php endif; ?></td>
                  <td>
                      <?php echo Form::open(['url'=>'admin/product-management/delete-product-brand', 'method'=>'post', 'class'=>'form-horizontal register-form outer-top-xs']); ?>

                          <input type="hidden" name="brand_id" value="<?php echo e($val->id); ?>"/>
                          <button type="submit" class="btn btn-danger btn-xs">Sil</button>
                      <?php echo Form::close(); ?>

                  </td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <tr>
                    <td colspan="5" style="text-align:center">Hiç kayıt bulunamadı.</td>
                </tr>
                <?php endif; ?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
    
    <div class="modal fade" id="addnew" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Marka Ekle</h4>
          </div>
          <?php echo Form::open(['url'=>'admin/product-management/new-product-brand', 'method'=>'post', 'class'=>'form-horizontal register-form outer-top-xs']); ?>

          <div class="modal-body">
            <div class="form-group">
                <label class="control-label col-sm-3" for="email">Marka</label>
                <div class="col-sm-9">
                  <input type="text" name="brand" class="form-control" id="email" placeholder="Marka" required>
                </div>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
            <button type="submit" class="btn btn-primary">Kaydet</button>
          </div>
          <?php echo Form::close(); ?>

        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
  <?php echo $__env->make('sweet::alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>