<?php $__env->startSection('styles'); ?>
    <style>
       @-webkit-keyframes ticker {
  0% {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
    visibility: visible;
  }
  100% {
    -webkit-transform: translate3d(-100%, 0, 0);
    transform: translate3d(-100%, 0, 0);
  }
}
@keyframes  ticker {
  0% {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
    visibility: visible;
  }
  100% {
    -webkit-transform: translate3d(-100%, 0, 0);
    transform: translate3d(-100%, 0, 0);
  }
}
.ticker-wrap {
  width: 100%;
  overflow: scroll;
  height: 30px;
  background-color: rgba(0, 0, 0, 0.9);
  padding-left: 100%;
}
.ticker-wrap .ticker {
  display: inline-block;
  height: 30px;
  line-height: 30px;
  white-space: nowrap;
  padding-right: 100%;
  box-sizing: content-box;
  -webkit-animation-iteration-count: infinite;
  animation-iteration-count: infinite;
  -webkit-animation-timing-function: linear;
  animation-timing-function: linear;
  -webkit-animation-name: ticker;
  animation-name: ticker;
  -webkit-animation-duration: 30s;
  animation-duration: 150s;
}
.ticker-wrap .ticker__item {
  display: inline-block;
  padding: 0 2rem;
  font-size: 16px;
  color: white;
}
    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
            <!--<a data-menu="menu-3" href="#" class="demo-settings demo-settings-fixed bg-highlight">Settings</a>-->
            <div class="single-slider owl-carousel owl-no-dots bottom-30" style="margin-bottom:0px !important">
               <?php if(session('lang') == "gb"): ?>
               <div class="store-slide-1">
                  <a href="#" class="store-slide-image" style="margin: 0px auto 30px !important; max-width:100% !important"><img src="<?php echo e(asset('assets/mobile/images/slider/31.jpg')); ?>" alt="" style="width:100%; height:150px"></a>
               </div>
               <div class="store-slide-1">
                  <a href="#" class="store-slide-image" style="margin: 0px auto 30px !important; max-width:100% !important"><img src="<?php echo e(asset('assets/mobile/images/slider/41.png')); ?>" alt="" style="width:100%; height:150px"></a>
               </div>
               <div class="store-slide-1">
                  <a href="#" class="store-slide-image" style="margin: 0px auto 30px !important; max-width:100% !important"><img src="<?php echo e(asset('assets/mobile/images/slider/21.jpg')); ?>" alt="" style="width:100%; height:150px"></a>
               </div>
               <div class="store-slide-1">
                  <a href="#" class="store-slide-image" style="margin: 0px auto 30px !important; max-width:100% !important"><img src="<?php echo e(asset('assets/mobile/images/slider/11.jpg')); ?>" alt="" style="width:100%; height:150px"></a>
               </div>
               <?php else: ?>
               <div class="store-slide-1">
                  <a href="#" class="store-slide-image" style="margin: 0px auto 30px !important; max-width:100% !important"><img src="<?php echo e(asset('assets/mobile/images/slider/32.jpg')); ?>" alt="" style="width:100%; height:150px"></a>
               </div>
               <div class="store-slide-1">
                  <a href="#" class="store-slide-image" style="margin: 0px auto 30px !important; max-width:100% !important"><img src="<?php echo e(asset('assets/mobile/images/slider/42.png')); ?>" alt="" style="width:100%; height:150px"></a>
               </div>
               <div class="store-slide-1">
                  <a href="#" class="store-slide-image" style="margin: 0px auto 30px !important; max-width:100% !important"><img src="<?php echo e(asset('assets/mobile/images/slider/22.jpg')); ?>" alt="" style="width:100%; height:150px"></a>
               </div>
               <div class="store-slide-1">
                  <a href="#" class="store-slide-image" style="margin: 0px auto 30px !important; max-width:100% !important"><img src="<?php echo e(asset('assets/mobile/images/slider/12.jpg')); ?>" alt="" style="width:100%; height:150px"></a>
               </div>
               <?php endif; ?>
            </div>
            <div class="content-full">
               <!--<p class="center-text boxed-text-large bottom-30">
                  Plenty of categories to choose from. You can also use icon lists like the main menu for this section.
               </p>-->
               <?php if(Sentinel::check() && (Sentinel::getUser()->id == 1 || Sentinel::getUser()->id == 8 || Sentinel::getUser()->id == 18888)): ?>
               <!--
               <button class="uppercase ultrabold button-xs button button-blue" id="rewarded" onClick="showOfferWall();" style="text-align:center"><i class="fa fa-shopping-cart color-white"></i> OfferWall</button>
                <button class="uppercase ultrabold button-xxs button button-bl覺e" onClick="showInterstitial();" style="float:right"><i class="fa fa-shopping-cart color-white"></i> Show Interstitial</button>
                <button class="uppercase ultrabold button-xxs button button-bl覺e" onClick="showRewarded();" style="float:right"><i class="fa fa-shopping-cart color-white"></i> Show Rewarded</button>
              -->
               <?php endif; ?>
               
               <div class="ticker-wrap" style="margin-top:-10px">
                  <div class="ticker">
                    <!--
                     <?php $__currentLoopData = $prizes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                     <?php 
                        if($val->currency == "try") {
                            $curr = "LP";
                        } else {
                            $curr = "LP";
                        }
                     ?>
                        <div class="ticker__item"><?php echo app('translator')->getFromJson('general.win_x_prize', [ 'user' => $val->first_name, 'prize' => number_format($val->prize, 2, ',', '.') . ' ' . $curr ]); ?></div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  -->
                  <?php $__currentLoopData = $completedTasks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <div class="ticker__item"><?php echo app('translator')->getFromJson('general.user_completed_task', [ 'user' => $val->first_name, 'prize' => $val->point_value ]); ?></div>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </div>
               </div>
               

               
               



<?php if(Sentinel::check() && Carbon\Carbon::now() < '2018-10-31 21:00:00'): ?>

<div class="content">
                  <h3 class="uppercase bolder center-text"><span class="color-highlight">Bonus</span></h3>
               </div>

<div class="content" style="margin-top:10px">
<?php echo Form::open(['url'=>'free-bonus', 'method'=>'post', 'class'=>'register-form outer-top-xs']); ?>

                  <div class="input-simple-1 has-icon input-green bottom-30" style="text-align:center"><em><?php echo app('translator')->getFromJson('general.enter_code_to_win_ticket'); ?></em><i class="fa fa-user"></i><input type="text" name="bonus_code" placeholder="<?php echo app('translator')->getFromJson('general.bonus_code'); ?>" value="LUCKY"></div>

                   <button type="submit" class="button button-green" style="width:100%"><?php echo app('translator')->getFromJson('general.get_bonus'); ?></button>
               <?php echo Form::close(); ?>

             </div>

<div class="decoration decoration-margins"></div>

<?php else: ?>
<!--
<div class="content">
                  <h3 class="uppercase bolder center-text"><span class="color-highlight">Bonus</span></h3>
               </div>
<div class="content" style="text-align:center">
<?php echo app('translator')->getFromJson('general.no_active_bonus'); ?>
             </div>

<div class="decoration decoration-margins"></div>
-->
<?php endif; ?>

<?php if(Sentinel::check() && Sentinel::getUser()->verification == 1): ?>
<div class="card card-red" style="padding: 8px 0px; margin: 20px 0px; height: 80px">
<?php if(Sentinel::check() && Sentinel::getUser()->currency == "try"): ?>
<h1 class="color-white center-text uppercase bolder top-10" style="font-size:40px"><?php echo e(number_format($earnings, 2, ',', '.')); ?> LP</h1>
<?php else: ?>
<h1 class="color-white center-text uppercase bolder top-10" style="font-size:40px">LP<?php echo e(number_format($earnings / 7, 2, '.', ',')); ?></h1>


<?php endif; ?>
<p class="small-text center-text color-white bottom-15 opacity-50" style="margin-top:5px; font-size:16px !important"><?php echo app('translator')->getFromJson('general.total_earnings_homepage'); ?></p>
</div>
<div class="decoration decoration-margins" style="margin-bottom:10px"></div>
<?php endif; ?>
               
                <p class="center-text boxed-text-large">
                    <?php echo app('translator')->getFromJson('general.you_can_earn_5_extra_tickets'); ?>
                    <?php if(!Sentinel::check()): ?>
                    <br /><br />
                    <a href="<?php echo e(url('signup')); ?>" class="button button-rounded button-dark" style="font-weight:bold; font-size:18px"><?php echo app('translator')->getFromJson('general.signup'); ?></a>
                    <?php endif; ?>
                </p>
                
                <div class="decoration decoration-margins" style="margin-top:10px"></div>


                
               <div class="content">
                  <h3 class="uppercase bolder center-text"><?php echo app('translator')->getFromJson('general.active'); ?> <span class="color-highlight"><?php echo app('translator')->getFromJson('general.rooms'); ?></span></h3>
               </div>
               
               <div class="blog-categories blog-categories-3 bottom-20">
                  <?php $__currentLoopData = $rooms; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                     <a href="<?php echo e(url('room', $val->slug)); ?>?show=1"><strong></strong><em><?php if(Lang::locale() == "tr"): ?><?php echo e($val->name); ?><?php else: ?><?php echo e($val->name_en); ?><?php endif; ?></em><span class="bg-red-dark opacity-50"></span><img src="<?php echo e(asset('assets/mobile/images/empty.png')); ?>" data-src="<?php echo e(asset($val->photo)); ?>" class="preload-image responsive-image" alt="img"></a>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  <div class="clear"></div>
               </div>
               
            </div>
            <div class="decoration decoration-margins"></div>
<!--
            <div class="double-slider owl-carousel owl-has-dots bottom-15">
                  <div class="store-featured-1">
                     <a href="#" style="width:100%; max-width:500px"><img src="<?php echo e(asset('assets/mobile/images/payment_methods/ecopayz.png')); ?>" alt="img" style="width:100%; max-width:500px"></a>
                  </div>
                  <div class="store-featured-1">
                     <a href="#" style="width:100%; max-width:500px"><img src="<?php echo e(asset('assets/mobile/images/payment_methods/skrill.png')); ?>" alt="img" style="width:100%; max-width:500px"></a>
                  </div>
                  <div class="store-featured-1">
                     <a href="#" style="width:100%; max-width:500px"><img src="<?php echo e(asset('assets/mobile/images/payment_methods/neteller.png')); ?>" alt="img" style="width:100%; max-width:500px"></a>
                  </div>
                  <div class="store-featured-1">
                     <a href="#" style="width:100%; max-width:500px"><img src="<?php echo e(asset('assets/mobile/images/payment_methods/jeton.png')); ?>" alt="img" style="width:100%; max-width:500px"></a>
                  </div>
                  <div class="store-featured-1">
                     <a href="#" style="width:100%; max-width:500px"><img src="<?php echo e(asset('assets/mobile/images/payment_methods/payoneer.png')); ?>" alt="img" style="width:100%; max-width:500px"></a>
                  </div>
               </div>
-->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
  
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.mobile.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>