        <div id="menu-cart" class="menu-flyin">
            <div class="menu-flyin-content top-25">
               <?php $__empty_1 = true; $__currentLoopData = Cart::content(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
               <?php $product = App\Products::where('id', $row->id)->select('slug')->first();
                     $photo = App\ProductPhotos::where('product_id', $row->id)->where('is_featured', 1)->select('photo')->first(); ?>
               <div class="menu-cart-item">
                  <img class="preload-image" src="<?php echo e(asset('assets/mobile/images/empty.png')); ?>" data-src="<?php echo e(asset('assets/mobile/images/placeholders/1.jpg')); ?>" alt="img">
                  <strong><?php echo e($product->title); ?></strong>
                  <span><?php echo e($product->subtitle); ?></span>
                  <em><?php echo e(number_format($product->price, 2, ',', '.')); ?>₺ <?php if($product->old_price > 0): ?><del><?php echo e(number_format($product->old_price, 2, ',', '.')); ?>₺</del><?php endif; ?></em>
                  <a href="#" class="color-red-dark"><i class="fa fa-times"></i> Sepetten Çıkar</a>
               </div>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
               <div style="text-align:center; font-weight:bold">Sepetiniz boş.</div>
               <?php endif; ?>
               <?php if(Cart::count() > 0): ?>
               <div class="decoration bottom-10"></div>
               <div class="menu-cart-total">
                  <h5 class="float-left">Toplam</h5>
                  <h5 class="uppercase float-right"><?php echo e(number_format(Cart::total(), 2, ',', '.')); ?>₺</h5>
                  <div class="clear"></div>
                  <a href="#" class="button button-full button-center button-s top-20 button-rounded button-green uppercase ultrabold">Checkout</a>
               </div>
               <?php endif; ?>
            </div>
         </div>