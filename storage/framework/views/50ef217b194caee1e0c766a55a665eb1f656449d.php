<?php $__env->startSection('styles'); ?>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
            <div class="content-full">
               <div class="decoration decoration-margins"></div>
               <div class="content">
                  <?php if($rooms[0]->location == "tr"): ?>
                     <h3 class="uppercase bolder center-text"><?php echo app('translator')->getFromJson('general.turkey'); ?> <span class="color-highlight"><?php echo app('translator')->getFromJson('general.rooms'); ?></span></h3>
                  <?php elseif($rooms[0]->location == "eu"): ?>
                     <h3 class="uppercase bolder center-text"><?php echo app('translator')->getFromJson('general.europe'); ?> <span class="color-highlight"><?php echo app('translator')->getFromJson('general.rooms'); ?></span></h3>
                  <?php else: ?>
                     <h3 class="uppercase bolder center-text"><?php echo app('translator')->getFromJson('general.world'); ?> <span class="color-highlight"><?php echo app('translator')->getFromJson('general.rooms'); ?></span></h3>
                  <?php endif; ?>
               </div>
               
               <div class="blog-categories blog-categories-3 bottom-20">
                  <?php $__currentLoopData = $rooms; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                     <a href="<?php echo e(url('room', $val->slug)); ?>"><strong></strong><em><?php if($lang == "tr"): ?><?php echo e($val->name); ?><?php else: ?><?php echo e($val->name_en); ?><?php endif; ?></em><span class="bg-red-dark opacity-50"></span><img src="<?php echo e(asset('assets/mobile/images/empty.png')); ?>" data-src="<?php echo e(asset($val->photo)); ?>" class="preload-image responsive-image" alt="img"></a>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  <div class="clear"></div>
               </div>
               
            </div>
            <div class="decoration decoration-margins"></div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.mobile.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>