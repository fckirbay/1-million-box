    <script type="text/javascript" src="<?php echo e(asset('assets/mobile/scripts/jquery.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/mobile/scripts/plugins.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/mobile/scripts/custom.js')); ?>"></script>
    <!-- Sweetalert -->
  <script src="<?php echo e(asset('assets/theme/plugins/sweetalert/dist/sweetalert.min.js')); ?>"></script>
        <!-- || Request::segment(1) === 'premium-membership' || Request::segment(1) === 'premium-membership-advantages' -->
        <?php if(Sentinel::check() && Sentinel::getUser()->verification == 1 && Sentinel::getUser()->support == 0): ?>
          <?php if(session('livechat') == "yes"): ?>
            <script type="text/javascript">
                var Tawk_API=Tawk_API||{};
                Tawk_API.visitor = {
                name : '<?php echo e(Sentinel::getUser()->first_name); ?>' + ' (' + <?php echo e(Sentinel::getUser()->id); ?> + ')',
                email : 'no@email.com'
                };

                var Tawk_LoadStart=new Date();
                (function(){
                var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
                s1.async=true;
                s1.src='https://embed.tawk.to/5b367edbe04c852f48c99204/default';
                s1.charset='UTF-8';
                s1.setAttribute('crossorigin','*');
                s0.parentNode.insertBefore(s1,s0);
                })();
            </script>
          <?php endif; ?>
        <?php endif; ?>
        <!--End of Tawk.to Script-->
    <?php if(Sentinel::check() && session('browser') == "android"): ?>

            <script type="text/javascript">
                var firebase_token = android.getFirebaseToken();
                var app_version = android.getApplicationVersion();
                  if((firebase_token != null && firebase_token != "<?php echo e(Sentinel::getUser()->firebase); ?>") || (app_version != null && app_version != "<?php echo e(Sentinel::getUser()->app_version); ?>"))  {
                    $.ajax({
                    url: 'firebase',
                    method: 'POST',
                    data:{
                      firebase_token: firebase_token,
                      app_version, app_version,
                      os: 1,
                      _token: "<?php echo e(csrf_token()); ?>"
                    },
                    dataType: 'json',
                    success: function(data) {
                      JSON.stringify(data); //to string
                    }
                    });
                  }
                

              function showInterstitial() {
                android.showInterstitial();
              }
              
              function buyPremium(membership, user_id) {
                android.buyPremium(membership, user_id);
              }
            </script>
        <?php elseif(Sentinel::check() && session('browser') == "ios"): ?>

        
        <script type="text/javascript">
          /* wk.bridge.min.js | v0.2 */
          (function(){if(window.isIOS){return}window.isIOS=function(){return navigator&&navigator.userAgent&&(/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent))}}());
          (function(){if(window.bridge){return}window.bridge=function(){var callbacks=[],callbackID=0,registerHandlers=[];document.addEventListener("PacificDidReceiveNativeCallback",function(e){if(e.detail){var detail=e.detail;var id=isNaN(parseInt(detail.id))?-1:parseInt(detail.id);if(id!=-1){callbacks[id]&&callbacks[id](detail.parameters,detail.error);delete callbacks[id]}}},false);document.addEventListener("PacificDidReceiveNativeBroadcast",function(e){if(e.detail){var detail=e.detail;var name=detail.name;if(name!==undefined&&registerHandlers[name]){var namedListeners=registerHandlers[name];if(namedListeners instanceof Array){var parameters=detail.parameters;namedListeners.forEach(function(handler){handler(parameters)})}}}},false);return{"post":function(action,parameters,callback,print){var id=callbackID++;callbacks[id]=callback;if(window.webkit&&window.webkit.messageHandlers&&window.webkit.messageHandlers.pacific){window.webkit.messageHandlers.pacific.postMessage({"action":action,"parameters":parameters,"callback":id,"print":print||0})}},"on":function(name,callback){var namedListeners=registerHandlers[name];if(!namedListeners){registerHandlers[name]=namedListeners=[]}namedListeners.push(callback);return function(){namedListeners[indexOf(namedListeners,callback)]=null}},"off":function(name){delete registerHandlers[name]}}}()}());
        </script>

        <script type="text/javascript">
                window.bridge.post('firebasetoken', {}, (results, error) => {
                  var firebase_token = results.token;
                  if(firebase_token != null && firebase_token != "<?php echo e(Sentinel::getUser()->firebase); ?>")  {
                    $.ajax({
                    url: 'firebase',
                    method: 'POST',
                    data:{
                      firebase_token: firebase_token,
                      //app_version, app_version,
                      os: 2,
                      _token: "<?php echo e(csrf_token()); ?>"
                    },
                    dataType: 'json',
                    success: function(data) {
                      JSON.stringify(data); //to string
                    }
                    });
                  }
                  //alert(firebase_token);
              });


                
                
                  
            </script>
        <?php endif; ?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-123750533-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-123750533-1');
</script>


  <?php echo $__env->make('sweet::alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>