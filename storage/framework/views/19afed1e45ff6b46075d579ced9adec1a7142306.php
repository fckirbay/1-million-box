    <div id="preloader" class="preloader-light">
        <h1 class="center-text color-black ultrabold uppercase bottom-0 fa-2x"><?php echo app('translator')->getFromJson('general.lucky'); ?><span class="color-red-dark"><?php echo app('translator')->getFromJson('general.box'); ?></span></h1>
        <div id="preload-spinner"></div>
        <p><?php echo app('translator')->getFromJson('general.time_to_win'); ?></p>
        <em><?php echo app('translator')->getFromJson('general.most_profitable_game'); ?></em>
    </div>