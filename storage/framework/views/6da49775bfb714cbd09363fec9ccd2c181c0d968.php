<?php $__env->startSection('styles'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
	<!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Sayfalar <a href="<?php echo e(url('admin/site-management/new-page')); ?>" class="btn btn-primary btn-xs">Yeni Ekle</a></h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Ara">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th style="width:40%">Başlık</th>
                  <th style="width:15%">Kategori</th>
                  <th style="width:15%">Yayınlanma Tarihi</th>
                  <th style="width:20%">İşlem</th>
                </tr>
                <?php $__empty_1 = true; $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                <tr>
                  <td><?php echo e($val->title); ?></td>
                  <td><?php echo e($val->category); ?></td>
                  <td><?php echo e(Carbon\Carbon::parse($val->created_at)->format('H:i:s d.m.Y')); ?></td>
                  <td><a href="<?php echo e(url('admin/site-management/edit-page', $val->id)); ?>" class="btn btn-primary btn-xs">Düzenle</a> <a href="<?php echo e(url('admin/site-management/delete-page', $val->id)); ?>" class="btn btn-danger btn-xs">Sil</a></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <tr>
                    <td colspan="4" style="text-align:center">Hiç kayıt bulunamadı.</td>
                </tr>
                <?php endif; ?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
  <?php echo $__env->make('sweet::alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>