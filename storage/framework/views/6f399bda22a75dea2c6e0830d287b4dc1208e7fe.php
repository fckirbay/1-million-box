<?php $__env->startSection('styles'); ?>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="row">
    <!-- ============================================== SIDEBAR ============================================== -->	
	<div class="col-xs-12 col-sm-12 col-md-3 sidebar">
	     <?php echo $__env->make('partials.theme.top-navigation', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	     <?php echo $__env->make('partials.theme.month-deals', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	     <?php echo $__env->make('partials.theme.special-offer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	     <?php echo $__env->make('partials.theme.special-deals', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	     <?php echo $__env->make('partials.theme.testimonials', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	     <div class="home-banner">
             <img src="<?php echo e(asset('assets/theme/images/banners/LHS-banner.jpg')); ?>" alt="Image">
         </div> 
	</div>
	<!-- ============================================== CONTENT ============================================== -->
		<div class="col-xs-12 col-sm-12 col-md-9 homebanner-holder">
			<!-- ========================================== SECTION – HERO ========================================= -->
			
        <div id="hero">
        	<div id="owl-main" class="owl-carousel owl-inner-nav owl-ui-sm">
        		
        		<?php $__empty_1 = true; $__currentLoopData = $slider; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
        		<div class="item" style="background-image: url(<?php echo e(asset($val->photo)); ?>);">
        			<div class="container-fluid">
        				<div class="caption bg-color vertical-center text-left">
                            <div class="slider-header fadeInDown-1"><?php echo $val->title_1; ?></div>
        					<div class="big-text fadeInDown-1">
        						<?php echo $val->title_2; ?>

        					</div>
        
        					<div class="excerpt fadeInDown-2 hidden-xs">
        					
        					<span><?php echo $val->title_3; ?></span>
        
        					</div>
        					<div class="button-holder fadeInDown-3">
        						<a href="<?php echo e(url($val->button_link)); ?>" class="btn-lg btn btn-uppercase btn-primary shop-now-button"><?php echo e($val->button_title); ?></a>
        					</div>
        				</div><!-- /.caption -->
        			</div><!-- /.container-fluid -->
        		</div><!-- /.item -->
        		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
        		<?php endif; ?>
        
        	</div><!-- /.owl-carousel -->
        </div>
        			
        <!-- ========================================= SECTION – HERO : END ========================================= -->	
        
        			<!-- ============================================== INFO BOXES ============================================== -->
        <div class="info-boxes wow fadeInUp">
        	<div class="info-boxes-inner">
        		<div class="row">
        			<div class="col-md-6 col-sm-4 col-lg-4">
        				<div class="info-box">
        					<div class="row">
        						
        						<div class="col-xs-12">
        							<h4 class="info-box-heading green">para iade</h4>
        						</div>
        					</div>	
        					<h6 class="text">30 Gün içinde para iadesi</h6>
        				</div>
        			</div><!-- .col -->
        
        			<div class="hidden-md col-sm-4 col-lg-4">
        				<div class="info-box">
        					<div class="row">
        						
        						<div class="col-xs-12">
        							<h4 class="info-box-heading green">ücretsiz kargo</h4>
        						</div>
        					</div>
        					<h6 class="text">100₺ üstü ücretsiz kargo</h6>	
        				</div>
        			</div><!-- .col -->
        
        			<div class="col-md-6 col-sm-4 col-lg-4">
        				<div class="info-box">
        					<div class="row">
        						
        						<div class="col-xs-12">
        							<h4 class="info-box-heading green">özel indirimler</h4>
        						</div>
        					</div>
        					<h6 class="text">Peluş ürünlerinde %20 indirim</h6>	
        				</div>
        			</div><!-- .col -->
        		</div><!-- /.row -->
        	</div><!-- /.info-boxes-inner -->
        	
        </div><!-- /.info-boxes -->
        <!-- ============================================== INFO BOXES : END ============================================== -->
        			<!-- ============================================== SCROLL TABS ============================================== -->
        <div id="product-tabs-slider" class="scroll-tabs outer-top-vs wow fadeInUp">
        	<div class="more-info-tab clearfix ">
        	   <h3 class="new-product-title pull-left">Yeni Ürünler</h3>
        		<ul class="nav nav-tabs nav-tab-line pull-right" id="new-products-1">
        			<li class="active"><a data-transition-type="backSlide" href="#all" data-toggle="tab">Tümü</a></li>
        			<li><a data-transition-type="backSlide" href="#smartphone" data-toggle="tab">Giyim</a></li>
        			<li><a data-transition-type="backSlide" href="#laptop" data-toggle="tab">Elektronik</a></li>
        			<li><a data-transition-type="backSlide" href="#apple" data-toggle="tab">Ayakkabı</a></li>
        		</ul><!-- /.nav-tabs -->
        	</div>
        
        	<div class="tab-content outer-top-xs">
        		<div class="tab-pane in active" id="all">			
        			<div class="product-slider">
        				<div class="owl-carousel home-owl-carousel custom-carousel owl-theme" data-item="4">
        				    	
        		
        	
        		
        	<?php $__empty_1 = true; $__currentLoopData = $newProducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
        		<div class="item item-carousel">
        			<div class="products">
        				
                	<div class="product">		
                		<div class="product-image">
                			<div class="image">
                				<a href="<?php echo e(url($val->slug)); ?>"><img class="manual" src="<?php echo e(asset($val->photo)); ?>" alt=""></a>
                			</div><!-- /.image -->			
                
                			<div class="tag sale"><span>yeni</span></div>            		   
                		</div><!-- /.product-image -->
        			
        		
        		<div class="product-info text-left">
        			<h3 class="name center"><a href="<?php echo e(url($val->slug)); ?>"><?php echo e($val->title); ?></a></h3>
        			<!--<div class="rating rateit-small"></div>-->
        			<div class="description"></div>
        
        			<div class="product-price center">	
        				<span class="price">
        					<?php echo e(number_format($val->price, 2, ',', '.')); ?>₺				</span>
        					<?php if($val->old_price > 0): ?>
        										     <span class="price-before-discount"><?php echo e(number_format($val->old_price, 2, ',', '.')); ?>₺</span>
        										     <?php endif; ?>
        									
        			</div><!-- /.product-price -->
        			
        		</div><!-- /.product-info -->
        					<div class="cart clearfix animate-effect">
        				<div class="action">
        					<ul class="list-unstyled">
        						<li class="lnk wishlist">
        							<a data-toggle="tooltip" class="add-to-cart" href="<?php echo e(url($val->slug)); ?>">
        								 <i class="icon fa fa-search"></i> Ürünü İncele
        							</a>
        						</li>
        					</ul>
        				</div><!-- /.action -->
        			</div><!-- /.cart -->
        			</div><!-- /.product -->
              
        			</div><!-- /.products -->
        		</div><!-- /.item -->
        		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
        		<?php endif; ?>
        	
        		
        	
        		
        						</div><!-- /.home-owl-carousel -->
        			</div><!-- /.product-slider -->
        		</div><!-- /.tab-pane -->
        
        		<div class="tab-pane" id="smartphone">
        			<div class="product-slider">
        				<div class="owl-carousel home-owl-carousel custom-carousel owl-theme">
        				    	
        		<?php $__empty_1 = true; $__currentLoopData = $newProducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
        		<div class="item item-carousel">
        			<div class="products">
        				
                	<div class="product">		
                		<div class="product-image">
                			<div class="image">
                				<a href="<?php echo e(url($val->slug)); ?>"><img class="manual" src="<?php echo e(asset($val->photo)); ?>" alt=""></a>
                			</div><!-- /.image -->			
                
                			<div class="tag sale"><span>yeni</span></div>            		   
                		</div><!-- /.product-image -->
        			
        		
        		<div class="product-info text-left">
        			<h3 class="name"><a href="<?php echo e(url($val->slug)); ?>"><?php echo e($val->title); ?></a></h3>
        			<!--<div class="rating rateit-small"></div>-->
        			<div class="description"></div>
        
        			<div class="product-price">	
        				<span class="price">
        					<?php echo e(number_format($val->price, 2, ',', '.')); ?>₺				</span>
        					<?php if($val->old_price > 0): ?>
        										     <span class="price-before-discount"><?php echo e(number_format($val->old_price, 2, ',', '.')); ?>₺</span>
        										     <?php endif; ?>
        									
        			</div><!-- /.product-price -->
        			
        		</div><!-- /.product-info -->
        					<div class="cart clearfix animate-effect">
        				<div class="action">
        					<ul class="list-unstyled">
        						<li class="lnk wishlist">
        							<a data-toggle="tooltip" class="add-to-cart" href="<?php echo e(url($val->slug)); ?>">
        								 <i class="icon fa fa-search"></i> Ürünü İncele
        							</a>
        						</li>
        					</ul>
        				</div><!-- /.action -->
        			</div><!-- /.cart -->
        			</div><!-- /.product -->
              
        			</div><!-- /.products -->
        		</div><!-- /.item -->
        		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
        		<?php endif; ?>
        	
        	
        						</div><!-- /.home-owl-carousel -->
        			</div><!-- /.product-slider -->
        		</div><!-- /.tab-pane -->
        
        		<div class="tab-pane" id="laptop">
        			<div class="product-slider">
        				<div class="owl-carousel home-owl-carousel custom-carousel owl-theme">
        				    	
        		<?php $__empty_1 = true; $__currentLoopData = $newProducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
        		<div class="item item-carousel">
        			<div class="products">
        				
                	<div class="product">		
                		<div class="product-image">
                			<div class="image">
                				<a href="<?php echo e(url($val->slug)); ?>"><img class="manual" src="<?php echo e(asset($val->photo)); ?>" alt=""></a>
                			</div><!-- /.image -->			
                
                			<div class="tag sale"><span>yeni</span></div>            		   
                		</div><!-- /.product-image -->
        			
        		
        		<div class="product-info text-left">
        			<h3 class="name"><a href="<?php echo e(url($val->slug)); ?>"><?php echo e($val->title); ?></a></h3>
        			<!--<div class="rating rateit-small"></div>-->
        			<div class="description"></div>
        
        			<div class="product-price">	
        				<span class="price">
        					<?php echo e(number_format($val->price, 2, ',', '.')); ?>₺				</span>
        					<?php if($val->old_price > 0): ?>
        										     <span class="price-before-discount"><?php echo e(number_format($val->old_price, 2, ',', '.')); ?>₺</span>
        										     <?php endif; ?>
        									
        			</div><!-- /.product-price -->
        			
        		</div><!-- /.product-info -->
        					<div class="cart clearfix animate-effect">
        				<div class="action">
        					<ul class="list-unstyled">
        						<li class="lnk wishlist">
        							<a data-toggle="tooltip" class="add-to-cart" href="<?php echo e(url($val->slug)); ?>">
        								 <i class="icon fa fa-search"></i> Ürünü İncele
        							</a>
        						</li>
        					</ul>
        				</div><!-- /.action -->
        			</div><!-- /.cart -->
        			</div><!-- /.product -->
              
        			</div><!-- /.products -->
        		</div><!-- /.item -->
        		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
        		<?php endif; ?>
        	
        		
        	
        		
        						</div><!-- /.home-owl-carousel -->
        			</div><!-- /.product-slider -->
        		</div><!-- /.tab-pane -->
        
        		<div class="tab-pane" id="apple">
        			<div class="product-slider">
        				<div class="owl-carousel home-owl-carousel custom-carousel owl-theme">
        				    	
        		<?php $__empty_1 = true; $__currentLoopData = $newProducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
        		<div class="item item-carousel">
        			<div class="products">
        				
                	<div class="product">		
                		<div class="product-image">
                			<div class="image">
                				<a href="<?php echo e(url($val->slug)); ?>"><img class="manual" src="<?php echo e(asset($val->photo)); ?>" alt=""></a>
                			</div><!-- /.image -->			
                
                			<div class="tag sale"><span>yeni</span></div>            		   
                		</div><!-- /.product-image -->
        			
        		
        		<div class="product-info text-left">
        			<h3 class="name"><a href="<?php echo e(url($val->slug)); ?>"><?php echo e($val->title); ?></a></h3>
        			<!--<div class="rating rateit-small"></div>-->
        			<div class="description"></div>
        
        			<div class="product-price">	
        				<span class="price">
        					<?php echo e(number_format($val->price, 2, ',', '.')); ?>₺				</span>
        					<?php if($val->old_price > 0): ?>
        										     <span class="price-before-discount"><?php echo e(number_format($val->old_price, 2, ',', '.')); ?>₺</span>
        										     <?php endif; ?>
        									
        			</div><!-- /.product-price -->
        			
        		</div><!-- /.product-info -->
        					<div class="cart clearfix animate-effect">
        				<div class="action">
        					<ul class="list-unstyled">
        						<li class="lnk wishlist">
        							<a data-toggle="tooltip" class="add-to-cart" href="<?php echo e(url($val->slug)); ?>">
        								 <i class="icon fa fa-search"></i> Ürünü İncele
        							</a>
        						</li>
        					</ul>
        				</div><!-- /.action -->
        			</div><!-- /.cart -->
        			</div><!-- /.product -->
              
        			</div><!-- /.products -->
        		</div><!-- /.item -->
        		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
        		<?php endif; ?>
        	
        		
        						</div><!-- /.home-owl-carousel -->
        			</div><!-- /.product-slider -->
        		</div><!-- /.tab-pane -->
        
        	</div><!-- /.tab-content -->
        </div><!-- /.scroll-tabs -->
        <!-- ============================================== SCROLL TABS : END ============================================== -->
        			<!-- ============================================== WIDE PRODUCTS ============================================== -->
        <div class="wide-banners wow fadeInUp outer-bottom-xs">
        	<div class="row">
        <div class="col-md-7 col-sm-7">
        <div class="wide-banner cnt-strip">
        <div class="image">
        <img class="img-responsive" src="<?php echo e(asset('assets/theme/images/banners/home-banner-2.jpg')); ?>" alt="">
        </div>
        
        </div><!-- /.wide-banner -->
        </div><!-- /.col -->
        <div class="col-md-5 col-sm-5">
        <div class="wide-banner cnt-strip">
        <div class="image">
        <img class="img-responsive" src="<?php echo e(asset('assets/theme/images/banners/home-banner-3.jpg')); ?>" alt="">
        </div>
        
        </div><!-- /.wide-banner -->
        </div><!-- /.col -->
        </div><!-- /.row -->
        </div><!-- /.wide-banners -->
        
        <!-- ============================================== WIDE PRODUCTS : END ============================================== -->
        			<!-- ============================================== FEATURED PRODUCTS ============================================== -->
        <section class="section featured-product wow fadeInUp">
        	<h3 class="section-title">Seçilmiş Ürünler</h3>
        	<div class="owl-carousel home-owl-carousel custom-carousel owl-theme outer-top-xs">
        	    	
        		<?php $__empty_1 = true; $__currentLoopData = $featuredProducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
        		<div class="item item-carousel">
        			<div class="products">
        				
                	<div class="product">		
                		<div class="product-image">
                			<div class="image">
                				<a href="<?php echo e(url($val->slug)); ?>"><img class="manual" src="<?php echo e(asset($val->photo)); ?>" alt=""></a>
                			</div><!-- /.image -->			
                
                			<!--<div class="tag sale"><span>yeni</span></div>-->          		   
                		</div><!-- /.product-image -->
        			
        		
        		<div class="product-info text-left">
        			<h3 class="name center"><a href="<?php echo e(url($val->slug)); ?>"><?php echo e($val->title); ?></a></h3>
        			<!--<div class="rating rateit-small"></div>-->
        			<div class="description"></div>
        
        			<div class="product-price center">	
        				<span class="price">
        					<?php echo e(number_format($val->price, 2, ',', '.')); ?>₺				</span>
        					<?php if($val->old_price > 0): ?>
        										     <span class="price-before-discount"><?php echo e(number_format($val->old_price, 2, ',', '.')); ?>₺</span>
        										     <?php endif; ?>
        									
        			</div><!-- /.product-price -->
        			
        		</div><!-- /.product-info -->
        					<div class="cart clearfix animate-effect">
        				<div class="action">
        					<ul class="list-unstyled">
        						<li class="lnk wishlist">
        							<a data-toggle="tooltip" class="add-to-cart" href="<?php echo e(url($val->slug)); ?>">
        								 <i class="icon fa fa-search"></i> Ürünü İncele
        							</a>
        						</li>
        					</ul>
        				</div><!-- /.action -->
        			</div><!-- /.cart -->
        			</div><!-- /.product -->
              
        			</div><!-- /.products -->
        		</div><!-- /.item -->
        		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
        		<?php endif; ?>
        	
        		
        			</div><!-- /.home-owl-carousel -->
        </section><!-- /.section -->
        <!-- ============================================== FEATURED PRODUCTS : END ============================================== -->
        			<!-- ============================================== WIDE PRODUCTS ============================================== -->
        <div class="wide-banners wow fadeInUp outer-bottom-xs">
        	<div class="row">
        
        		<div class="col-md-12">
        			<div class="wide-banner cnt-strip">
        				<div class="image">
        					<img class="img-responsive" src="<?php echo e(asset('assets/theme/images/banners/home-banner-1.jpg')); ?>" alt="">
        				</div>	
        				<div class="strip strip-text">
        					<div class="strip-inner">
        						<h2 class="text-right">Erkek Yaz Modası<br>
        						<span class="shopping-needs">%40'a varan indirimler</span></h2>
        					</div>	
        				</div>
        				<div class="new-label">
        				    <div class="text">YENİ</div>
        				</div><!-- /.new-label -->
        			</div><!-- /.wide-banner -->
        		</div><!-- /.col -->
        
        	</div><!-- /.row -->
        </div><!-- /.wide-banners -->
        <!-- ============================================== WIDE PRODUCTS : END ============================================== -->
        			<!-- ============================================== BEST SELLER ============================================== -->
        
        <div class="best-deal wow fadeInUp outer-bottom-xs">
        	<h3 class="section-title">Çok Satanlar</h3>
        	<div class="sidebar-widget-body outer-top-xs">
        		<div class="owl-carousel best-seller custom-carousel owl-theme outer-top-xs">
        	        	        <div class="item">
        	        	<div class="products best-product">
        		        							<div class="product">
        							<div class="product-micro">
        	<div class="row product-micro-row">
        		<div class="col col-xs-5">
        			<div class="product-image">
        				<div class="image">
        					<a href="<?php echo e(url($topProducts[0]->slug)); ?>">
        						<img src="<?php echo e(asset($topProducts[0]->photo)); ?>" class="manual-mini" alt="">
        					</a>					
        				</div><!-- /.image -->
        					
        											
        					
        								</div><!-- /.product-image -->
        		</div><!-- /.col -->
        		<div class="col2 col-xs-7">
        			<div class="product-info">
        				<h3 class="name"><a href="<?php echo e(url($topProducts[0]->slug)); ?>"><?php echo e($topProducts[0]->title); ?></a></h3>
        			<!--<div class="rating rateit-small"></div>-->
        				<div class="product-price">	
        				<span class="price">
        					<?php echo e(number_format($topProducts[0]->price, 2, ',', '.')); ?>₺				</span>
        				
        			</div><!-- /.product-price -->
        			
        			</div>
        		</div><!-- /.col -->
        	</div><!-- /.product-micro-row -->
        </div><!-- /.product-micro -->
              
        						</div>
        		        							<div class="product">
        							<div class="product-micro">
        	<div class="row product-micro-row">
        		<div class="col col-xs-5">
        			<div class="product-image">
        				<div class="image">
        					<a href="<?php echo e(url($topProducts[0]->slug)); ?>">
        						<img src="<?php echo e(asset($topProducts[1]->photo)); ?>" class="manual-mini" alt="">
        					</a>					
        				</div><!-- /.image -->
        					
        											
        					
        								</div><!-- /.product-image -->
        		</div><!-- /.col -->
        		<div class="col2 col-xs-7">
        			<div class="product-info">
        				<h3 class="name"><a href="<?php echo e(url($topProducts[0]->slug)); ?>"><?php echo e($topProducts[1]->title); ?></a></h3>
        			<!--<div class="rating rateit-small"></div>-->
        				<div class="product-price">	
        				<span class="price">
        					<?php echo e(number_format($topProducts[1]->price, 2, ',', '.')); ?>₺				</span>
        				
        			</div><!-- /.product-price -->
        			
        			</div>
        		</div><!-- /.col -->
        	</div><!-- /.product-micro-row -->
        </div><!-- /.product-micro -->
              
        						</div>
        		        		        	</div>
        	        </div>
        	    		        <div class="item">
        	        	<div class="products best-product">
        		        							<div class="product">
        							<div class="product-micro">
        	<div class="row product-micro-row">
        		<div class="col col-xs-5">
        			<div class="product-image">
        				<div class="image">
        					<a href="<?php echo e(url($topProducts[0]->slug)); ?>">
        						<img src="<?php echo e(asset($topProducts[2]->photo)); ?>" class="manual-mini" alt="">
        					</a>					
        				</div><!-- /.image -->
        					
        											
        					
        								</div><!-- /.product-image -->
        		</div><!-- /.col -->
        		<div class="col2 col-xs-7">
        			<div class="product-info">
        				<h3 class="name"><a href="<?php echo e(url($topProducts[0]->slug)); ?>"><?php echo e($topProducts[2]->title); ?></a></h3>
        			<!--<div class="rating rateit-small"></div>-->
        				<div class="product-price">	
        				<span class="price">
        					<?php echo e(number_format($topProducts[2]->price, 2, ',', '.')); ?>₺				</span>
        				
        			</div><!-- /.product-price -->
        			
        			</div>
        		</div><!-- /.col -->
        	</div><!-- /.product-micro-row -->
        </div><!-- /.product-micro -->
              
        						</div>
        		        							<div class="product">
        							<div class="product-micro">
        	<div class="row product-micro-row">
        		<div class="col col-xs-5">
        			<div class="product-image">
        				<div class="image">
        					<a href="<?php echo e(url($topProducts[0]->slug)); ?>">
        						<img src="<?php echo e(asset($topProducts[3]->photo)); ?>" class="manual-mini" alt="">
        					</a>					
        				</div><!-- /.image -->
        					
        											
        					
        								</div><!-- /.product-image -->
        		</div><!-- /.col -->
        		<div class="col2 col-xs-7">
        			<div class="product-info">
        				<h3 class="name"><a href="<?php echo e(url($topProducts[0]->slug)); ?>"><?php echo e($topProducts[3]->title); ?></a></h3>
        			<!--<div class="rating rateit-small"></div>-->
        				<div class="product-price">	
        				<span class="price">
        					<?php echo e(number_format($topProducts[3]->price, 2, ',', '.')); ?>₺				</span>
        				
        			</div><!-- /.product-price -->
        			
        			</div>
        		</div><!-- /.col -->
        	</div><!-- /.product-micro-row -->
        </div><!-- /.product-micro -->
              
        						</div>
        		        		        	</div>
        	        </div>
        	    		        <div class="item">
        	        	<div class="products best-product">
        		        							<div class="product">
        							<div class="product-micro">
        	<div class="row product-micro-row">
        		<div class="col col-xs-5">
        			<div class="product-image">
        				<div class="image">
        					<a href="<?php echo e(url($topProducts[0]->slug)); ?>">
        						<img src="<?php echo e(asset($topProducts[4]->photo)); ?>" class="manual-mini" alt="">
        					</a>					
        				</div><!-- /.image -->
        					
        											
        					
        								</div><!-- /.product-image -->
        		</div><!-- /.col -->
        		<div class="col2 col-xs-7">
        			<div class="product-info">
        				<h3 class="name"><a href="<?php echo e(url($topProducts[0]->slug)); ?>"><?php echo e($topProducts[4]->title); ?></a></h3>
        			<!--<div class="rating rateit-small"></div>-->
        				<div class="product-price">	
        				<span class="price">
        					<?php echo e(number_format($topProducts[4]->price, 2, ',', '.')); ?>₺				</span>
        				
        			</div><!-- /.product-price -->
        			
        			</div>
        		</div><!-- /.col -->
        	</div><!-- /.product-micro-row -->
        </div><!-- /.product-micro -->
              
        						</div>
        		        							<div class="product">
        							<div class="product-micro">
        	<div class="row product-micro-row">
        		<div class="col col-xs-5">
        			<div class="product-image">
        				<div class="image">
        					<a href="<?php echo e(url($topProducts[0]->slug)); ?>">
        						<img src="<?php echo e(asset($topProducts[5]->photo)); ?>" class="manual-mini" alt="">
        					</a>					
        				</div><!-- /.image -->
        					
        											
        					
        								</div><!-- /.product-image -->
        		</div><!-- /.col -->
        		<div class="col2 col-xs-7">
        			<div class="product-info">
        				<h3 class="name"><a href="<?php echo e(url($topProducts[0]->slug)); ?>"><?php echo e($topProducts[5]->title); ?></a></h3>
        			<!--<div class="rating rateit-small"></div>-->
        				<div class="product-price">	
        				<span class="price">
        					<?php echo e(number_format($topProducts[5]->price, 2, ',', '.')); ?>₺				</span>
        				
        			</div><!-- /.product-price -->
        			
        			</div>
        		</div><!-- /.col -->
        	</div><!-- /.product-micro-row -->
        </div><!-- /.product-micro -->
              
        						</div>
        		        		        	</div>
        	        </div>
        	    		        <div class="item">
        	        	<div class="products best-product">
        		        							<div class="product">
        							<div class="product-micro">
        	<div class="row product-micro-row">
        		<div class="col col-xs-5">
        			<div class="product-image">
        				<div class="image">
        					<a href="<?php echo e(url($topProducts[0]->slug)); ?>">
        						<img src="<?php echo e(asset($topProducts[6]->photo)); ?>" class="manual-mini" alt="">
        					</a>					
        				</div><!-- /.image -->
        					
        											
        					
        								</div><!-- /.product-image -->
        		</div><!-- /.col -->
        		<div class="col2 col-xs-7">
        			<div class="product-info">
        				<h3 class="name"><a href="<?php echo e(url($topProducts[0]->slug)); ?>"><?php echo e($topProducts[6]->title); ?></a></h3>
        			<!--<div class="rating rateit-small"></div>-->
        				<div class="product-price">	
        				<span class="price">
        					<?php echo e(number_format($topProducts[6]->price, 2, ',', '.')); ?>₺				</span>
        				
        			</div><!-- /.product-price -->
        			
        			</div>
        		</div><!-- /.col -->
        	</div><!-- /.product-micro-row -->
        </div><!-- /.product-micro -->
              
        						</div>
        		        							<div class="product">
        							<div class="product-micro">
        	<div class="row product-micro-row">
        		<div class="col col-xs-5">
        			<div class="product-image">
        				<div class="image">
        					<a href="<?php echo e(url($topProducts[0]->slug)); ?>">
        						<img src="<?php echo e(asset($topProducts[7]->photo)); ?>" class="manual-mini" alt="">
        					</a>					
        				</div><!-- /.image -->
        					
        											
        					
        								</div><!-- /.product-image -->
        		</div><!-- /.col -->
        		<div class="col2 col-xs-7">
        			<div class="product-info">
        				<h3 class="name"><a href="<?php echo e(url($topProducts[0]->slug)); ?>"><?php echo e($topProducts[7]->title); ?></a></h3>
        			<!--<div class="rating rateit-small"></div>-->
        				<div class="product-price">	
        				<span class="price">
        					<?php echo e(number_format($topProducts[7]->price, 2, ',', '.')); ?>₺				</span>
        				
        			</div><!-- /.product-price -->
        			
        			</div>
        		</div><!-- /.col -->
        	</div><!-- /.product-micro-row -->
        </div><!-- /.product-micro -->
              
        						</div>
        		        		        	</div>
        	        </div>
        	    		    </div>
        	</div><!-- /.sidebar-widget-body -->
        </div><!-- /.sidebar-widget -->
        <!-- ============================================== BEST SELLER : END ============================================== -->	
        
        			<!-- ============================================== BLOG SLIDER ============================================== -->
        <section class="section latest-blog outer-bottom-vs wow fadeInUp">
        	<h3 class="section-title">son yazılar</h3>
        	<div class="blog-slider-container outer-top-xs">
        		<div class="owl-carousel blog-slider custom-carousel">
        		    
        				<?php $__empty_1 = true; $__currentLoopData = $blog; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
        				<div class="item">
        					<div class="blog-post">
        						<div class="blog-post-image">
        							<div class="image">
        								<a href="blog.html"><img src="<?php echo e(asset($val->image)); ?>" alt="" style="height:220px; width:100%"></a>
        							</div>
        						</div><!-- /.blog-post-image -->
        					
        					
        						<div class="blog-post-info text-left">
        							<h3 class="name"><a href="<?php echo e(url('blog', $val->slug)); ?>"><?php echo e($val->title); ?></a></h3>	
        							<span class="info">byDukkan &nbsp;|&nbsp; <?php echo e(Carbon\Carbon::parse($val->created_at)->format('d.m.Y')); ?></span>
        							<p class="text"><?php echo e(substr($val->content, 0, 100)); ?>...</p>
        							<a href="<?php echo e(url('blog', $val->slug)); ?>" class="lnk btn btn-primary">Devamı</a>
        						</div><!-- /.blog-post-info -->
        						
        						
        					</div><!-- /.blog-post -->
        				</div><!-- /.item -->
        				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
        				<?php endif; ?>
        			
        						
        		</div><!-- /.owl-carousel -->
        	</div><!-- /.blog-slider-container -->
        </section><!-- /.section -->
        <!-- ============================================== BLOG SLIDER : END ============================================== -->	
        
        			<!-- ============================================== FEATURED PRODUCTS ============================================== -->
        <section class="section wow fadeInUp new-arriavls">
        	<h3 class="section-title">Yeni Gelenler</h3>
        	<div class="owl-carousel home-owl-carousel custom-carousel owl-theme outer-top-xs">
        	    	
        		
        		<?php $__empty_1 = true; $__currentLoopData = $newProducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
        		<div class="item item-carousel">
        			<div class="products">
        				
                	<div class="product">		
                		<div class="product-image">
                			<div class="image">
                				<a href="<?php echo e(url($val->slug)); ?>"><img class="manual" src="<?php echo e(asset($val->photo)); ?>" alt=""></a>
                			</div><!-- /.image -->			
                
                			<div class="tag sale"><span>yeni</span></div>            		   
                		</div><!-- /.product-image -->
        			
        		
        		<div class="product-info text-left">
        			<h3 class="name center"><a href="<?php echo e(url($val->slug)); ?>"><?php echo e($val->title); ?></a></h3>
        			<!--<div class="rating rateit-small"></div>-->
        			<div class="description"></div>
        
        			<div class="product-price center">	
        				<span class="price">
        					<?php echo e(number_format($val->price, 2, ',', '.')); ?>₺				</span>
        					<?php if($val->old_price > 0): ?>
        										     <span class="price-before-discount"><?php echo e(number_format($val->old_price, 2, ',', '.')); ?>₺</span>
        										     <?php endif; ?>
        									
        			</div><!-- /.product-price -->
        			
        		</div><!-- /.product-info -->
        					<div class="cart clearfix animate-effect">
        				<div class="action">
        					<ul class="list-unstyled">
        						<li class="lnk wishlist">
        							<a data-toggle="tooltip" class="add-to-cart" href="<?php echo e(url($val->slug)); ?>">
        								 <i class="icon fa fa-search"></i> Ürünü İncele
        							</a>
        						</li>
        					</ul>
        				</div><!-- /.action -->
        			</div><!-- /.cart -->
        			</div><!-- /.product -->
              
        			</div><!-- /.products -->
        		</div><!-- /.item -->
        		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
        		<?php endif; ?>
        			</div><!-- /.home-owl-carousel -->
        </section><!-- /.section -->
        <!-- ============================================== FEATURED PRODUCTS : END ============================================== -->

	</div><!-- /.homebanner-holder -->
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.theme.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>