<!DOCTYPE html>
<html lang="tr">
    <head>
    <?php echo $__env->make('partials.theme.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->yieldContent('styles'); ?>
    </head>
    <body class="cnt-home">
        <?php echo $__env->make('partials.theme.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="body-content outer-top-xs" id="top-banner-and-menu">
    	    <div class="container">
                <?php echo $__env->yieldContent('content'); ?>
                <?php echo $__env->make('partials.theme.brands-carousel', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
        </div>
        <?php echo $__env->make('partials.theme.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->make('partials.theme.scripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->yieldContent('scripts'); ?>
    </body>
</html>