<?php if($paginator->hasPages()): ?>
        
        <div class="pagination">
        <?php if($paginator->onFirstPage()): ?>
            
        <?php else: ?>
            <a href="<?php echo e($paginator->previousPageUrl()); ?>" class="bg-gray-light" style="width:7%"><i class="fa fa-angle-left"></i></a>
        <?php endif; ?>

        
        <?php $__currentLoopData = $elements; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $element): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            
            <?php if(is_string($element)): ?>
                <a href="#" class="no-border" style="width:7%" disabled><?php echo e($element); ?></a>
            <?php endif; ?>

            
            <?php if(is_array($element)): ?>
                <?php $__currentLoopData = $element; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $page => $url): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($page == $paginator->currentPage()): ?>
                        <a href="#" class="bg-blue-dark color-white" style="width:7%"><?php echo e($page); ?></a>
                    <?php else: ?>
                        <a href="<?php echo e($url); ?>" style="width:7%"><?php echo e($page); ?></a>
                    <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        
        <?php if($paginator->hasMorePages()): ?>
            <a href="<?php echo e($paginator->nextPageUrl()); ?>" class="bg-gray-light" style="width:7%"><i class="fa fa-angle-right"></i></a>
        <?php else: ?>
            
        <?php endif; ?>
        </div>
<?php endif; ?>