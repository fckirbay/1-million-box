<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScratchWin extends Model
{
    protected $table = 'scratch_win';
    public $timestamps = false;
}
