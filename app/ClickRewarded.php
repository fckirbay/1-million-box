<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClickRewarded extends Model
{
    protected $table = 'click_rewarded';
    public $timestamps = false;
}
