<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticketing extends Model
{
    protected $table = 'ticketing';
    public $timestamps = false;
}
