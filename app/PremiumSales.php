<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PremiumSales extends Model
{
    protected $table = 'premium_sales';
    public $timestamps = false;
}
