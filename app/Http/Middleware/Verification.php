<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;
use Alert;
use Carbon;
use App\User;
use App\UserLocations;

class Verification
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if(Sentinel::check() && Sentinel::getUser()->verification == 0 && \Request::segment(1) != 'logout') {
            return redirect('verify');
            //return response()->view('mobile.verify');
        } elseif(Sentinel::check() && Sentinel::getUser()->blocked == 1) {
            Alert::error(trans('general.blocked_user'));
            Sentinel::logout();
            return redirect('/login');
        }

        

        if(Sentinel::check() && Sentinel::getUser()->premium_membership != null && Sentinel::getUser()->premium_expiration > Carbon\Carbon::now()) {
            session(['premium' => Sentinel::getUser()->premium_membership]);
            if(Sentinel::getUser()->premium_membership == "bronze_tr" || Sentinel::getUser()->premium_membership == "bronze_en") {
                session(['color' => '#CD7F32']);
                session(['extra' => 6]);
                session(['coefficient' => 1.25]);
            } elseif(Sentinel::getUser()->premium_membership == "silver_tr" || Sentinel::getUser()->premium_membership == "silver_en") {
                session(['color' => '#C0C0C0']);
                session(['livechat' => 'yes']);
                session(['ads' => 'no']);
                session(['extra' => 7]);
                session(['coefficient' => 1.50]);
            } elseif(Sentinel::getUser()->premium_membership == "gold_tr" || Sentinel::getUser()->premium_membership == "gold_en") {
                session(['color' => '#D4AF37']);
                session(['livechat' => 'yes']);
                session(['ads' => 'no']);
                session(['extra' => 8]);
                session(['coefficient' => 1.75]);
            } elseif(Sentinel::getUser()->premium_membership == "platinum_tr" || Sentinel::getUser()->premium_membership == "platinum_en") {
                session(['color' => '#E5E4E2']);
                session(['livechat' => 'yes']);
                session(['ads' => 'no']);
                session(['extra' => 10]);
                session(['coefficient' => 2.00]);
            } elseif(Sentinel::getUser()->premium_membership == "vip_tr" || Sentinel::getUser()->premium_membership == "vip_en") {
                session(['color' => '#77ec20']);
                session(['livechat' => 'yes']);
                session(['ads' => 'no']);
                session(['extra' => 10]);
                session(['coefficient' => 2.5]);
            }
        } else {
            $request->session()->forget('premium');
            $request->session()->forget('color');
            $request->session()->forget('livechat');
            $request->session()->forget('ads');
            $request->session()->forget('extra');
            $request->session()->forget('coefficient');
        }
		
		
        if(Sentinel::check()) {
            $ip = $_SERVER['REMOTE_ADDR'];
            if($ip != null && (Sentinel::getUser()->ip_address == null || Sentinel::getUser()->ip_address != $ip)) {
                $updateIp = User::where('id', Sentinel::getUser()->id)->update(['ip_address' => $ip]);

                $checkBefore = UserLocations::where('user_id', Sentinel::getUser()->id)->where('ip', $ip)->count();

                if($checkBefore == 0) {
                    // Get cURL resource
                    $curl = curl_init();
                    // Set some options - we are passing in a useragent too here
                    curl_setopt_array($curl, array(
                        CURLOPT_RETURNTRANSFER => 1,
                        CURLOPT_URL => 'https://iplocation.com',
                        CURLOPT_USERAGENT => 'IP Location',
                        CURLOPT_POST => 1,
                        CURLOPT_POSTFIELDS => array(
                            'ip' => $ip
                        )
                    ));
                    // Send the request & save response to $resp
                    $resp = curl_exec($curl);
                    $result = json_decode($resp);

                    if(isset($result->country_code) && isset($result->country_name)) {
                        $newLocation = new UserLocations();
                        $newLocation->user_id = Sentinel::getUser()->id;
                        $newLocation->ip = $ip;
                        $newLocation->country_code = $result->country_code;
                        $newLocation->country_name = $result->country_name;
                        $newLocation->latitude = $result->lat;
                        $newLocation->longitude = $result->lng;
                        $newLocation->zip_code = $result->postal_code;
                        $newLocation->region_code = $result->region;
                        $newLocation->region_name = $result->region_name;
                        $newLocation->time_zone = $result->region_name;
                        $newLocation->city = $result->city;
                        $newLocation->checkDate = Carbon\Carbon::now();
                        $newLocation->save();
                    }
                    // Close request to clear up some resources
                    curl_close($curl);
                }
            }
        }
		
        return $next($request);
        
    }
}
