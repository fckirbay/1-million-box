<?php

namespace App\Http\Controllers\Crons;

use App\Http\Controllers\Controller;
use Redirect;
use Illuminate\Http\Request;
use Sentinel;
use Validator;
use Alert;
use App\User;
use DB;
use Carbon;
use App\Notifications;

class SendNotificationsController extends Controller {
    
    public function setSendNotification() {
        
        $notifications = Notifications::where('status', 0)->orderBy('id', 'asc')->get();
                
        foreach($notifications as $key => $val) {
			if($val->firebase != null) {

				if($val->time == null || $val->time <= Carbon\Carbon::now()) {
			     	$msg = array
			          (
						'body' 	=> $val->notification,
						'title'	=> $val->title,
			            'icon'	=> 'smallicon',/*Default Icon*/
			            'sound' => 'mySound'
			          );

			          $fields = array
						(
							'to'			=> $val->firebase,
							'notification'	=> $msg
						);
				
					$headers = array
							(
								'Authorization: key=AIzaSyAgnz3ML4Lk-rREwdrDVuYz7uTRpDgA67o',
								'Content-Type: application/json'
							);
					#Send Reponse To FireBase Server	
						$ch = curl_init();
						curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
						curl_setopt( $ch,CURLOPT_POST, true );
						curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
						curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
						curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
						curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
						$result = curl_exec($ch );
						curl_close( $ch );
				
					$update = Notifications::where('id', $val->id)->update(['status'=>1]);
				}

				


			}
            
            
			
        }
        
    }

    public function setSendNotification2() {
        
        


			     $msg = array
			          (
						'body' 	=> 'Deniyoruz...',
						'title'	=> 'Deneme',
				        'icon'	=> 'smallicon',/*Default Icon*/
				        'sound' => 'mySound'/*Default sound*/
			          );

			    $datamsg = array
					(
					    'url'   => 'http://app.betint.tv/tr/match/7631'
					);

				$fields = array
						(
							'to'		=> 'eyfwOEzlPBc:APA91bFzCBC5oUVbPyuXb7i0d6chcg01b23y1Sp2MHjbaG6XFdKGF0yNWvHH-dBFR0bo2Pv0HUM7jEQFNUWDXa9R8bi3lYTCRgN-R81A_yZ2wDbsdQL7kVi4BEM7KOqgegdNORN52bH9',
							'notification'	=> $msg,
							'data'          => $datamsg
						);
				
				
				$headers = array
						(
							'Authorization: key=AIzaSyD2eKzkZdQPDxrHveCxg5A4e2vjV6Wm52w',
							'Content-Type: application/json'
						);
			#Send Reponse To FireBase Server	
					$ch = curl_init();
					curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
					curl_setopt( $ch,CURLOPT_POST, true );
					curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
					curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
					curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
					curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
					$result = curl_exec($ch );
					curl_close( $ch );
			

        
    }


    
}