<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;
use Redirect;
use Illuminate\Http\Request;
use Sentinel;
use Alert;

class LoginController extends Controller {

    public function getLogin() {
        
        return view('frontend/login');

    }
    
    public function getForgotPassword() {
        
        return view('frontend/forgot-password');

    }
    
    public function setLogin(Request $request) {
        
        $validator = Validator::make($request->all(), [
            'username' => 'required|max:30',
            'password' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        
        $credentials = [
         'email'    => $request->username . '@email.com',
         'password' => $request->password,
        ];
        
        $user = Sentinel::authenticateAndRemember($credentials);

        
        if($user != null) {

            if($user->blocked == 1) {
            
                if($user->blocked_reason != null) {
                    Alert::error($user->blocked_reason);
                } else {
                    Alert::error(\Lang::get('general.your_account_has_been_blocked'));
                }
                Sentinel::logout();
                return redirect('login');
            }

            return redirect('index');
            
        } else {
            Alert::error(\Lang::get('general.user_not_found'));
            return redirect('login');
        }

    }
    
    public function redirectToTwitter()
    {
        return \Socialite::driver('twitter')->redirect();
    }
    
    public function handleTwitterCallback()
    {
        $user = \Socialite::driver('twitter')->user();

        // $user->token;
    }
    
    public function redirectToFacebook()
    {
        return \Socialite::driver('twitter')->redirect();
    }
    
    public function handleFacebookCallback()
    {
        $user = \Socialite::driver('twitter')->user();

        // $user->token;
    }
    
    public function setForgotPassword(Request $request) {
        
        $validator = Validator::make($request->all(), [
            'phone' => 'required|max:20',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        

        
        $user = User::where('phone', $request->phone)->first();

        
        
        if($user && $user->verification == 1) {

            if($user->email_2 == null) {
                Alert::error(\Lang::get('general.you_dont_have_an_email_address'), \Lang::get('general.sorry'))->autoclose(4000);
                return Redirect::back();
            }

            $trys = PasswordRecovery::where('user_id', $user->id)->where('created_at', '>=', Carbon\Carbon::now()->subDay())->count();
            if($trys >= 3) {
                Alert::error(\Lang::get('general.you_can_request_up_to_3_times'), \Lang::get('general.sorry'))->autoclose(4000);
                return Redirect::back();
            }

            $last_try = PasswordRecovery::where('user_id', $user->id)->orderBy('created_at', 'desc')->first();
            if($last_try && $last_try->created_at > Carbon\Carbon::now()->subMinutes(5)) {
                Alert::error(\Lang::get('general.five_minutes_must_be_past_your_last_request'), \Lang::get('general.sorry'))->autoclose(4000);
                return Redirect::back();
            }

            $user = Sentinel::findById($user->id);

            if($user->firebase == null) {
                Alert::success(\Lang::get('general.your_password_couldnt_recover'), \Lang::get('general.sorry'))->autoclose(4000);
                return redirect('/login');
            }
            
            $title = "Password Recovery";
            $password = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 6)), 0, 6);

            $remind = new PasswordRecovery();
            $remind->user_id = $user->id;
            $remind->code = $password;
            $remind->created_at = Carbon\Carbon::now();
            $remind->save();

            $email = $user->email_2;
            $username = $user->first_name;
            
            if($user->lang == "tr") {
                $content= "Kullanıcı adınız: ". $username." Yeni şifreniz: ". $password ." Hesabınıza ilk girişinizde şifrenizi değiştirmeyi unutmayınız.";
                Mail::send('emails.send', ['content' => $content, 'username' => $username], function ($message) use ($email, $username)
                 {
                    $message->to($email)->subject('Şifre Sıfırlama ('.$username.')');
                    $message->from('info@luckybox.fun', 'LuckyBox');
                });
            } else {
                $content= "Your new password: <strong>". $password ."</strong> Don't forget to change your password at your first login.<br/><br /> System Date: ". Carbon\Carbon::now();
                Mail::send('emails.send', ['content' => $content, 'username' => $username], function ($message) use ($email, $username)
                 {
                    $message->to($email)->subject('Password Recovery ('.$username.')');
                    $message->from('info@luckybox.fun', 'LuckyBox');
                });
            }

            $credentials = [
                'password' => $password,
            ];
            $update = Sentinel::update($user, $credentials);


            $newNotify = new Notifications();
            $newNotify->user_id = $user->id;
            $newNotify->title = "Password Recovery!";
            $newNotify->notification = $content;
            $newNotify->firebase = $user->firebase;
            $newNotify->status = 0;
            $newNotify->created_at = Carbon\Carbon::now();
            $newNotify->save();

            

            Alert::success(\Lang::get('general.your_password_has_been_sent'), \Lang::get('general.congratulations'))->autoclose(4000);
            return redirect('/login');
        } elseif($user && $user->verification == 0) {
            Alert::error(\Lang::get('general.account_is_not_verified'), \Lang::get('general.sorry'))->autoclose(4000);
            return Redirect::back();
        } else {
            Alert::error(\Lang::get('general.this_phone_number_is_not_registered'), \Lang::get('general.sorry'))->autoclose(4000);
            return Redirect::back();
        }
        

    }
    
    public function setLogout() {
        
        Sentinel::logout();
        
        return redirect('/');
        
    }
    

}