<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Theme\FunctionsController;
use Redirect;
use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;
use DB;
use Carbon;
use Sentinel;
use App\Blog;
use App\Slider;
use App\Rooms;
use App\Boxes;
use App\Prizes;
use App\User;
use App\Notifications;
use App\PremiumSales;
use App\Transactions;
use App\ClickRewarded;
use App\ScratchWin;
use App\Offerwall;

class IndexController extends Controller {

    public function getIndex() {
        
        return view('frontend/index');

    }

}