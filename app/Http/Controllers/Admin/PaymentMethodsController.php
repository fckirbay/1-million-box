<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Illuminate\Http\Request;
use Sentinel;
use Alert;
use App\PaymentMethods;

class PaymentMethodsController extends Controller {
    
    public function getPaymentMethods() {

    	$paymentMethods = paymentMethods::get();

        return view('admin/payment-methods', compact('paymentMethods'));
        
    }

    public function setNewPaymentMethod(Request $request) {

    	$file = $request->file('logo');

        /*
        //Display File Name
        echo 'File Name: '.$file->getClientOriginalName();
        echo '<br>';
   
        //Display File Extension
        echo 'File Extension: '.$file->getClientOriginalExtension();
        echo '<br>';
   
        //Display File Real Path
        echo 'File Real Path: '.$file->getRealPath();
        echo '<br>';
   
        //Display File Size
        echo 'File Size: '.$file->getSize();
        echo '<br>';
   
        //Display File Mime Type
        echo 'File Mime Type: '.$file->getMimeType();

        exit;
        */
        
        $destinationPath = 'assets/mobile/images/prizes';
        $file->move($destinationPath,$file->getClientOriginalName());

        $new = new PaymentMethods();
        $new->title = $request->title;
        $new->description = $request->description;
        $new->prize = $request->prize;
        $new->logo = $destinationPath.'/'.$file->getClientOriginalName();
        $new->save();
        
        if($new) {
            Alert::success('İşlem başarılı!');
            return Redirect::back();
        } else {
            Alert::error('İşlem başarısız!', 'Üzgünüz!');
            return Redirect::back();
        }
        
    }

    public function setDeletePaymentMethod($id) {
        
        $delete = PaymentMethods::where('id', $id)->delete();
        
        if($delete) {
            Alert::success('İşlem başarılı!');
            return Redirect::back();
        } else {
            Alert::error('İşlem başarısız!', 'Üzgünüz!');
            return Redirect::back();
        }
        
    }

}

