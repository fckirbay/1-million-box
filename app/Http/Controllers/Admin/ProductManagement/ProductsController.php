<?php

namespace App\Http\Controllers\Admin\ProductManagement;

use App\Http\Controllers\Controller;
use Redirect;
use Illuminate\Http\Request;
use Sentinel;
use Alert;
use DB;
use Input;
use Validator;
use Illaoi;
use App\Products;
use App\ProductCategories;
use App\ProductBrands;
use App\User;

class ProductsController extends Controller {
    
    public function getProducts() {
        
        $products = Products::orderBy('id', 'desc')->get();
        
        return view('admin/product-management/products', compact('products'));
        
    }
    
    public function getNewProduct() {
        
        $categories = ProductCategories::where('is_active', 1)->get();
        $brands = ProductBrands::where('is_active', 1)->get();
        
        // Seller List
        $sellers = User::get();
        
        return view('admin/product-management/new-product', compact('categories', 'brands', 'sellers'));
        
    }
    
}

