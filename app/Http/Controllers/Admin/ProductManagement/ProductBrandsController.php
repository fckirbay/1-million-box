<?php

namespace App\Http\Controllers\Admin\ProductManagement;

use App\Http\Controllers\Controller;
use Redirect;
use Illuminate\Http\Request;
use Sentinel;
use Alert;
use DB;
use Input;
use Validator;
use Illaoi;
use App\ProductBrands;

class ProductBrandsController extends Controller {
    
    public function getProductBrands() {
        
        $brands = ProductBrands::orderBy('id', 'desc')->get();
        
        return view('admin/product-management/product-brands', compact('brands'));
        
    }
    
    public function setNewProductBrand(Request $request) {
        
        $slug = Illaoi::generateUnique($request->brand, new ProductBrands);
        
        $add = new ProductBrands();
        $add->slug = $slug;
        $add->brand = $request->brand;
        $add->save();
        
        if($add) {
            Alert::success('Marka oluşturuldu!');
            return Redirect::back();
        } else {
            Alert::error('Marka oluşturulamadı!', 'Üzgünüz!');
            return Redirect::back();
        }
        
    }
    
    public function setDeleteProductBrand(Request $request) {
        
        $delete = ProductBrands::where('id', $request->brand_id)->delete();
        
        if($delete) {
            Alert::success('Marka silindi!');
            return Redirect::back();
        } else {
            Alert::error('Marka silinemedi!', 'Üzgünüz!');
            return Redirect::back();
        }
        
    }
    
}

