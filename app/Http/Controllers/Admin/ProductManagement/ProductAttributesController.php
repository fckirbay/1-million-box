<?php

namespace App\Http\Controllers\Admin\ProductManagement;

use App\Http\Controllers\Controller;
use Redirect;
use Illuminate\Http\Request;
use Sentinel;
use Alert;
use DB;
use Input;
use Validator;
use Illaoi;
use App\ProductAttributes;

class ProductAttributesController extends Controller {
    
    public function getProductAttributes() {
        
        $attributes = ProductAttributes::orderBy('feature', 'asc')->get();
        
        return view('admin/product-management/product-attributes', compact('attributes'));
        
    }
    
    public function setNewProductAttributes(Request $request) {
        
        $add = new ProductAttributes();
        $add->feature = $request->feature;
        $add->type = $request->type;
        $add->value = $request->value;
        $add->save();
        
        if($add) {
            Alert::success('Kategori oluşturuldu!');
            return Redirect::back();
        } else {
            Alert::error('Kategori oluşturulamadı!', 'Üzgünüz!');
            return Redirect::back();
        }
        
    }
    
    public function setDeleteProductAttributes(Request $request) {
        
        $delete = ProductAttributes::where('id', $request->attribute_id)->delete();
        
        if($delete) {
            Alert::success('Özellik silindi!');
            return Redirect::back();
        } else {
            Alert::error('Özellik silinemedi!', 'Üzgünüz!');
            return Redirect::back();
        }
        
    }
    
}

