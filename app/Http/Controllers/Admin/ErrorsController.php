<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Illuminate\Http\Request;
use Sentinel;
use Alert;
use DB;
use Input;
use Validator;
use Illaoi;
use Carbon;
use Mail;
use App\User;
use App\Errors;

class ErrorsController extends Controller {
    
    public function getErrors() {
        
$title = "Başlık";
$content = "İçerik";


        $errors = Errors::orderBy('id', 'desc')->paginate(50);
        
        return view('admin/errors', compact('errors'));
        
    }

    public function setDeleteErrors() {

        $del = Errors::where('id', '>', 1)->delete();
        
        if($del) {
            Alert::success('İşlem başarılı!');
            return Redirect::back();
        } else {
            Alert::error('İşlem başarısız!', 'Üzgünüz!');
            return Redirect::back();
        }
        
    }
    
}

