<?php

namespace App\Http\Controllers\Admin\SiteManagement;

use App\Http\Controllers\Controller;
use Redirect;
use Illuminate\Http\Request;
use Sentinel;
use Alert;
use DB;
use Input;
use Validator;
use Illaoi;
use App\Slider;

class SlidesController extends Controller {
    
    public function getSlides() {
        
        $slides = Slider::orderBy('order', 'asc')->get();
        
        return view('admin/site-management/slides', compact('slides'));
        
    }
    
    public function setNewSlide(Request $request) {
        
        $add = new Slider();
        $add->photo = "";
        $add->title_1 = $request->title_1;
        $add->title_2 = $request->title_2;
        $add->title_3 = $request->title_3;
        $add->button_title = $request->button_title;
        $add->button_link = $request->button_link;
        $add->order = $request->order;
        $add->save();
        
        if($add) {
            Alert::success('Kategori oluşturuldu!');
            return Redirect::back();
        } else {
            Alert::error('Kategori oluşturulamadı!', 'Üzgünüz!');
            return Redirect::back();
        }
        
    }
    
    public function setDeleteProductCategory(Request $request) {
        
        $delete = ProductCategories::where('id', $request->category_id)->delete();
        
        if($delete) {
            Alert::success('Kategori silindi!');
            return Redirect::back();
        } else {
            Alert::error('Kategori silinemedi!', 'Üzgünüz!');
            return Redirect::back();
        }
        
    }
    
}

