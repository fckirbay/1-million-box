<?php

namespace App\Http\Controllers\Admin\SiteManagement;

use App\Http\Controllers\Controller;
use Redirect;
use Illuminate\Http\Request;
use Sentinel;
use Alert;
use DB;
use Input;
use Validator;
use Illaoi;
use App\User;
use App\Pages;
use App\ContentCategories;


class PagesController extends Controller {
    
    public function getPages() {
        
        $pages = DB::table('pages')
            ->join('content_categories', 'content_categories.id', '=', 'pages.category_id')
            ->where('pages.deleted_at', null)
            ->select('content_categories.category', 'pages.*')
            ->get();
        
        return view('admin/site-management/pages', compact('pages'));
        
    }
    
    public function getEditPage($pageId) {
        
        $langs = Langs::orderBy('lang', 'asc')->get();
        
        $categories = ContentCategories::where('lang', env("DEFAULT_LANGUAGE", "TR"))->get();
        
        return view('panel/site-management/edit-page', compact('langs', 'pageId', 'categories'));
        
    }
    
    public function getNewPage() {
        
        $categories = ContentCategories::get();
        
        return view('admin/site-management/new-page', compact('categories'));
        
    }
    
    public function setNewPage(Request $request) {
        
        $slug = Illaoi::generateUnique($request->title, new Pages);
        
        $add = new Pages();
        $add->category_id = $request->category_id;
        $add->user_id = Sentinel::getUser()->id;
        $add->title = $request->title;
        $add->slug = $slug;
        $add->content = $request->content;
        $add->image = null;
        $add->seo_title = $request->seo_title;
        $add->seo_description = $request->seo_description;
        $add->seo_keywords = $request->seo_keywords;
        $add->is_visible = $request->is_visible;
        $add->save();

        
        if($add) {
            Alert::success('Sayfa oluşturuldu!');
            return Redirect::back();
        } else {
            Alert::error('Sayfa oluşturulamadı!', 'Üzgünüz!');
            return Redirect::back();
        }
        
    }
    
    public function setEditPage(Request $request) {
        
        $nice_names = array(
            'title' => 'Title',
            'content' => 'Content',
            'seo_title' => 'Seo Title',
            'seo_description' => 'Seo Description',
            'seo_keywords' => 'Seo Keywords'
        );
        
        $validator = Validator::make($request->all(), [
            'title' => 'max:255',
            'content' => 'max:20000',
            'seo_title' => 'max:255',
            'seo_description' => 'max:500',
            'seo_keywords' => 'max:500'
        ])->setAttributeNames($nice_names);
        
        if ($validator->fails()) {
            Alert::error(trans('translations.please_try_again'), trans('translations.an_error_occurred'));
            return Redirect::back()->withErrors($validator)->withInput();
        }
        
        $control = Pages::where('lang', $request->lang)->where('page_id', $request->page_id)->first();
        
        if($control != null) {
            
            $control->password = $request->password;
            $control->title = $request->title;
            $control->content = $request->content;
            $control->category_id = $request->category_id;
            $control->order = $request->order;
            $control->image = null;
            $control->seo_title = $request->seo_title;
            $control->seo_description = $request->seo_description;
            $control->seo_keywords = $request->seo_keywords;
            $control->save();
            
        } else {
            
            $slug = Illaoi::generateUnique($request->title, new Pages);
        
            $control = new Pages();
            $control->lang = $request->lang;
            $control->page_id = $request->page_id;
            $control->user_id = Sentinel::getUser()->id;
            $control->order = $request->order;
            $control->title = $request->title;
            $control->category_id = $request->category_id;
            $control->slug = $slug;
            $control->content = $request->content;
            $control->image = null;
            $control->seo_title = $request->seo_title;
            $control->seo_description = $request->seo_description;
            $control->seo_keywords = $request->seo_keywords;
            $control->save();
            
        }
        
        if($control) {
            Alert::success(trans('translations.success'));
            return Redirect::back();
        } else {
            Alert::error(trans('translations.please_try_again'), trans('translations.an_error_occurred'));
            return Redirect::back();
        }
        
    }
    
    public function setDeletePage($newsId) {
        
        $delete = Pages::where('page_id', $newsId)->delete();
        
        if($delete) {
            Alert::success(trans('translations.success'));
            return Redirect::back();
        } else {
            Alert::error(trans('translations.please_try_again'), trans('translations.an_error_occurred'));
            return Redirect::back();
        }
        
    }
    
}

