<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Illuminate\Http\Request;
use Sentinel;
use Alert;
use DB;
use Input;
use Validator;
use Illaoi;
use Carbon;
use App\Transactions;
use App\User;
use App\Notifications;;

class PaymentsController extends Controller {
    
    public function getPayments() {

        $payments = DB::table('transactions')
            ->join('users2s', 'users2s.id', '=', 'transactions.user_id')
            ->select('users2s.username', 'transactions.*')
            ->where('transactions.type', 1)
            ->orderBy('transactions.id', 'desc')
            ->paginate(50);
        
        return view('admin/payments', compact('payments'));
        
    }

    public function setCompletePayment($id) {

    	$transaction = Transactions::where('id', $id)->first();
    	$transaction->payment_status = 1;
    	$transaction->save();

        $user = User::where('id', $transaction->user_id)->first();

        if($user->firebase != null) {
            $sendNotify = new Notifications();
            $sendNotify->user_id = $user->id;
            $sendNotify->title = 'LuckyBox News';
            if($user->lang == "tr") {
                $sendNotify->notification = "Ödeme işleminiz tamamlanmıştır! İyi günlerde harcayın. :)";
            } else {
                $sendNotify->notification = "You have a new message!";
            }
            $sendNotify->firebase = $user->firebase;
            $sendNotify->save();
        }
        
        if($sendNotify) {
            Alert::success('İşlem başarılı!');
            return Redirect::back();
        } else {
            Alert::error('İşlem başarısız!', 'Üzgünüz!');
            return Redirect::back();
        }
        
    }
    
}

