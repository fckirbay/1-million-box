<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Illuminate\Http\Request;
use Sentinel;
use Alert;
use DB;
use Input;
use Validator;
use Illaoi;
use Carbon;
use App\User;
use App\Offerwall;

class OfferwallController extends Controller {
    
    public function getOfferwall() {

        if(!isset($_GET['days'])) {
            $offers = DB::table('offerwall')
                ->leftjoin('users2s', 'offerwall.user_id', '=', 'users2s.id')
                ->select('users2s.username', 'offerwall.user_id', DB::raw('sum(usd_value) as sum'), DB::raw('count(*) as total'))
                ->where('is_completed', 1)
                ->where('offerwall.complete_date', '>=', Carbon\Carbon::now()->subDays(7))
                ->groupBy('users2s.username', 'offerwall.user_id')
                ->orderBy('sum', 'desc')
                ->paginate(50); 
        } else {
            if($_GET['order'] != "complete_date") {
                $offers = DB::table('offerwall')
                    ->leftjoin('users2s', 'offerwall.user_id', '=', 'users2s.id')
                    ->select('users2s.username', 'offerwall.user_id', DB::raw('sum(usd_value) as sum'), DB::raw('count(*) as total'))
                    ->where('is_completed', 1)
                    ->where('offerwall.complete_date', '>=', Carbon\Carbon::now()->subDays($_GET['days']))
                    ->groupBy('users2s.username', 'offerwall.user_id')
                    ->orderBy($_GET['order'], 'desc')
                    ->paginate(50);
            } else {
                $offers = DB::table('offerwall')
                    ->leftjoin('users2s', 'offerwall.user_id', '=', 'users2s.id')
                    ->select('users2s.username', 'offerwall.user_id', 'offerwall.usd_value as total', 'offerwall.usd_value as sum')
                    ->where('is_completed', 1)
                    ->where('offerwall.complete_date', '>=', Carbon\Carbon::now()->subDays($_GET['days']))
                    ->orderBy($_GET['order'], 'desc')
                    ->paginate(50); 
            }
            
        }
        
        return view('admin/offerwall', compact('offers'));
        
    }
    
}

