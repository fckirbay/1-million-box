<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Illuminate\Http\Request;
use Sentinel;
use Alert;
use DB;
use Input;
use Validator;
use Illaoi;
use Carbon;
use App\User;
use App\ClickRewarded;

class RewardedsController extends Controller {
    
    public function getRewardeds() {        
    	
        if(!isset($_GET['days'])) {
            $rewardeds = DB::table('click_rewarded')
                ->leftjoin('users2s', 'click_rewarded.user_id', '=', 'users2s.id')
                ->select('users2s.username', DB::raw('count(*) as total'))
                ->where('is_completed', 1)
                ->where('complete_date', '>=', Carbon\Carbon::now()->subDays(7))
                ->groupBy('first_name')
                ->orderBy('total', 'desc')
                ->paginate(50); 
        } else {
            $rewardeds = DB::table('click_rewarded')
                ->leftjoin('users2s', 'click_rewarded.user_id', '=', 'users2s.id')
                ->select('users2s.username', DB::raw('count(*) as total'))
                ->where('is_completed', 1)
                ->where('complete_date', '>=', Carbon\Carbon::now()->subDays($_GET['days']))
                ->groupBy('first_name')
                ->orderBy('total', 'desc')
                ->paginate(50); 
        }
    	       

        return view('admin/rewardeds', compact('rewardeds'));
        
    }
    
}

