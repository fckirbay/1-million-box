<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Illuminate\Http\Request;
use Sentinel;
use Alert;

class LoginController extends Controller {
    
    public function getLogin() {
        
        return view('admin/login');
    
    }
    
    public function setLogin(Request $request) {
        
        $credentials = [
         'email'    => $request->email . '@email.com',
         'password' => $request->password,
        ];

        $user = Sentinel::authenticateAndRemember($credentials);
        if($user != null)
        {
            return redirect('admin/dashboard');
        }else{
            Alert::error("Kullanıcı bulunamadı!");
            return view('admin/login');
        }
        
    }
    
    public function setLogout() {
        
        Sentinel::logout();
        
        $kontrol = Sentinel::getUser() ? 0 : 1;
        
        if($kontrol != 0) {
            return view('admin/login');
        } else {
            return view('/');
        }
        
    }
    
}

