<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Illuminate\Http\Request;
use Sentinel;
use Alert;
use DB;

class ExchangesController extends Controller {
    
    public function getExchanges() {        

        $exchanges = DB::table('exchanges')
            ->join('users2s', 'users2s.id', '=', 'exchanges.user_id')
            ->orderBy('exchanges.id', 'desc')
            ->paginate(50);
        
        return view('admin/exchanges', compact('exchanges'));
        
    }
    
}

