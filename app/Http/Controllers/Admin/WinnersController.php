<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Illuminate\Http\Request;
use Sentinel;
use Alert;
use DB;
use App\User;
use App\Winners;

class WinnersController extends Controller {
    
    public function getWinners() {

        $winners = DB::table('winners')
            ->join('users', 'users.id', '=', 'winners.user_id')
            ->select('winners.amount', 'users.first_name', 'users.id')
            ->orderBy('winners.created_at', 'desc')
            ->paginate(50);

        return view('admin/winners', compact('winners'));
        
    }

    public function setWinners() {

        $user = User::where('is_fake', 1)->inRandomOrder()->first();

        $probability = rand(0, 100);
        if($probability < 75) {
        	$amount = 50;
        } elseif($probability < 85) {
        	$amount = 100;
        } elseif($probability < 93) {
        	$amount = 150;
        } elseif($probability < 97) {
        	$amount = 200;
        } elseif($probability < 99) {
        	$amount = 250;
        } else {
        	$amount = 500;
        }

        $newWinner = new Winners();
        $newWinner->user_id = $user->id;
        $newWinner->amount = $amount;
        $newWinner->save();

        if($newWinner) {
            Alert::success('İşlem başarılı!');
            return Redirect::back();
        } else {
            Alert::error('İşlem başarısız!', 'Üzgünüz!');
            return Redirect::back();
        }
        
    }

}

