<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameLaps extends Model
{
    protected $table = 'game_laps';
}
