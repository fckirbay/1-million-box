<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users2s extends Model
{
    protected $table = 'users2s';
    public $timestamps = false;
}
