<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserReferences extends Model
{
    protected $table = 'user_references';
    public $timestamps = false;
}
