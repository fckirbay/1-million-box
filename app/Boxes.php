<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Boxes extends Model
{
    protected $table = 'boxes';
    public $timestamps = false;
}
